import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Observable} from 'rxjs/Observable';

@Injectable()
export class DataFilterService {
    private _dataFilterChangedSubject = new BehaviorSubject<any>(null);

    public dataFilterChangedSource$: Observable<any> = this._dataFilterChangedSubject.asObservable();

    setFilterChanged(model: any) {
        this._dataFilterChangedSubject.next(model);
    }
}