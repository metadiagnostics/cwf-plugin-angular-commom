"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var _ = require("lodash");
var utilities_1 = require("../../misc/utilities");
var data_filter_service_1 = require("./data-filter.service");
var DataFilterComponent = /** @class */ (function () {
    function DataFilterComponent(dataFilterService) {
        this.dataFilterService = dataFilterService;
    }
    DataFilterComponent.prototype.ngOnInit = function () {
        if (!_.isUndefined(this.buttons)) {
            _.each(_.get(this, 'buttons', []), function (button) {
                button.value = utilities_1.Guid.newGuid();
            });
            this.selectedButton = _.get(this.buttons, '[0].value');
        }
        if (!_.isUndefined(this.secondaryDropdownConfig)) {
            _.each(_.get(this, 'secondaryDropdownConfig.options', []), function (option) {
                option.value = utilities_1.Guid.newGuid();
            });
            this.selectedSecondaryOption = _.get(this.secondaryDropdownConfig.options, '[0].value');
        }
    };
    DataFilterComponent.prototype.buttonClicked = function (event) {
        this.sendFilterChangeEvent();
    };
    DataFilterComponent.prototype.secondaryOptionSelected = function (event) {
        this.sendFilterChangeEvent();
    };
    DataFilterComponent.prototype.sendFilterChangeEvent = function () {
        var filter = {};
        var button = _.find(this.buttons, { value: this.selectedButton });
        if (!_.isUndefined(this.buttons)) {
            if (_.get(button, 'params')) {
                _.assign(filter, button.params);
            }
        }
        if (!_.isUndefined(this.secondaryDropdownConfig)) {
            var secondaryFilter = _.find(this.secondaryDropdownConfig.options, { value: this.selectedSecondaryOption });
            if (_.get(secondaryFilter, 'params')) {
                _.assign(filter, secondaryFilter.params);
            }
        }
        this.dataFilterService.setFilterChanged(filter);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], DataFilterComponent.prototype, "buttons", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], DataFilterComponent.prototype, "secondaryDropdownConfig", void 0);
    DataFilterComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'data-filter',
            templateUrl: './data-filter.component.html',
            styleUrls: ['./data-filter.css'],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [data_filter_service_1.DataFilterService])
    ], DataFilterComponent);
    return DataFilterComponent;
}());
exports.DataFilterComponent = DataFilterComponent;
//# sourceMappingURL=data-filter.component.js.map