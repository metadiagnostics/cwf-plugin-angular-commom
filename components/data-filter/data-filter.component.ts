import {Component, ViewEncapsulation, OnInit, Input} from '@angular/core';

import * as _ from 'lodash';
import {Guid} from "../../misc/utilities";
import {DataFilterService} from "./data-filter.service";

@Component({
    moduleId: module.id,
    selector: 'data-filter',
    templateUrl: './data-filter.component.html',
    styleUrls: ['./data-filter.css'],
    encapsulation: ViewEncapsulation.None
})
export class DataFilterComponent implements OnInit {
    @Input() buttons: any;
    @Input() secondaryDropdownConfig: any;
    selectedButton: string;
    selectedSecondaryOption: string;

    constructor(private dataFilterService: DataFilterService) {}

    ngOnInit() {

        if (!_.isUndefined(this.buttons)) {
            _.each(_.get(this, 'buttons', []), function (button) {
                button.value = Guid.newGuid();
            });
            this.selectedButton = _.get(this.buttons, '[0].value');

        }

        if (!_.isUndefined(this.secondaryDropdownConfig)) {
            _.each(_.get(this, 'secondaryDropdownConfig.options', []), option => {
                option.value = Guid.newGuid();
            });
            this.selectedSecondaryOption = _.get(this.secondaryDropdownConfig.options, '[0].value');
        }
    }

    buttonClicked(event: any) {
        this.sendFilterChangeEvent();
    }

    secondaryOptionSelected(event: any) {
        this.sendFilterChangeEvent();
    }

    private sendFilterChangeEvent() {

        let filter = {};
        let button = _.find(this.buttons, {value: this.selectedButton});

        if (!_.isUndefined(this.buttons)) {
            if (_.get(button, 'params')) {
                _.assign(filter, button.params);
            }
        }

        if (!_.isUndefined(this.secondaryDropdownConfig)) {
            let secondaryFilter = _.find(this.secondaryDropdownConfig.options, {value: this.selectedSecondaryOption});

            if (_.get(secondaryFilter, 'params')) {
                _.assign(filter, secondaryFilter.params);
            }
        }

        this.dataFilterService.setFilterChanged(filter);
    }
}