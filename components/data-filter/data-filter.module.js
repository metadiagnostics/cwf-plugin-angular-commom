"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var selectbutton_1 = require("primeng/components/selectbutton/selectbutton");
var dropdown_1 = require("primeng/components/dropdown/dropdown");
var data_filter_service_1 = require("./data-filter.service");
var data_filter_component_1 = require("./data-filter.component");
var DataFilterModule = /** @class */ (function () {
    function DataFilterModule() {
    }
    DataFilterModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                selectbutton_1.SelectButtonModule,
                dropdown_1.DropdownModule
            ],
            declarations: [
                data_filter_component_1.DataFilterComponent
            ],
            exports: [
                data_filter_component_1.DataFilterComponent
            ],
            providers: [
                data_filter_service_1.DataFilterService
            ]
        })
    ], DataFilterModule);
    return DataFilterModule;
}());
exports.DataFilterModule = DataFilterModule;
//# sourceMappingURL=data-filter.module.js.map