import { Observable } from 'rxjs/Observable';
export declare class DataFilterService {
    private _dataFilterChangedSubject;
    dataFilterChangedSource$: Observable<any>;
    setFilterChanged(model: any): void;
}
