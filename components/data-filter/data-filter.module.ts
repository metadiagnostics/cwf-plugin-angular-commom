import {NgModule} from "@angular/core";

import { CommonModule} from "@angular/common";
import { FormsModule} from "@angular/forms";
import {SelectButtonModule} from "primeng/components/selectbutton/selectbutton";
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import {DataFilterService} from "./data-filter.service";
import {DataFilterComponent} from "./data-filter.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SelectButtonModule,
        DropdownModule
    ],
    declarations: [
        DataFilterComponent
    ],
    exports: [
        DataFilterComponent
    ],
    providers: [
        DataFilterService
    ]
})
export class DataFilterModule{}