import { OnInit } from '@angular/core';
import { DataFilterService } from "./data-filter.service";
export declare class DataFilterComponent implements OnInit {
    private dataFilterService;
    buttons: any;
    secondaryDropdownConfig: any;
    selectedButton: string;
    selectedSecondaryOption: string;
    constructor(dataFilterService: DataFilterService);
    ngOnInit(): void;
    buttonClicked(event: any): void;
    secondaryOptionSelected(event: any): void;
    private sendFilterChangeEvent();
}
