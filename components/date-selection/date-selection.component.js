"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
var moment = require("moment");
var DateSelectionComponent = /** @class */ (function () {
    function DateSelectionComponent() {
        this.dateChangedSubject = new BehaviorSubject_1.BehaviorSubject(null);
        this.dateChanged$ = this.dateChangedSubject.asObservable();
    }
    DateSelectionComponent.prototype.ngOnInit = function () {
        this.dateValue = new Date();
    };
    DateSelectionComponent.prototype.onDateSelected = function () {
        this.dateChangedSubject.next(this.dateValue);
    };
    DateSelectionComponent.prototype.clickArrow = function (event, direction) {
        event.preventDefault();
        var momentDate = moment(this.dateValue);
        if (direction === 'left') {
            momentDate.subtract(7, 'day');
        }
        else {
            momentDate.add(7, 'day');
        }
        this.dateValue = momentDate.toDate();
        this.onDateSelected();
    };
    DateSelectionComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'date-selection',
            templateUrl: './date-selection.component.html',
            encapsulation: core_1.ViewEncapsulation.None,
            styleUrls: ['./date-selection.component.css']
        })
    ], DateSelectionComponent);
    return DateSelectionComponent;
}());
exports.DateSelectionComponent = DateSelectionComponent;
//# sourceMappingURL=date-selection.component.js.map