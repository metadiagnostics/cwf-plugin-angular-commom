import { Observable } from "rxjs/Observable";
export declare class DateSelectionComponent {
    private dateValue;
    private dateChangedSubject;
    dateChanged$: Observable<any>;
    ngOnInit(): void;
    onDateSelected(): void;
    clickArrow(event: any, direction: string): void;
}
