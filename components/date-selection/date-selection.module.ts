import {NgModule} from "@angular/core";

import { CommonModule} from "@angular/common";
import { FormsModule} from "@angular/forms";
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { DateSelectionComponent } from "./date-selection.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        CalendarModule
    ],
    declarations: [
        DateSelectionComponent
    ],
    exports: [
        DateSelectionComponent
    ]
})
export class DateSelectionModule{}