import {Component, ViewEncapsulation} from '@angular/core';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Observable} from "rxjs/Observable";

import * as moment from 'moment';

@Component({
    moduleId   : module.id,
    selector   : 'date-selection',
    templateUrl: './date-selection.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./date-selection.component.css']
})
export class DateSelectionComponent {

    private dateValue: Date;

    private dateChangedSubject = new BehaviorSubject<any>(null);
    public dateChanged$: Observable<any> = this.dateChangedSubject.asObservable();

    ngOnInit() {
        this.dateValue = new Date();
    }

    onDateSelected() {
        this.dateChangedSubject.next(this.dateValue);
    }

    clickArrow(event: any, direction: string) {
        event.preventDefault();
        let momentDate = moment(this.dateValue);
        if (direction === 'left') {
            momentDate.subtract(7, 'day');
        } else {
            momentDate.add(7, 'day');
        }
        this.dateValue = momentDate.toDate();
        this.onDateSelected();
    }
}