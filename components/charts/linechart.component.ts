/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import {Component, Input, ViewEncapsulation} from '@angular/core';
import * as _ from 'lodash';
import { LineChartElement, PlottableDataSetEntry, PlottableData  } from './linechart.utils';

@Component({
               moduleId: module.id,
               selector: 'linechart',
               templateUrl: './linechart.component.html',
               encapsulation: ViewEncapsulation.None
           })


export class LinechartComponent {
    data: PlottableData;

    @Input()
    options: any;

    constructor() {
        this.clear();
    }


    public  clear() {
        this.data = {
            labels: [],
            datasets: []
        };
    }
    public draw(title: string, values: Array<LineChartElement>) {
        this.clear();

        if (_.isEmpty(values)) {
            return;
        }

        let labels = [];
        let data = [];
        _.forEach(values, function (value) {
            labels.push(value.getX());
            data.push(parseInt(value.getY()));
        });

        let dataSetEntry = {} as  PlottableDataSetEntry;
        dataSetEntry.label = title;
        dataSetEntry.fill = true;
        dataSetEntry.borderColor = '#000000';
        dataSetEntry.data = data;

        let plottableData  = {} as PlottableData;
        plottableData.datasets = [];
        plottableData.labels = labels;
        plottableData.datasets.push(dataSetEntry);
        this.data = plottableData;
    }

    public drawMultipleDataSets(plottableData: PlottableData, options: any) {
        this.clear();

        if (_.isEmpty(plottableData)) {
            return;
        }

        this.data = plottableData;
    }

    selectData(event) {
        // console.log(event);
    }
}
