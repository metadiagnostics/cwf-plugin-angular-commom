export declare class LineChartElement {
    private x;
    private y;
    constructor(x: string, y: number);
    getX(): string;
    getY(): number;
}
export interface PlottableDataSetEntry {
    label: string;
    data: Array<number>;
    fill: boolean;
    borderColor: string;
    borderDash: number[];
}
export interface PlottableData {
    labels: Array<string>;
    datasets: Array<PlottableDataSetEntry>;
}
