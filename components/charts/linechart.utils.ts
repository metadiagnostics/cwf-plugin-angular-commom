/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


export class LineChartElement {
    constructor(private x: string, private y: number){
    }

    public getX() : string {
        return this.x;
    }

    public getY() : number {
        return this.y;
    }
}

export interface PlottableDataSetEntry {
    label : string;
    data : Array<number>;
    fill : boolean;
    borderColor : string;
    borderDash: number[];
}

export interface PlottableData {
    labels: Array<string>;
    datasets: Array<PlottableDataSetEntry>;
}

