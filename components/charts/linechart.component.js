"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var _ = require("lodash");
var LinechartComponent = /** @class */ (function () {
    function LinechartComponent() {
        this.clear();
    }
    LinechartComponent.prototype.clear = function () {
        this.data = {
            labels: [],
            datasets: []
        };
    };
    LinechartComponent.prototype.draw = function (title, values) {
        this.clear();
        if (_.isEmpty(values)) {
            return;
        }
        var labels = [];
        var data = [];
        _.forEach(values, function (value) {
            labels.push(value.getX());
            data.push(parseInt(value.getY()));
        });
        var dataSetEntry = {};
        dataSetEntry.label = title;
        dataSetEntry.fill = true;
        dataSetEntry.borderColor = '#000000';
        dataSetEntry.data = data;
        var plottableData = {};
        plottableData.datasets = [];
        plottableData.labels = labels;
        plottableData.datasets.push(dataSetEntry);
        this.data = plottableData;
    };
    LinechartComponent.prototype.drawMultipleDataSets = function (plottableData, options) {
        this.clear();
        if (_.isEmpty(plottableData)) {
            return;
        }
        this.data = plottableData;
    };
    LinechartComponent.prototype.selectData = function (event) {
        // console.log(event);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], LinechartComponent.prototype, "options", void 0);
    LinechartComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'linechart',
            templateUrl: './linechart.component.html',
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [])
    ], LinechartComponent);
    return LinechartComponent;
}());
exports.LinechartComponent = LinechartComponent;
//# sourceMappingURL=linechart.component.js.map