import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {LinechartComponent} from "./linechart.component";
import { ChartModule } from 'angular2-chartjs';

@NgModule({
    imports: [
        CommonModule,
        ChartModule
    ],
    declarations: [
        LinechartComponent
    ],
    exports: [
        LinechartComponent
    ]
})
export class ChartsModule{}