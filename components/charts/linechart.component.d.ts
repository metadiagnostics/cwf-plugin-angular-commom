import { LineChartElement, PlottableData } from './linechart.utils';
export declare class LinechartComponent {
    data: PlottableData;
    options: any;
    constructor();
    clear(): void;
    draw(title: string, values: Array<LineChartElement>): void;
    drawMultipleDataSets(plottableData: PlottableData, options: any): void;
    selectData(event: any): void;
}
