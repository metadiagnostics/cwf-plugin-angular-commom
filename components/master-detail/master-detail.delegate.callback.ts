/*
 *  Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
import { MasterDetailCalllback } from './master-detail.callback'; 
import { MasterDetailComponent } from './master-detail.component'; 

/**
 * Implementation of MasterDetailCalllback that delegates all the invocations
 * to a MasterDetailComponent component.
 */
export class MasterDetailDelegateCalllback implements MasterDetailCalllback{

    private cmp:MasterDetailComponent;
    
    constructor(){
    }
    
    public setMasterDetailComponent(cmp:MasterDetailComponent){
      this.cmp = cmp;
    }
  
    updateMasterGrid(model: any){
      this.cmp.updateMasterGrid(model);
    }
    
    updateDetailGrid(model: any){
      this.cmp.updateDetailGrid(model);
    }
    
    updateAttachments(attachments: any){
      this.cmp.updateAttachments(attachments);
    }
    
    activatePlugin(response: any){
      this.cmp.activatePlugin(response);
    }
    
    updateAccessionData(response:any){
      this.cmp.updateAccessionData(response);
    }

    updateNarrative(response: any) {
        this.cmp.updateNarrative(response);
    }
    
    updateFormDefinition(response: any) {
        this.cmp.updateFormDefinition(response);
    }
    
    updateFormResponses(response: any) {
        this.cmp.updateFormResponses(response);
    }

    liveUpdateMasterGrid(model: any){
        this.cmp.liveUpdateMasterGrid(model);
    }
}