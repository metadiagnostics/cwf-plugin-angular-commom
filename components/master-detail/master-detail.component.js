"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var constants_1 = require("./constants");
var _ = require("lodash");
var moment = require("moment");
var ibackend_service_1 = require("../../services/ibackend.service");
var utilities_1 = require("../../misc/utilities");
var md_attachments_component_1 = require("./attachments/md-attachments.component");
var md_detail_table_component_1 = require("./detail-table/md-detail-table.component");
var md_narrative_component_1 = require("./narrative/md-narrative.component");
var md_smart_form_component_1 = require("./smart-form/md-smart-form.component");
var data_filter_service_1 = require("../../components/data-filter/data-filter.service");
var table_model_processor_1 = require("../../components/table/table.model.processor");
var table_grid_renderer_1 = require("../../components/table/table.grid.renderer");
var create_resource_service_1 = require("../../services/create-resource.service");
var grid_pagination_service_1 = require("../../services/grid.pagination.service");
var MasterDetailComponent = /** @class */ (function () {
    function MasterDetailComponent(backendService, dataFilterService, createResourceService, gridPaginationService, sanitizer, componentFactoryResolver) {
        this.backendService = backendService;
        this.dataFilterService = dataFilterService;
        this.createResourceService = createResourceService;
        this.gridPaginationService = gridPaginationService;
        this.sanitizer = sanitizer;
        this.componentFactoryResolver = componentFactoryResolver;
        this.totalNumberOfRecords = 0;
        this.currentPageIndex = 0;
        this.pageSize = 0;
        this.currentSelectedRowId = undefined;
        this.detailSelectedIndex = 0;
        /**
         * This maps contains the relation between tab index and detail component.
         */
        this.detailComponentsRendered = new Map();
        this.detailComponents = new Map();
        this.selectedDate = moment().toDate();
    }
    MasterDetailComponent.prototype.ngOnInit = function () {
        this.initializeHeaderGrid();
    };
    MasterDetailComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.createResourceService.closeResourceFormEvent.subscribe(function (domain) {
            if (domain != null) {
                _this.createResourceComponent.closeForm();
                _this.createModeEnabled = false;
            }
        });
        this.gridPaginationService.pageNumber$.subscribe(function (pageIndex) {
            if (pageIndex === null || pageIndex < 1 || _this.currentPageIndex === pageIndex) {
                return;
            }
            // console.log(`I have told to change page to ${pageIndex} `);
            _this.currentPageIndex = pageIndex;
            _this.setupAndRequestData();
        });
        //HEADER Section Configuration
        if (this.config.sections.header) {
            if (this.config.sections.header.dateSelection) {
                this.dateSelectionComponent.dateChanged$.subscribe(function (date) {
                    if (date != null) {
                        _this.selectedDate = date;
                        _this.resetPagingSettings();
                        _this.setupAndRequestData();
                    }
                });
            }
            if (this.config.sections.header.filterButtons || this.config.sections.header.secondaryDropdownConfig) {
                this.currentDataFilter = {};
                _.assign(this.currentDataFilter, _.get(this, 'config.sections.header.filterButtons[0].params'));
                _.assign(this.currentDataFilter, _.get(this, 'config.sections.header.secondaryDropdownConfig.options[0].params'));
                this.dataFilterService.dataFilterChangedSource$.subscribe(function (model) {
                    // && !_.isEqual(model, this.currentDataFilter)
                    if (model !== null) {
                        _this.currentDataFilter = model;
                        _this.resetPagingSettings();
                        _this.setupAndRequestData();
                    }
                });
            }
        }
        if (this.config.sections.details.tabs) {
            this.detailPanel.onChange.subscribe(function (e) {
                if (e != null) {
                    _this.onDetailTabSelected(e.index);
                }
            });
        }
    };
    MasterDetailComponent.prototype.setupAndRequestData = function () {
        this.resetDetailSection();
        this.grid.showLoadingIndicator();
        this.requestData();
    };
    MasterDetailComponent.prototype.onDetailTabSelected = function (index) {
        var uuid = this.detailComponentsRendered.get(index);
        if (_.isUndefined(this.currentSelectedRowId) || !this.config.sections.details || !this.config.sections.details.tabs || this.config.sections.details.tabs.length == 0) {
            return;
        }
        if (_.isUndefined(uuid)) {
            //If we don't have any component for that tab, we have to initialize it.
            var cmpRef = this.initializeTab(index);
            //And create the mappings
            this.detailComponents.set(cmpRef.instance.getUUID(), cmpRef);
            this.detailComponentsRendered.set(index, cmpRef.instance.getUUID());
            cmpRef.instance.init();
        }
        else {
            this.detailComponents.get(uuid).instance.refresh();
        }
    };
    MasterDetailComponent.prototype.initializeTab = function (index) {
        var tab = this.detailPanel.findSelectedTab();
        var tabCfg = this.config.sections.details.tabs[index];
        var cmpRef;
        var tabDiv = this.detailTabs.find(function (d) {
            return ('dynDiv-' + index) == d.element.nativeElement.className;
        });
        if (tabCfg.type === 'DETAILS_TABLE') {
            cmpRef = tabDiv.createComponent(this.componentFactoryResolver.resolveComponentFactory(md_detail_table_component_1.MDDetailTableComponent), 0);
        }
        else if (tabCfg.type === 'ATTACHMENTS') {
            cmpRef = tabDiv.createComponent(this.componentFactoryResolver.resolveComponentFactory(md_attachments_component_1.MDAttachmentsComponent), 0);
        }
        else if (tabCfg.type === 'NARRATIVE') {
            cmpRef = tabDiv.createComponent(this.componentFactoryResolver.resolveComponentFactory(md_narrative_component_1.MDNarrativeComponent), 0);
            //TODO: Might be a better way to do this
            var selectedRows = this.grid.gridOptions.api.getSelectedRows();
            if (_.isArray(selectedRows) && selectedRows.length > 0) {
                cmpRef.instance.setResourceSubType(selectedRows[0].classType);
            }
        }
        else if (tabCfg.type === 'SMART_FORM') {
            cmpRef = tabDiv.createComponent(this.componentFactoryResolver.resolveComponentFactory(md_smart_form_component_1.MDSmartFormComponent), 0);
        }
        var uuid = utilities_1.Guid.newGuid();
        cmpRef.instance.setUUID(uuid);
        cmpRef.instance.setResourceId(this.currentSelectedRowId);
        cmpRef.instance.setConfiguration(tabCfg);
        return cmpRef;
    };
    MasterDetailComponent.prototype.initializeHeaderGrid = function () {
        var _this = this;
        var gridOptions = {
            onGridReady: function () {
                if (utilities_1.Utilities.isRunningStandalone()) {
                    _this.requestData();
                }
            },
            onSelectionChanged: function (event) {
                var selectedRows = event.api.getSelectedRows();
                if (_.isArray(selectedRows) && selectedRows.length > 0) {
                    _this.currentSelectedRowId = selectedRows[0].id;
                    _this.resetDetailSection();
                    if (_this.config.sections.details.tabs) {
                        //refresh first tab
                        //TODO: actually change to the first tab
                        _this.detailPanel.activeIndex = 0;
                        _this.detailSelectedIndex = 0;
                        _this.onDetailTabSelected(0);
                    }
                }
            },
            onRowClicked: function (event) {
                if (_.isFunction(_this.config.sections.header.shouldOpenDrawer) && _this.config.sections.header.shouldOpenDrawer(event.data)) {
                    _this.backendService.sendBackendEvent('openDrawer', {
                        dataType: _this.config.type,
                        resourceId: _.get(event, 'data.id'),
                        '_callback': 'openDrawer'
                    });
                }
            }
        };
        _.assign(gridOptions, this.config.sections.header.gridOptions);
        var modelProcessor = new table_model_processor_1.TableModelProcessor(this.config.sections.header.columns);
        var renderer = new table_grid_renderer_1.TableGrid();
        if (this.isPagingServerSide()) {
            this.pageSize = this.getDefaultPageSize();
            this.grid.setServerSidePaging(true);
        }
        this.grid.initialize(modelProcessor, renderer, gridOptions, true, 50, true);
    };
    MasterDetailComponent.prototype.requestData = function () {
        this.grid.setModel({ data: [] });
        this.resetDetailSection();
        this.grid.showLoadingIndicator();
        if (_.isEmpty(this.backendService.getRootNativeElement())) {
            this.backendService.setRootNativeElement(this.config.root.nativeElement);
        }
        var params = this.getModelQueryParams();
        params._callback = 'updateMasterGrid';
        params._liveUpdateCallback = 'liveUpdateMasterGrid';
        this.backendService.sendBackendEvent('retrieveData', params);
    };
    MasterDetailComponent.prototype.getModelQueryParams = function () {
        var retVal = {
            type: this.config.type,
            categories: this.config.sections.header.categories
        };
        if (this.config.sections.header.dateSelection) {
            retVal.startTime = this.getFormattedDateParam(this.selectedDate, -90);
            retVal.endTime = this.getFormattedDateParam(this.selectedDate, 90);
        }
        if (!_.isUndefined(this.currentDataFilter)) {
            _.assign(retVal, this.currentDataFilter);
        }
        if (!_.isEmpty(this.config.extraParams)) {
            _.assign(retVal, this.config.extraParams);
        }
        if (this.isPagingServerSide()) {
            retVal = this.adjustPagingParams(retVal);
        }
        return retVal;
    };
    MasterDetailComponent.prototype.adjustPagingParams = function (input) {
        input.pageOffset = (this.getCurrentPageIndex() - 1) * this.pageSize;
        input.count = this.pageSize;
        console.log(" asking for data page offset is " + input.pageOffset);
        return input;
    };
    ;
    MasterDetailComponent.prototype.isPagingServerSide = function () {
        return _.get(this, 'config.pagination.strategy') === constants_1.Constants.SERVER;
    };
    MasterDetailComponent.prototype.resetPagingSettings = function () {
        if (this.isPagingServerSide()) {
            this.resetPageIndex();
        }
    };
    MasterDetailComponent.prototype.getDefaultPageSize = function () {
        return _.get(this, 'config.pagination.pageSize', 0);
    };
    MasterDetailComponent.prototype.getFormattedDateParam = function (date, daysToAdd) {
        return moment(date).add(daysToAdd, 'day').format('YYYYMMDD');
    };
    MasterDetailComponent.prototype.createNewItem = function () {
        this.createModeEnabled = true;
        this.createResourceComponent.initForm(_.get(this, 'config.sections.createButton.domain'));
    };
    MasterDetailComponent.prototype.getCurrentPageIndex = function () {
        return this.currentPageIndex === 0 ? 1 : this.currentPageIndex;
    };
    MasterDetailComponent.prototype.resetPageIndex = function () {
        this.currentPageIndex = 0;
        this.gridPaginationService.setPageNumber(0);
        this.gridPaginationService.setTotalPages(0);
    };
    // Server callback for we asked for master data
    MasterDetailComponent.prototype.updateMasterGrid = function (response) {
        var model = response.payload;
        this.grid.setModel(model);
        if (_.get(model, 'data.length', 0) > 0) {
            this.grid.selectRow(0);
        }
        if (this.isPagingServerSide()) {
            this.totalNumberOfRecords = response.payload.totalRows;
            this.gridPaginationService.setTotalPages(this.totalNumberOfRecords / this.pageSize);
        }
    };
    // Server callback for we asked for detail data
    MasterDetailComponent.prototype.updateDetailGrid = function (response) {
        var uuid = response._uuid;
        //TODO: unsafe invocation of 'updateDetailGrid'
        var compRef = this.detailComponents.get(uuid);
        if (!_.isUndefined(compRef) && compRef.instance) {
            compRef.instance['updateDetailGrid'].apply(compRef.instance, [response]);
        }
    };
    //Server callback
    MasterDetailComponent.prototype.updateAttachments = function (response) {
        var uuid = response._uuid;
        //TODO: unsafe invocation of 'updateAttachments'
        var instance = this.detailComponents.get(uuid).instance;
        instance['updateAttachments'].apply(instance, [response]);
    };
    //Server callback
    MasterDetailComponent.prototype.updateAccessionData = function (response) {
        var uuid = response._uuid;
        //TODO: unsafe invocation of 'updateAccessionData'
        var instance = this.detailComponents.get(uuid).instance;
        instance['updateAccessionData'].apply(instance, [response]);
    };
    MasterDetailComponent.prototype.updateNarrative = function (response) {
        var instance = this.detailComponents.get(response._uuid).instance;
        instance['updateNarrative'].apply(instance, [response]);
    };
    //Server callback
    MasterDetailComponent.prototype.updateFormDefinition = function (response) {
        if (this.createModeEnabled) {
            this.createResourceComponent.updateFormDefinition(response);
        }
        else {
            var uuid = response._uuid;
            //TODO: unsafe invocation of 'updateFormDefinition'
            var instance = this.detailComponents.get(uuid).instance;
            instance['updateFormDefinition'].apply(instance, [response]);
        }
    };
    //Server callback
    MasterDetailComponent.prototype.updateFormResponses = function (response) {
        var uuid = response._uuid;
        //TODO: unsafe invocation of 'updateFormResponses'
        var instance = this.detailComponents.get(uuid).instance;
        if (!_.isUndefined(instance)) {
            instance['updateFormResponses'].apply(instance, [response]);
        }
    };
    // Server callback when plugin is activated
    MasterDetailComponent.prototype.activatePlugin = function (response) {
        //This callback is not propagated to the components.
        if (_.get(response, 'payload.dataChanged')) {
            this.requestData();
        }
    };
    // Server live update callback for master data
    MasterDetailComponent.prototype.liveUpdateMasterGrid = function (response) {
        var model = response.payload;
        this.grid.updateModel(model);
        var selectedRows = this.grid.gridOptions.api.getSelectedRows();
        if (!_.isArray(selectedRows) || selectedRows.length === 0) {
            this.grid.selectRow(0);
        }
    };
    MasterDetailComponent.prototype.resetDetailSection = function () {
        this.detailComponents.forEach(function (cmpRef, uuid) {
            cmpRef.instance.destroy();
            cmpRef.destroy();
        });
        this.detailComponentsRendered.clear();
        this.detailComponents.clear();
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], MasterDetailComponent.prototype, "config", void 0);
    __decorate([
        core_1.ViewChild('grid'),
        __metadata("design:type", Object)
    ], MasterDetailComponent.prototype, "grid", void 0);
    __decorate([
        core_1.ViewChild('dateSelection'),
        __metadata("design:type", Object)
    ], MasterDetailComponent.prototype, "dateSelectionComponent", void 0);
    __decorate([
        core_1.ViewChild('detailPanel'),
        __metadata("design:type", Object)
    ], MasterDetailComponent.prototype, "detailPanel", void 0);
    __decorate([
        core_1.ViewChild('createResource'),
        __metadata("design:type", Object)
    ], MasterDetailComponent.prototype, "createResourceComponent", void 0);
    __decorate([
        core_1.ViewChildren('dynDiv', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.QueryList)
    ], MasterDetailComponent.prototype, "detailTabs", void 0);
    MasterDetailComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'master-detail',
            templateUrl: 'master-detail.component.html',
            styleUrls: ['./master-detail.component.css'],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [ibackend_service_1.IBackendService,
            data_filter_service_1.DataFilterService,
            create_resource_service_1.CreateResourceService,
            grid_pagination_service_1.GridPaginationService,
            platform_browser_1.DomSanitizer,
            core_1.ComponentFactoryResolver])
    ], MasterDetailComponent);
    return MasterDetailComponent;
}());
exports.MasterDetailComponent = MasterDetailComponent;
//# sourceMappingURL=master-detail.component.js.map