import { MasterDetailCalllback } from './master-detail.callback';
import { MasterDetailComponent } from './master-detail.component';
/**
 * Implementation of MasterDetailCalllback that delegates all the invocations
 * to a MasterDetailComponent component.
 */
export declare class MasterDetailDelegateCalllback implements MasterDetailCalllback {
    private cmp;
    constructor();
    setMasterDetailComponent(cmp: MasterDetailComponent): void;
    updateMasterGrid(model: any): void;
    updateDetailGrid(model: any): void;
    updateAttachments(attachments: any): void;
    activatePlugin(response: any): void;
    updateAccessionData(response: any): void;
    updateNarrative(response: any): void;
    updateFormDefinition(response: any): void;
    updateFormResponses(response: any): void;
    liveUpdateMasterGrid(model: any): void;
}
