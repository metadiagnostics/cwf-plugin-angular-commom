/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MasterDetailComponent} from './master-detail.component';
import {DateSelectionModule} from '../date-selection/date-selection.module';
import {ButtonModule} from 'primeng/components/button/button';
import {TableModule} from '../table/table.module';
import {SmartFormModule} from '../smart-form/smart-form.module';
import {FormsModule} from '@angular/forms';
import {TabViewModule} from 'primeng/components/tabview/tabview';
import { SplitPaneModule } from 'ng2-split-pane/lib/ng2-split-pane';
import {ibackendProvider} from '../../providers/ibackend.provider';
import {MDAttachmentsModule} from './attachments/md-attachments.module';
import {MDDetailTableModule} from './detail-table/md-detail-table.module';
import {MDNarrativeModule} from './narrative/md-narrative.module';
import {MDSmartFormModule} from './smart-form/md-smart-form.module';
import {CreateResourceModule} from '../create-resource/create-resource.module';
import {DataFilterModule} from '../data-filter/data-filter.module';
import {GridPaginationService} from '../../services/grid.pagination.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TabViewModule,
        SplitPaneModule,
        DateSelectionModule,
        DataFilterModule,
        ButtonModule,
        TableModule,
        SmartFormModule,
        MDAttachmentsModule,
        MDDetailTableModule,
        MDNarrativeModule,
        MDSmartFormModule,
        CreateResourceModule
    ],
    declarations: [
        MasterDetailComponent
    ],
    exports: [
        MasterDetailComponent
    ],
    providers: [
        ibackendProvider
    ]
})
export class MasterDetailModule{}