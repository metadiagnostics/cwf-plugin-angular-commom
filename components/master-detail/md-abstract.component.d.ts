export declare abstract class MDAbstractComponent {
    private uuid;
    private resourceId;
    private resourceSubType;
    private configuration;
    setUUID(uuid: string): void;
    getUUID(): string;
    setResourceId(resourceId: string): void;
    getResourceId(): string;
    setConfiguration(configuration: any): void;
    getConfiguration(): any;
    setResourceSubType(resourceSubType: string): any;
    getResourceSubType(): any;
    abstract init(): any;
    abstract refresh(): any;
    abstract clean(): any;
    abstract destroy(): any;
}
