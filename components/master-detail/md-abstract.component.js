"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var MDAbstractComponent = /** @class */ (function () {
    function MDAbstractComponent() {
    }
    MDAbstractComponent.prototype.setUUID = function (uuid) {
        this.uuid = uuid;
    };
    MDAbstractComponent.prototype.getUUID = function () {
        return this.uuid;
    };
    MDAbstractComponent.prototype.setResourceId = function (resourceId) {
        this.resourceId = resourceId;
    };
    MDAbstractComponent.prototype.getResourceId = function () {
        return this.resourceId;
    };
    MDAbstractComponent.prototype.setConfiguration = function (configuration) {
        this.configuration = configuration;
    };
    MDAbstractComponent.prototype.getConfiguration = function () {
        return this.configuration;
    };
    MDAbstractComponent.prototype.setResourceSubType = function (resourceSubType) {
        this.resourceSubType = resourceSubType;
    };
    MDAbstractComponent.prototype.getResourceSubType = function () {
        return this.resourceSubType;
    };
    return MDAbstractComponent;
}());
exports.MDAbstractComponent = MDAbstractComponent;
//# sourceMappingURL=md-abstract.component.js.map