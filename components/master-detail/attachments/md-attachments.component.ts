/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Component, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import * as _ from 'lodash';
import * as moment from 'moment';
import * as base64js from "base64-js"

import {TableComponent} from "../../../components/table/table.component";
import {TableGrid} from "../../../components/table/table.grid.renderer";

import { MDAbstractComponent } from '../md-abstract.component';

import { IBackendService } from '../../../services/ibackend.service';

import {Utilities} from "../../../misc/utilities";
import {AttachmentModelProcessor} from "./attachment.model.processor";


@Component({
  moduleId   : module.id,
  templateUrl: 'md-attachments.component.html',
  styleUrls: ['./md-attachments.component.css']
})
export class MDAttachmentsComponent extends MDAbstractComponent {

    @ViewChild('lightboxDataContent')
    private lightboxDataContent;

    @ViewChild('lightboxImageContent')
    private lightboxImageContent;

    @ViewChild('lightboxImageLink')
    private lightboxImageLink;

    @ViewChild('lightboxContentLink')
    private lightboxContentLink;

    readonly MODE_SINGLE: string = "_MODE_SINGLE_";
    readonly MODE_MULTI: string = "_MODE_MULTI_";

    private attachmentsGridRef: TableComponent;

    private attachments = [];
    private accession: any = {};
    private diagnosticCodes: string = "";

    constructor(private backendService: IBackendService,
                private sanitizer:DomSanitizer) {
      super();
    }

    ngOnInit() {}
      
    ngAfterViewInit() {
      
      if (this.getMode() === this.MODE_MULTI){ 
        let modelProcessor = new AttachmentModelProcessor([{
            "field": "title",
            "headerName": "Attachment"
        }, {
            "field": "creation",
            "headerName": "Date/Time"
        }]);

        let renderer = new TableGrid();

        let options: any = {
            onGridReady: () => {
                this.refresh();
            },
            onRowClicked: (event) => {
                if (_.get(event, 'data.contentType', '').indexOf('jpeg') > -1) {
                    let imageElem = this.lightboxImageContent.nativeElement;
                    imageElem.setAttribute('src', _.get(event, 'data.fileURL'));
                    this.lightboxImageLink.nativeElement.click();
                } else {
                    let frameElem = this.lightboxDataContent.nativeElement;
                    frameElem.setAttribute('type', _.get(event, 'data.contentType'));
                    frameElem.setAttribute('data', _.get(event, 'data.fileURL'));
                    this.lightboxContentLink.nativeElement.click();
                }
            },
            rowStyle: {
                cursor: 'pointer'
            }
        };
            
        this.attachmentsGridRef.initialize(modelProcessor, renderer, options);
      }
    }
    
    public init(){
      if (this.getMode() === this.MODE_SINGLE){ 
        this.refresh();
      }
    }
    
    public refresh(){
      this.requestAttachments(this.getResourceId());
      this.requestAccessionData(this.getResourceId());
    }
    
    public clean(){
      this.setAttachments([]);
      this.accession = {};
      this.diagnosticCodes = "";
    }
    
    public destroy(){}

    requestAttachments(resourceId:string){
      let params: any = {
        type: this.getConfiguration().type,
        diagnosticReportId: resourceId,
        _uuid: this.getUUID(),
        _callback: "updateAttachments"
      };
      
      let event: string = this.getMode() === this.MODE_MULTI ? "retrieveAttachments" : "retrieveSingleAttachment";
      this.backendService.sendBackendEvent(event, params);
    }

    requestAccessionData(resourceId:string){
      let params: any = {
        type: 'ACCESSION',
        id: resourceId,
        _uuid: this.getUUID(),
        _callback: "updateAccessionData"
      };

      //TODO: this event could be overkilling!      
      this.backendService.sendBackendEvent("retrieveDetailData", params);
      
    }
    
    updateAttachments(response: any){
      let newAttachments = [];
      
      if (this.getMode() === this.MODE_MULTI){
        let model = {
          "metadata": {},
          "data": [],
          "type": "creation"
        };

        if (response.payload && response.payload.length > 0){
          let index: number = 0;

          for (let at of response.payload){
            
            let bytes: any = base64js.toByteArray(Utilities.base64clean(at.base64Content));
            let blob: Blob =new Blob([bytes], { type: at.contentType });
            let fileURL = URL.createObjectURL(blob);
            let creationDate = moment(at.creation).format("MM-DD-YYYY HH:mm");
            let d = {
                creation: creationDate,
                contentType: at.contentType,
                index: ""+index,
                fileURL: fileURL,
                title: at.title
            };
            
            index++;
            model.data.push(d);
            
            //convert to attachment
            newAttachments.push({
              "title": at.title,
              "download": at.title+"."+at.defaultExtension
            });
          }
          
        }

        this.attachmentsGridRef.setModel(model);
      } else {
        if (response.payload){
          let at = response.payload;
          
          let content: string = window.atob(at.base64Content);
          newAttachments.push({
            "title": at.title,
            "content": content
          });
        }
      }
      
      this.setAttachments(newAttachments);
    }
    
    updateAccessionData(response: any){
      let accessionId: string= response.payload.accessionNumber;
      let accessionDate: string = _.isEmpty(response.payload.accessionDateTime)? "" : moment(response.payload.accessionDateTime, 'YYYYMMDDHHmm').format('MM-DD-YYYY HH:mm');
      
      this.accession = {
        "id": accessionId,
        "date": accessionDate
      };
      
      if (response.payload.diagnosticCodes && _.isArray(response.payload.diagnosticCodes)) {
        this.diagnosticCodes = response.payload.diagnosticCodes.map(dc => dc.display + "("+dc.code +")").join(", ");
      }
    }
    
    setAttachments(attachments){
      this.attachments = attachments;
    }
    
    @ViewChild('attachmentsGrid') set content(content: TableComponent){
      this.attachmentsGridRef = content;
    }
    
    private getMode(): string{
      return !this.getConfiguration().mode || this.getConfiguration().mode === "multi" ? this.MODE_MULTI : this.MODE_SINGLE;
    }
}
