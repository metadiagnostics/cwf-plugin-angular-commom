"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var accession_detail_module_1 = require("../../accession-detail/accession-detail.module");
var table_module_1 = require("../../table/table.module");
var lightbox_1 = require("primeng/components/lightbox/lightbox");
var md_attachments_component_1 = require("./md-attachments.component");
var MDAttachmentsModule = /** @class */ (function () {
    function MDAttachmentsModule() {
    }
    MDAttachmentsModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                lightbox_1.LightboxModule,
                accession_detail_module_1.AccessionDetailModule,
                table_module_1.TableModule
            ],
            declarations: [
                md_attachments_component_1.MDAttachmentsComponent
            ],
            entryComponents: [
                md_attachments_component_1.MDAttachmentsComponent
            ],
            exports: [
                md_attachments_component_1.MDAttachmentsComponent
            ]
        })
    ], MDAttachmentsModule);
    return MDAttachmentsModule;
}());
exports.MDAttachmentsModule = MDAttachmentsModule;
//# sourceMappingURL=md-attachments.module.js.map