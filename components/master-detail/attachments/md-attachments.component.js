"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var _ = require("lodash");
var moment = require("moment");
var base64js = require("base64-js");
var table_component_1 = require("../../../components/table/table.component");
var table_grid_renderer_1 = require("../../../components/table/table.grid.renderer");
var md_abstract_component_1 = require("../md-abstract.component");
var ibackend_service_1 = require("../../../services/ibackend.service");
var utilities_1 = require("../../../misc/utilities");
var attachment_model_processor_1 = require("./attachment.model.processor");
var MDAttachmentsComponent = /** @class */ (function (_super) {
    __extends(MDAttachmentsComponent, _super);
    function MDAttachmentsComponent(backendService, sanitizer) {
        var _this = _super.call(this) || this;
        _this.backendService = backendService;
        _this.sanitizer = sanitizer;
        _this.MODE_SINGLE = "_MODE_SINGLE_";
        _this.MODE_MULTI = "_MODE_MULTI_";
        _this.attachments = [];
        _this.accession = {};
        _this.diagnosticCodes = "";
        return _this;
    }
    MDAttachmentsComponent.prototype.ngOnInit = function () { };
    MDAttachmentsComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        if (this.getMode() === this.MODE_MULTI) {
            var modelProcessor = new attachment_model_processor_1.AttachmentModelProcessor([{
                    "field": "title",
                    "headerName": "Attachment"
                }, {
                    "field": "creation",
                    "headerName": "Date/Time"
                }]);
            var renderer = new table_grid_renderer_1.TableGrid();
            var options = {
                onGridReady: function () {
                    _this.refresh();
                },
                onRowClicked: function (event) {
                    if (_.get(event, 'data.contentType', '').indexOf('jpeg') > -1) {
                        var imageElem = _this.lightboxImageContent.nativeElement;
                        imageElem.setAttribute('src', _.get(event, 'data.fileURL'));
                        _this.lightboxImageLink.nativeElement.click();
                    }
                    else {
                        var frameElem = _this.lightboxDataContent.nativeElement;
                        frameElem.setAttribute('type', _.get(event, 'data.contentType'));
                        frameElem.setAttribute('data', _.get(event, 'data.fileURL'));
                        _this.lightboxContentLink.nativeElement.click();
                    }
                },
                rowStyle: {
                    cursor: 'pointer'
                }
            };
            this.attachmentsGridRef.initialize(modelProcessor, renderer, options);
        }
    };
    MDAttachmentsComponent.prototype.init = function () {
        if (this.getMode() === this.MODE_SINGLE) {
            this.refresh();
        }
    };
    MDAttachmentsComponent.prototype.refresh = function () {
        this.requestAttachments(this.getResourceId());
        this.requestAccessionData(this.getResourceId());
    };
    MDAttachmentsComponent.prototype.clean = function () {
        this.setAttachments([]);
        this.accession = {};
        this.diagnosticCodes = "";
    };
    MDAttachmentsComponent.prototype.destroy = function () { };
    MDAttachmentsComponent.prototype.requestAttachments = function (resourceId) {
        var params = {
            type: this.getConfiguration().type,
            diagnosticReportId: resourceId,
            _uuid: this.getUUID(),
            _callback: "updateAttachments"
        };
        var event = this.getMode() === this.MODE_MULTI ? "retrieveAttachments" : "retrieveSingleAttachment";
        this.backendService.sendBackendEvent(event, params);
    };
    MDAttachmentsComponent.prototype.requestAccessionData = function (resourceId) {
        var params = {
            type: 'ACCESSION',
            id: resourceId,
            _uuid: this.getUUID(),
            _callback: "updateAccessionData"
        };
        //TODO: this event could be overkilling!      
        this.backendService.sendBackendEvent("retrieveDetailData", params);
    };
    MDAttachmentsComponent.prototype.updateAttachments = function (response) {
        var newAttachments = [];
        if (this.getMode() === this.MODE_MULTI) {
            var model = {
                "metadata": {},
                "data": [],
                "type": "creation"
            };
            if (response.payload && response.payload.length > 0) {
                var index = 0;
                for (var _i = 0, _a = response.payload; _i < _a.length; _i++) {
                    var at = _a[_i];
                    var bytes = base64js.toByteArray(utilities_1.Utilities.base64clean(at.base64Content));
                    var blob = new Blob([bytes], { type: at.contentType });
                    var fileURL = URL.createObjectURL(blob);
                    var creationDate = moment(at.creation).format("MM-DD-YYYY HH:mm");
                    var d = {
                        creation: creationDate,
                        contentType: at.contentType,
                        index: "" + index,
                        fileURL: fileURL,
                        title: at.title
                    };
                    index++;
                    model.data.push(d);
                    //convert to attachment
                    newAttachments.push({
                        "title": at.title,
                        "download": at.title + "." + at.defaultExtension
                    });
                }
            }
            this.attachmentsGridRef.setModel(model);
        }
        else {
            if (response.payload) {
                var at = response.payload;
                var content = window.atob(at.base64Content);
                newAttachments.push({
                    "title": at.title,
                    "content": content
                });
            }
        }
        this.setAttachments(newAttachments);
    };
    MDAttachmentsComponent.prototype.updateAccessionData = function (response) {
        var accessionId = response.payload.accessionNumber;
        var accessionDate = _.isEmpty(response.payload.accessionDateTime) ? "" : moment(response.payload.accessionDateTime, 'YYYYMMDDHHmm').format('MM-DD-YYYY HH:mm');
        this.accession = {
            "id": accessionId,
            "date": accessionDate
        };
        if (response.payload.diagnosticCodes && _.isArray(response.payload.diagnosticCodes)) {
            this.diagnosticCodes = response.payload.diagnosticCodes.map(function (dc) { return dc.display + "(" + dc.code + ")"; }).join(", ");
        }
    };
    MDAttachmentsComponent.prototype.setAttachments = function (attachments) {
        this.attachments = attachments;
    };
    Object.defineProperty(MDAttachmentsComponent.prototype, "content", {
        set: function (content) {
            this.attachmentsGridRef = content;
        },
        enumerable: true,
        configurable: true
    });
    MDAttachmentsComponent.prototype.getMode = function () {
        return !this.getConfiguration().mode || this.getConfiguration().mode === "multi" ? this.MODE_MULTI : this.MODE_SINGLE;
    };
    __decorate([
        core_1.ViewChild('lightboxDataContent'),
        __metadata("design:type", Object)
    ], MDAttachmentsComponent.prototype, "lightboxDataContent", void 0);
    __decorate([
        core_1.ViewChild('lightboxImageContent'),
        __metadata("design:type", Object)
    ], MDAttachmentsComponent.prototype, "lightboxImageContent", void 0);
    __decorate([
        core_1.ViewChild('lightboxImageLink'),
        __metadata("design:type", Object)
    ], MDAttachmentsComponent.prototype, "lightboxImageLink", void 0);
    __decorate([
        core_1.ViewChild('lightboxContentLink'),
        __metadata("design:type", Object)
    ], MDAttachmentsComponent.prototype, "lightboxContentLink", void 0);
    __decorate([
        core_1.ViewChild('attachmentsGrid'),
        __metadata("design:type", table_component_1.TableComponent),
        __metadata("design:paramtypes", [table_component_1.TableComponent])
    ], MDAttachmentsComponent.prototype, "content", null);
    MDAttachmentsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            templateUrl: 'md-attachments.component.html',
            styleUrls: ['./md-attachments.component.css']
        }),
        __metadata("design:paramtypes", [ibackend_service_1.IBackendService,
            platform_browser_1.DomSanitizer])
    ], MDAttachmentsComponent);
    return MDAttachmentsComponent;
}(md_abstract_component_1.MDAbstractComponent));
exports.MDAttachmentsComponent = MDAttachmentsComponent;
//# sourceMappingURL=md-attachments.component.js.map