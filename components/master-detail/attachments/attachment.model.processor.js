"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var table_model_processor_1 = require("../../table/table.model.processor");
var AttachmentModelProcessor = /** @class */ (function (_super) {
    __extends(AttachmentModelProcessor, _super);
    function AttachmentModelProcessor() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AttachmentModelProcessor.prototype.process = function (model) {
        if (_.isNil(model)) {
            console.error('can not process an empty model');
            return;
        }
        this.rows = model.data;
    };
    return AttachmentModelProcessor;
}(table_model_processor_1.TableModelProcessor));
exports.AttachmentModelProcessor = AttachmentModelProcessor;
//# sourceMappingURL=attachment.model.processor.js.map