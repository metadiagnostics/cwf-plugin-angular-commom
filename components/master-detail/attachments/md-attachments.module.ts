import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {AccessionDetailModule} from "../../accession-detail/accession-detail.module";
import {TableModule} from "../../table/table.module";
import {LightboxModule} from "primeng/components/lightbox/lightbox";
import {MDAttachmentsComponent} from "./md-attachments.component";

@NgModule({
    imports: [
        CommonModule,
        LightboxModule,
        AccessionDetailModule,
        TableModule
    ],
    declarations: [
        MDAttachmentsComponent
    ],
    entryComponents: [
        MDAttachmentsComponent
    ],
    exports: [
        MDAttachmentsComponent
    ]
})
export class MDAttachmentsModule{}