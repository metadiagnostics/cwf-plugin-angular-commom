import { TableModelProcessor } from "../../table/table.model.processor";
export declare class AttachmentModelProcessor extends TableModelProcessor {
    process(model: any): void;
}
