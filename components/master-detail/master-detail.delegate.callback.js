"use strict";
/*
 *  Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Implementation of MasterDetailCalllback that delegates all the invocations
 * to a MasterDetailComponent component.
 */
var MasterDetailDelegateCalllback = /** @class */ (function () {
    function MasterDetailDelegateCalllback() {
    }
    MasterDetailDelegateCalllback.prototype.setMasterDetailComponent = function (cmp) {
        this.cmp = cmp;
    };
    MasterDetailDelegateCalllback.prototype.updateMasterGrid = function (model) {
        this.cmp.updateMasterGrid(model);
    };
    MasterDetailDelegateCalllback.prototype.updateDetailGrid = function (model) {
        this.cmp.updateDetailGrid(model);
    };
    MasterDetailDelegateCalllback.prototype.updateAttachments = function (attachments) {
        this.cmp.updateAttachments(attachments);
    };
    MasterDetailDelegateCalllback.prototype.activatePlugin = function (response) {
        this.cmp.activatePlugin(response);
    };
    MasterDetailDelegateCalllback.prototype.updateAccessionData = function (response) {
        this.cmp.updateAccessionData(response);
    };
    MasterDetailDelegateCalllback.prototype.updateNarrative = function (response) {
        this.cmp.updateNarrative(response);
    };
    MasterDetailDelegateCalllback.prototype.updateFormDefinition = function (response) {
        this.cmp.updateFormDefinition(response);
    };
    MasterDetailDelegateCalllback.prototype.updateFormResponses = function (response) {
        this.cmp.updateFormResponses(response);
    };
    MasterDetailDelegateCalllback.prototype.liveUpdateMasterGrid = function (model) {
        this.cmp.liveUpdateMasterGrid(model);
    };
    return MasterDetailDelegateCalllback;
}());
exports.MasterDetailDelegateCalllback = MasterDetailDelegateCalllback;
//# sourceMappingURL=master-detail.delegate.callback.js.map