"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var master_detail_component_1 = require("./master-detail.component");
var date_selection_module_1 = require("../date-selection/date-selection.module");
var button_1 = require("primeng/components/button/button");
var table_module_1 = require("../table/table.module");
var smart_form_module_1 = require("../smart-form/smart-form.module");
var forms_1 = require("@angular/forms");
var tabview_1 = require("primeng/components/tabview/tabview");
var ng2_split_pane_1 = require("ng2-split-pane/lib/ng2-split-pane");
var ibackend_provider_1 = require("../../providers/ibackend.provider");
var md_attachments_module_1 = require("./attachments/md-attachments.module");
var md_detail_table_module_1 = require("./detail-table/md-detail-table.module");
var md_narrative_module_1 = require("./narrative/md-narrative.module");
var md_smart_form_module_1 = require("./smart-form/md-smart-form.module");
var create_resource_module_1 = require("../create-resource/create-resource.module");
var data_filter_module_1 = require("../data-filter/data-filter.module");
var MasterDetailModule = /** @class */ (function () {
    function MasterDetailModule() {
    }
    MasterDetailModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                tabview_1.TabViewModule,
                ng2_split_pane_1.SplitPaneModule,
                date_selection_module_1.DateSelectionModule,
                data_filter_module_1.DataFilterModule,
                button_1.ButtonModule,
                table_module_1.TableModule,
                smart_form_module_1.SmartFormModule,
                md_attachments_module_1.MDAttachmentsModule,
                md_detail_table_module_1.MDDetailTableModule,
                md_narrative_module_1.MDNarrativeModule,
                md_smart_form_module_1.MDSmartFormModule,
                create_resource_module_1.CreateResourceModule
            ],
            declarations: [
                master_detail_component_1.MasterDetailComponent
            ],
            exports: [
                master_detail_component_1.MasterDetailComponent
            ],
            providers: [
                ibackend_provider_1.ibackendProvider
            ]
        })
    ], MasterDetailModule);
    return MasterDetailModule;
}());
exports.MasterDetailModule = MasterDetailModule;
//# sourceMappingURL=master-detail.module.js.map