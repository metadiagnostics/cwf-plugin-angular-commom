/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Component, ComponentRef, Input, QueryList, ViewChild, ViewChildren, ViewContainerRef, ViewEncapsulation, ComponentFactoryResolver} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Constants } from './constants';

import * as _ from 'lodash';
import * as moment from 'moment';

import { IBackendService } from '../../services/ibackend.service';

import { Guid, Utilities } from '../../misc/utilities';

import { MasterDetailCalllback }  from './master-detail.callback';

import { MDAbstractComponent }  from './md-abstract.component';
import { MDAttachmentsComponent }  from './attachments/md-attachments.component';
import { MDDetailTableComponent }  from './detail-table/md-detail-table.component';
import { MDNarrativeComponent } from './narrative/md-narrative.component';
import { MDSmartFormComponent }  from './smart-form/md-smart-form.component';

import { DataFilterService} from '../../components/data-filter/data-filter.service';

import { TableModelProcessor } from '../../components/table/table.model.processor';
import { TableGrid } from '../../components/table/table.grid.renderer';
import { CreateResourceService } from '../../services/create-resource.service';
import { GridPaginationService } from '../../services/grid.pagination.service';

@Component({
  moduleId   : module.id,
  selector   : 'master-detail',
  templateUrl: 'master-detail.component.html',
  styleUrls: ['./master-detail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MasterDetailComponent implements MasterDetailCalllback {

    @Input()
    config: any;

    @ViewChild('grid')
    private grid;

    @ViewChild('dateSelection')
    private dateSelectionComponent;

    @ViewChild('detailPanel')
    private detailPanel;

    @ViewChild('createResource')
    private createResourceComponent;
    
    @ViewChildren('dynDiv', {read: ViewContainerRef})
    private detailTabs: QueryList<ViewContainerRef>;

    private totalNumberOfRecords: number = 0;
    private currentPageIndex: number = 0;
    private pageSize : number = 0;

    private currentDataFilter: any;
    private currentSelectedRowId: string = undefined;
    private selectedDate: Date;
    private createModeEnabled: boolean;

    detailSelectedIndex: number = 0;
    
    /**
     * This maps contains the relation between tab index and detail component.
     */
    private detailComponentsRendered: Map<number, string> = new Map();
    private detailComponents: Map<string, ComponentRef<MDAbstractComponent>> = new Map();
    
    constructor(private backendService: IBackendService,
                private dataFilterService: DataFilterService,
                private createResourceService: CreateResourceService,
                private gridPaginationService: GridPaginationService,
                private sanitizer:DomSanitizer,
                private componentFactoryResolver:ComponentFactoryResolver) {

        this.selectedDate = moment().toDate();
    }

    ngOnInit() {
        this.initializeHeaderGrid();
    }

    ngAfterViewInit() {
        this.createResourceService.closeResourceFormEvent.subscribe((domain) => {
            if (domain != null) {
                this.createResourceComponent.closeForm();
                this.createModeEnabled = false;
            }
        });

        this.gridPaginationService.pageNumber$.subscribe((pageIndex) => {
            if (pageIndex === null || pageIndex < 1 || this.currentPageIndex === pageIndex) {
                return;
            }

            // console.log(`I have told to change page to ${pageIndex} `);
            this.currentPageIndex = pageIndex;
            this.setupAndRequestData();
        });

        //HEADER Section Configuration
        if (this.config.sections.header){

          if (this.config.sections.header.dateSelection) {
              this.dateSelectionComponent.dateChanged$.subscribe(date => {
                  if (date != null) {
                      this.selectedDate = date;
                      this.resetPagingSettings();
                      this.setupAndRequestData();
                  }
              });
          }
          
          if (this.config.sections.header.filterButtons || this.config.sections.header.secondaryDropdownConfig){
            this.currentDataFilter = {};
            _.assign(this.currentDataFilter, _.get(this, 'config.sections.header.filterButtons[0].params'));
            _.assign(this.currentDataFilter, _.get(this, 'config.sections.header.secondaryDropdownConfig.options[0].params'));

            this.dataFilterService.dataFilterChangedSource$.subscribe(model => {
                // && !_.isEqual(model, this.currentDataFilter)
                if (model !== null) {
                    this.currentDataFilter = model;
                    this.resetPagingSettings();
                    this.setupAndRequestData();
                }
            });
          }
          
        }
        
        if (this.config.sections.details.tabs) {
            this.detailPanel.onChange.subscribe(e => {
                if (e != null) {
                    this.onDetailTabSelected(e.index);
                }
            });
        }
    }

    private setupAndRequestData() {
        this.resetDetailSection();
        this.grid.showLoadingIndicator();
        this.requestData();
    }

    private onDetailTabSelected(index:number){
      let uuid: string = this.detailComponentsRendered.get(index);
      
      if (_.isUndefined(this.currentSelectedRowId) || !this.config.sections.details || !this.config.sections.details.tabs || this.config.sections.details.tabs.length == 0){
        return;
      }
      
      if (_.isUndefined(uuid)){
        
        //If we don't have any component for that tab, we have to initialize it.
        let cmpRef: ComponentRef<MDAbstractComponent> = this.initializeTab(index);
        
        //And create the mappings
        this.detailComponents.set(cmpRef.instance.getUUID(), cmpRef);
        this.detailComponentsRendered.set(index, cmpRef.instance.getUUID());
        
        cmpRef.instance.init();
        
      } else {
        this.detailComponents.get(uuid).instance.refresh();
      }
    }

    private initializeTab(index:number): ComponentRef<MDAbstractComponent>{
      let tab = this.detailPanel.findSelectedTab();
      let tabCfg: any = this.config.sections.details.tabs[index];
      let cmpRef: ComponentRef<MDAbstractComponent>;
      
      let tabDiv = this.detailTabs.find(d => {
        return ('dynDiv-'+index) == d.element.nativeElement.className;
      });
      
      
      if (tabCfg.type === 'DETAILS_TABLE'){
        cmpRef = tabDiv.createComponent(this.componentFactoryResolver.resolveComponentFactory(MDDetailTableComponent), 0);
      } else if (tabCfg.type === 'ATTACHMENTS'){
        cmpRef = tabDiv.createComponent(this.componentFactoryResolver.resolveComponentFactory(MDAttachmentsComponent), 0);
      } else if (tabCfg.type === 'NARRATIVE') {
          cmpRef = tabDiv.createComponent(this.componentFactoryResolver.resolveComponentFactory(MDNarrativeComponent), 0);

          //TODO: Might be a better way to do this
          let selectedRows = this.grid.gridOptions.api.getSelectedRows();

          if (_.isArray(selectedRows) && selectedRows.length > 0) {
              cmpRef.instance.setResourceSubType(selectedRows[0].classType);
          }
      } else if (tabCfg.type === 'SMART_FORM'){
        cmpRef = tabDiv.createComponent(this.componentFactoryResolver.resolveComponentFactory(MDSmartFormComponent), 0);
      }
      
      let uuid: string = Guid.newGuid();
      cmpRef.instance.setUUID(uuid);
      cmpRef.instance.setResourceId(this.currentSelectedRowId);
      cmpRef.instance.setConfiguration(tabCfg);
      
      return cmpRef;
    }
    
    private initializeHeaderGrid() {
        let gridOptions = {
            onGridReady: () => {
                if (Utilities.isRunningStandalone()) {
                    this.requestData();
                }
            },
            onSelectionChanged: (event) => {
                let selectedRows = event.api.getSelectedRows();

                if (_.isArray(selectedRows) && selectedRows.length > 0) {
                  this.currentSelectedRowId = selectedRows[0].id;
                  
                  this.resetDetailSection();

                  if (this.config.sections.details.tabs) {
                      //refresh first tab
                      //TODO: actually change to the first tab
                      this.detailPanel.activeIndex = 0;
                      this.detailSelectedIndex = 0;
                      this.onDetailTabSelected(0);
                  }
                }
            },
            onRowClicked: (event) => {
                if (_.isFunction(this.config.sections.header.shouldOpenDrawer) && this.config.sections.header.shouldOpenDrawer(event.data)) {
                    this.backendService.sendBackendEvent('openDrawer', {
                        dataType: this.config.type,
                        resourceId: _.get(event, 'data.id'),
                        '_callback': 'openDrawer'
                    });
                }
            }
        };

        _.assign(gridOptions, this.config.sections.header.gridOptions);

        let modelProcessor = new TableModelProcessor(this.config.sections.header.columns);
        let renderer = new TableGrid();

        if (this.isPagingServerSide()) {
            this.pageSize = this.getDefaultPageSize();
            this.grid.setServerSidePaging(true);
        }

        this.grid.initialize(modelProcessor, renderer, gridOptions, true, 50, true);
    }

    requestData() {
        this.grid.setModel({data: []});
        this.resetDetailSection();
        this.grid.showLoadingIndicator();

        if (_.isEmpty(this.backendService.getRootNativeElement())) {
            this.backendService.setRootNativeElement(this.config.root.nativeElement);
        }

        let params = this.getModelQueryParams();
        
        params._callback = 'updateMasterGrid';
        params._liveUpdateCallback = 'liveUpdateMasterGrid';
        this.backendService.sendBackendEvent('retrieveData', params);
    }
    
    private getModelQueryParams() : any {
        let retVal: any = {
            type: this.config.type
            , categories: this.config.sections.header.categories
        };
        
        if (this.config.sections.header.dateSelection) {
            retVal.startTime = this.getFormattedDateParam(this.selectedDate, -90);
            retVal.endTime = this.getFormattedDateParam(this.selectedDate, 90);
        }

        if (!_.isUndefined(this.currentDataFilter)) {
            _.assign(retVal, this.currentDataFilter);
        }

        if (!_.isEmpty(this.config.extraParams)){
            _.assign(retVal, this.config.extraParams);
        }

        if (this.isPagingServerSide()) {
            retVal = this.adjustPagingParams(retVal);
        }

        return retVal;
    }

    private adjustPagingParams(input: any) : any {
        input.pageOffset = (this.getCurrentPageIndex() - 1 ) * this.pageSize;
        input.count = this.pageSize;

        console.log(` asking for data page offset is ${input.pageOffset}`);

        return input;
    };

    private isPagingServerSide() : boolean {
        return _.get(this, 'config.pagination.strategy') === Constants.SERVER;
    }

    private resetPagingSettings() : void {
        if (this.isPagingServerSide()) {
            this.resetPageIndex();
        }
    }

    private getDefaultPageSize() : number {
        return _.get(this, 'config.pagination.pageSize', 0);
    }


    private getFormattedDateParam(date: Date, daysToAdd: number) : string {
        return moment(date).add(daysToAdd, 'day').format('YYYYMMDD');
    }

    private createNewItem() {
        this.createModeEnabled = true;
        this.createResourceComponent.initForm(_.get(this, 'config.sections.createButton.domain'));
    }

    public getCurrentPageIndex() : number {
        return this.currentPageIndex === 0 ? 1 : this.currentPageIndex;
    }

    public resetPageIndex() : void {
        this.currentPageIndex = 0;
        this.gridPaginationService.setPageNumber(0);
        this.gridPaginationService.setTotalPages(0);
    }

    // Server callback for we asked for master data
    updateMasterGrid(response: any) {
      
        let model: any = response.payload;
        this.grid.setModel(model);

        if (_.get(model, 'data.length', 0) > 0) {
            this.grid.selectRow(0);
        }

        if (this.isPagingServerSide()) {
            this.totalNumberOfRecords = response.payload.totalRows;
            this.gridPaginationService.setTotalPages(this.totalNumberOfRecords/this.pageSize);
        }
    }
    
    // Server callback for we asked for detail data
    updateDetailGrid(response: any) {
        let uuid:string = response._uuid;
        //TODO: unsafe invocation of 'updateDetailGrid'
        let compRef = this.detailComponents.get(uuid);
        if(!_.isUndefined(compRef) && compRef.instance) {
            compRef.instance['updateDetailGrid'].apply(compRef.instance, [response]);
        }
    }
    
    
    //Server callback
    updateAttachments(response: any){
      let uuid:string = response._uuid;
      //TODO: unsafe invocation of 'updateAttachments'
      let instance = this.detailComponents.get(uuid).instance;
      instance['updateAttachments'].apply(instance, [response]);
    }
    
    //Server callback
    updateAccessionData(response: any){
      let uuid:string = response._uuid;
      //TODO: unsafe invocation of 'updateAccessionData'
      let instance = this.detailComponents.get(uuid).instance;
      instance['updateAccessionData'].apply(instance, [response]);
    }

    updateNarrative(response: any) {
        let instance = this.detailComponents.get(response._uuid).instance;
        instance['updateNarrative'].apply(instance, [response]);
    }

    //Server callback
    updateFormDefinition(response: any){
        if (this.createModeEnabled) {
            this.createResourceComponent.updateFormDefinition(response);
        } else {
            let uuid:string = response._uuid;
            //TODO: unsafe invocation of 'updateFormDefinition'
            let instance = this.detailComponents.get(uuid).instance;
            instance['updateFormDefinition'].apply(instance, [response]);
        }
    }
    
    //Server callback
    updateFormResponses(response: any){
      let uuid:string = response._uuid;
      //TODO: unsafe invocation of 'updateFormResponses'
      let instance = this.detailComponents.get(uuid).instance;

      if (!_.isUndefined(instance)) {
          instance['updateFormResponses'].apply(instance, [response]);
      }
    }

    // Server callback when plugin is activated
    activatePlugin(response) {
      //This callback is not propagated to the components.
        if (_.get(response, 'payload.dataChanged')) {
            this.requestData();
        }
    }

    // Server live update callback for master data
    liveUpdateMasterGrid(response: any) {
        let model: any = response.payload;
        this.grid.updateModel(model);
        let selectedRows = this.grid.gridOptions.api.getSelectedRows();

        if (!_.isArray(selectedRows) || selectedRows.length === 0) {
            this.grid.selectRow(0);
        }
    }

    resetDetailSection() {
      this.detailComponents.forEach((cmpRef: ComponentRef<MDAbstractComponent>, uuid: string) => {
        cmpRef.instance.destroy();
        cmpRef.destroy();
      });
      
      this.detailComponentsRendered.clear();
      this.detailComponents.clear();
    }
    
}
