import {NgModule} from "@angular/core";
import {FormsModule} from '@angular/forms'
import {CommonModule} from "@angular/common";
import {SmartFormModule} from "../../smart-form/smart-form.module";
import {MDSmartFormComponent} from "./md-smart-form.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SmartFormModule
    ],
    declarations: [
      MDSmartFormComponent
    ],
    entryComponents: [
      MDSmartFormComponent
    ],
    exports: [
      MDSmartFormComponent
    ]
})
export class MDSmartFormModule{}