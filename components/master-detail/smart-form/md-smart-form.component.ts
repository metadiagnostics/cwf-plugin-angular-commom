/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import * as _ from 'lodash';
import * as moment from 'moment';
import * as base64js from "base64-js"

import { MDAbstractComponent } from '../md-abstract.component';

import { IBackendService } from '../../../services/ibackend.service';

import {Utilities} from "../../../misc/utilities";


@Component({
  moduleId   : module.id,
  templateUrl: 'md-smart-form.component.html',
  styleUrls: ['./md-smart-form.component.css']
})
export class MDSmartFormComponent extends MDAbstractComponent {
  
    protected smartForm;
    protected responses = {};
    protected writebackMode: string;

    constructor(private backendService: IBackendService,
                private sanitizer:DomSanitizer) {
      super();
    }

    ngOnInit() {}
      
    ngAfterViewInit() {
    }
    
    public init(){
      this.writebackMode = this.getConfiguration().writebackMode;
      this.refresh();
    }
    
    public refresh(){
      this.clean();
      this.requestFormDefinition('gutchecknec', '1.0');
    }
    
    public clean(){
        this.smartForm = undefined;
        this.responses = {};
    }
    
    public destroy(){}

    requestFormDefinition(resourceId:string, formVersion: string){
      //The resourceId is the id of the Response and not of the form itself.
      let params: any = {
        type: this.getConfiguration().type,
        formType: resourceId,
        formVersion: formVersion,
        _uuid: this.getUUID(),
        _callback: "updateFormDefinition"
      };
      
      let event: string = "requestFormDefinition";
      this.backendService.sendBackendEvent(event, params);
    }
    
    requestFormResponses(resourceId:string){
      //The resourceId is the id of the Response and not of the form itself.
      let params: any = {
        type: this.getConfiguration().type,
        formResponseId: resourceId,
        _uuid: this.getUUID(),
        _callback: "updateFormResponses"
      };
      
      let event: string = "requestFormResponses";
      this.backendService.sendBackendEvent(event, params);
    }

    updateFormDefinition(response: any){
      this.smartForm = response.payload;
      if (response.payload){
        this.requestFormResponses(this.getResourceId());
      }
    }
    
    updateFormResponses(response: any){
      this.responses = response.payload;
    }
    
}
