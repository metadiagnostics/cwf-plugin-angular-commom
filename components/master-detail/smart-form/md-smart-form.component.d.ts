import { DomSanitizer } from '@angular/platform-browser';
import { MDAbstractComponent } from '../md-abstract.component';
import { IBackendService } from '../../../services/ibackend.service';
export declare class MDSmartFormComponent extends MDAbstractComponent {
    private backendService;
    private sanitizer;
    protected smartForm: any;
    protected responses: {};
    protected writebackMode: string;
    constructor(backendService: IBackendService, sanitizer: DomSanitizer);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    init(): void;
    refresh(): void;
    clean(): void;
    destroy(): void;
    requestFormDefinition(resourceId: string, formVersion: string): void;
    requestFormResponses(resourceId: string): void;
    updateFormDefinition(response: any): void;
    updateFormResponses(response: any): void;
}
