"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var md_abstract_component_1 = require("../md-abstract.component");
var ibackend_service_1 = require("../../../services/ibackend.service");
var MDSmartFormComponent = /** @class */ (function (_super) {
    __extends(MDSmartFormComponent, _super);
    function MDSmartFormComponent(backendService, sanitizer) {
        var _this = _super.call(this) || this;
        _this.backendService = backendService;
        _this.sanitizer = sanitizer;
        _this.responses = {};
        return _this;
    }
    MDSmartFormComponent.prototype.ngOnInit = function () { };
    MDSmartFormComponent.prototype.ngAfterViewInit = function () {
    };
    MDSmartFormComponent.prototype.init = function () {
        this.writebackMode = this.getConfiguration().writebackMode;
        this.refresh();
    };
    MDSmartFormComponent.prototype.refresh = function () {
        this.clean();
        this.requestFormDefinition('gutchecknec', '1.0');
    };
    MDSmartFormComponent.prototype.clean = function () {
        this.smartForm = undefined;
        this.responses = {};
    };
    MDSmartFormComponent.prototype.destroy = function () { };
    MDSmartFormComponent.prototype.requestFormDefinition = function (resourceId, formVersion) {
        //The resourceId is the id of the Response and not of the form itself.
        var params = {
            type: this.getConfiguration().type,
            formType: resourceId,
            formVersion: formVersion,
            _uuid: this.getUUID(),
            _callback: "updateFormDefinition"
        };
        var event = "requestFormDefinition";
        this.backendService.sendBackendEvent(event, params);
    };
    MDSmartFormComponent.prototype.requestFormResponses = function (resourceId) {
        //The resourceId is the id of the Response and not of the form itself.
        var params = {
            type: this.getConfiguration().type,
            formResponseId: resourceId,
            _uuid: this.getUUID(),
            _callback: "updateFormResponses"
        };
        var event = "requestFormResponses";
        this.backendService.sendBackendEvent(event, params);
    };
    MDSmartFormComponent.prototype.updateFormDefinition = function (response) {
        this.smartForm = response.payload;
        if (response.payload) {
            this.requestFormResponses(this.getResourceId());
        }
    };
    MDSmartFormComponent.prototype.updateFormResponses = function (response) {
        this.responses = response.payload;
    };
    MDSmartFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            templateUrl: 'md-smart-form.component.html',
            styleUrls: ['./md-smart-form.component.css']
        }),
        __metadata("design:paramtypes", [ibackend_service_1.IBackendService,
            platform_browser_1.DomSanitizer])
    ], MDSmartFormComponent);
    return MDSmartFormComponent;
}(md_abstract_component_1.MDAbstractComponent));
exports.MDSmartFormComponent = MDSmartFormComponent;
//# sourceMappingURL=md-smart-form.component.js.map