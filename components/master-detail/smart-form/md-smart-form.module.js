"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var common_1 = require("@angular/common");
var smart_form_module_1 = require("../../smart-form/smart-form.module");
var md_smart_form_component_1 = require("./md-smart-form.component");
var MDSmartFormModule = /** @class */ (function () {
    function MDSmartFormModule() {
    }
    MDSmartFormModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                smart_form_module_1.SmartFormModule
            ],
            declarations: [
                md_smart_form_component_1.MDSmartFormComponent
            ],
            entryComponents: [
                md_smart_form_component_1.MDSmartFormComponent
            ],
            exports: [
                md_smart_form_component_1.MDSmartFormComponent
            ]
        })
    ], MDSmartFormModule);
    return MDSmartFormModule;
}());
exports.MDSmartFormModule = MDSmartFormModule;
//# sourceMappingURL=md-smart-form.module.js.map