/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import {Component} from "@angular/core";
import {MDAbstractComponent} from "../md-abstract.component";
import {IBackendService} from "../../../services/ibackend.service";
import * as _ from 'lodash';

@Component({
    moduleId: module.id,
    templateUrl: './md-narrative.component.html'
})
export class MDNarrativeComponent extends MDAbstractComponent {
    private title: string;
    private narrative: string;
    private contentType: string;

    constructor(private backendService: IBackendService) {
        super();
    }

    init() {
        this.refresh();
    }

    refresh() {
        this.requestNarrative(this.getResourceId(), this.getResourceSubType());
    }

    clean() {
        this.title = '';
        this.narrative = '';
        this.contentType = '';
    }

    destroy() {}

    requestNarrative(resourceId: string, resourceType: string) {
        let params: any = {
            type: "NARRATIVE",
            _uuid: this.getUUID(),
            resourceId: resourceId,
            resourceType: resourceType,
            _callback: 'updateNarrative'
        };

        this.backendService.sendBackendEvent('retrieveNarrative', params);
    }

    updateNarrative(response: any) {
        this.title = _.get(response, 'payload.title', '');
        this.narrative = _.get(response, 'payload.narrative', 'No narrative available');
        this.contentType = _.get(response, 'payload.contentType', 'text');
    }

}