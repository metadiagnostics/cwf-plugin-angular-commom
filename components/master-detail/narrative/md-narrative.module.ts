import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {MDNarrativeComponent} from "./md-narrative.component";

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        MDNarrativeComponent
    ],
    entryComponents: [
        MDNarrativeComponent
    ],
    exports: [
        MDNarrativeComponent
    ]
})
export class MDNarrativeModule{}