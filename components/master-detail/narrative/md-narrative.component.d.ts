import { MDAbstractComponent } from "../md-abstract.component";
import { IBackendService } from "../../../services/ibackend.service";
export declare class MDNarrativeComponent extends MDAbstractComponent {
    private backendService;
    private title;
    private narrative;
    private contentType;
    constructor(backendService: IBackendService);
    init(): void;
    refresh(): void;
    clean(): void;
    destroy(): void;
    requestNarrative(resourceId: string, resourceType: string): void;
    updateNarrative(response: any): void;
}
