"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var md_abstract_component_1 = require("../md-abstract.component");
var ibackend_service_1 = require("../../../services/ibackend.service");
var _ = require("lodash");
var MDNarrativeComponent = /** @class */ (function (_super) {
    __extends(MDNarrativeComponent, _super);
    function MDNarrativeComponent(backendService) {
        var _this = _super.call(this) || this;
        _this.backendService = backendService;
        return _this;
    }
    MDNarrativeComponent.prototype.init = function () {
        this.refresh();
    };
    MDNarrativeComponent.prototype.refresh = function () {
        this.requestNarrative(this.getResourceId(), this.getResourceSubType());
    };
    MDNarrativeComponent.prototype.clean = function () {
        this.title = '';
        this.narrative = '';
        this.contentType = '';
    };
    MDNarrativeComponent.prototype.destroy = function () { };
    MDNarrativeComponent.prototype.requestNarrative = function (resourceId, resourceType) {
        var params = {
            type: "NARRATIVE",
            _uuid: this.getUUID(),
            resourceId: resourceId,
            resourceType: resourceType,
            _callback: 'updateNarrative'
        };
        this.backendService.sendBackendEvent('retrieveNarrative', params);
    };
    MDNarrativeComponent.prototype.updateNarrative = function (response) {
        this.title = _.get(response, 'payload.title', '');
        this.narrative = _.get(response, 'payload.narrative', 'No narrative available');
        this.contentType = _.get(response, 'payload.contentType', 'text');
    };
    MDNarrativeComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            templateUrl: './md-narrative.component.html'
        }),
        __metadata("design:paramtypes", [ibackend_service_1.IBackendService])
    ], MDNarrativeComponent);
    return MDNarrativeComponent;
}(md_abstract_component_1.MDAbstractComponent));
exports.MDNarrativeComponent = MDNarrativeComponent;
//# sourceMappingURL=md-narrative.component.js.map