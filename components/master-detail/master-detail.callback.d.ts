/**
 * Interface that any CWF plugin using master-detail component must implement.
 *
 */
export interface MasterDetailCalllback {
    updateMasterGrid(response: any): any;
    updateDetailGrid(response: any): any;
    updateAttachments(attachments: any): any;
    updateAccessionData(response: any): any;
    updateNarrative(response: any): any;
    updateFormDefinition(response: any): any;
    updateFormResponses(response: any): any;
    activatePlugin(response: any): any;
    liveUpdateMasterGrid(response: any): any;
}
