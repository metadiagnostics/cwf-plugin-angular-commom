import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {AccessionDetailModule} from "../../accession-detail/accession-detail.module";
import {TableModule} from "../../table/table.module";
import {MDDetailTableComponent} from "./md-detail-table.component";

@NgModule({
    imports: [
        CommonModule,
        AccessionDetailModule,
        TableModule
    ],
    declarations: [
        MDDetailTableComponent
    ],
    entryComponents: [
        MDDetailTableComponent
    ],
    exports: [
        MDDetailTableComponent
    ]
})
export class MDDetailTableModule{}