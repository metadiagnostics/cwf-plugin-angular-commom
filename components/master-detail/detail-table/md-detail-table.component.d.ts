import { MDAbstractComponent } from '../md-abstract.component';
import { IBackendService } from '../../../services/ibackend.service';
export declare class MDDetailTableComponent extends MDAbstractComponent {
    private backendService;
    private detailGrid;
    private accession;
    constructor(backendService: IBackendService);
    ngOnInit(): void;
    init(): void;
    refresh(): void;
    clean(): void;
    destroy(): void;
    requestDetailData(resourceId: string): void;
    updateDetailGrid(response: any): void;
}
