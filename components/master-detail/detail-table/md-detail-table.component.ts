/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Component, ViewChild } from '@angular/core';

import { GridOptions } from 'ag-grid/main';

import * as _ from 'lodash';
import * as moment from 'moment';

import {TableModelProcessor} from "../../../components/table/table.model.processor";
import {TableGrid} from "../../../components/table/table.grid.renderer";

import { MDAbstractComponent } from '../md-abstract.component';

import { IBackendService } from '../../../services/ibackend.service';


@Component({
  moduleId   : module.id,
  templateUrl: 'md-detail-table.component.html'
})
export class MDDetailTableComponent extends MDAbstractComponent {
  
    @ViewChild('detailGrid')
    private detailGrid;
    
    private accession: any = {};

    constructor(private backendService: IBackendService) {
      super();
    }

    ngOnInit() {
      
    }
    
    public init(){
      let detailModelProcessor = new TableModelProcessor(this.getConfiguration().columns, this.getConfiguration().conditionalHeader);
      let detailRenderer = new TableGrid(); 

      let gridOptions: GridOptions = {
        onGridReady: () => {
          this.refresh();
        }
      };
            
      this.detailGrid.initialize(detailModelProcessor, detailRenderer, gridOptions);
    }
    
    public refresh(){
      this.requestDetailData(this.getResourceId());
    }
    
    public clean(){
      this.detailGrid.setModel({
          data: [],
          metadata: {
              uuid: this.detailGrid.getUUID()
          }
      });
    }
    
    public destroy(){
    }
    
    requestDetailData(resourceId: string) {
        let params: any = {
          type: this.getConfiguration().resourceType,
          id: resourceId,
          _uuid: this.getUUID(),
          _callback: "updateDetailGrid"
        };
        
        this.backendService.sendBackendEvent("retrieveDetailData", params);
    }
    
    updateDetailGrid(response: any) {
      
      let model: any = response.payload;
      
      this.detailGrid.setModel(model);
      
      
      let accessionId: string= model.accessionNumber;
      let accessionDate: string = _.isEmpty(model.accessionDateTime)? "" : moment(model.accessionDateTime, 'YYYYMMDDHHmm').format('MM-DD-YYYY HH:mm');
      
      this.accession = {
        "id": accessionId,
        "date": accessionDate
      };
    }

    
}
