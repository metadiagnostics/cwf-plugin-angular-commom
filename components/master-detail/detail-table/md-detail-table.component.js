"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var _ = require("lodash");
var moment = require("moment");
var table_model_processor_1 = require("../../../components/table/table.model.processor");
var table_grid_renderer_1 = require("../../../components/table/table.grid.renderer");
var md_abstract_component_1 = require("../md-abstract.component");
var ibackend_service_1 = require("../../../services/ibackend.service");
var MDDetailTableComponent = /** @class */ (function (_super) {
    __extends(MDDetailTableComponent, _super);
    function MDDetailTableComponent(backendService) {
        var _this = _super.call(this) || this;
        _this.backendService = backendService;
        _this.accession = {};
        return _this;
    }
    MDDetailTableComponent.prototype.ngOnInit = function () {
    };
    MDDetailTableComponent.prototype.init = function () {
        var _this = this;
        var detailModelProcessor = new table_model_processor_1.TableModelProcessor(this.getConfiguration().columns, this.getConfiguration().conditionalHeader);
        var detailRenderer = new table_grid_renderer_1.TableGrid();
        var gridOptions = {
            onGridReady: function () {
                _this.refresh();
            }
        };
        this.detailGrid.initialize(detailModelProcessor, detailRenderer, gridOptions);
    };
    MDDetailTableComponent.prototype.refresh = function () {
        this.requestDetailData(this.getResourceId());
    };
    MDDetailTableComponent.prototype.clean = function () {
        this.detailGrid.setModel({
            data: [],
            metadata: {
                uuid: this.detailGrid.getUUID()
            }
        });
    };
    MDDetailTableComponent.prototype.destroy = function () {
    };
    MDDetailTableComponent.prototype.requestDetailData = function (resourceId) {
        var params = {
            type: this.getConfiguration().resourceType,
            id: resourceId,
            _uuid: this.getUUID(),
            _callback: "updateDetailGrid"
        };
        this.backendService.sendBackendEvent("retrieveDetailData", params);
    };
    MDDetailTableComponent.prototype.updateDetailGrid = function (response) {
        var model = response.payload;
        this.detailGrid.setModel(model);
        var accessionId = model.accessionNumber;
        var accessionDate = _.isEmpty(model.accessionDateTime) ? "" : moment(model.accessionDateTime, 'YYYYMMDDHHmm').format('MM-DD-YYYY HH:mm');
        this.accession = {
            "id": accessionId,
            "date": accessionDate
        };
    };
    __decorate([
        core_1.ViewChild('detailGrid'),
        __metadata("design:type", Object)
    ], MDDetailTableComponent.prototype, "detailGrid", void 0);
    MDDetailTableComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            templateUrl: 'md-detail-table.component.html'
        }),
        __metadata("design:paramtypes", [ibackend_service_1.IBackendService])
    ], MDDetailTableComponent);
    return MDDetailTableComponent;
}(md_abstract_component_1.MDAbstractComponent));
exports.MDDetailTableComponent = MDDetailTableComponent;
//# sourceMappingURL=md-detail-table.component.js.map