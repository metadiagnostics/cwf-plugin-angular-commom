/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { GridOptions } from 'ag-grid/main';
import * as _ from 'lodash';

export interface Cell {
    timestamp: string;
    value: string;
    info: string;
}

export abstract class Grid {
    protected gridWidth: number = 0;
    protected gridHeight: number = 0;
    private rowHeight = 25;
    private pagingPanelHeight = 32;
    private elementId: string;
    private paging: boolean = false;
    private pageSize: number = 0;

    constructor() {}

    public initialize(gridOptions: GridOptions, elementId, paging?: boolean, pageSize? : number) {
        this.elementId = elementId;
        gridOptions.suppressLoadingOverlay = false;
        gridOptions.suppressNoRowsOverlay = false;
        gridOptions.columnDefs = [];
        gridOptions.rowData = [];
        if (!_.isUndefined(paging) && paging) {
            gridOptions.pagination = true;
            this.paging = true;
            if (!_.isUndefined(pageSize)) {
                gridOptions.paginationPageSize = pageSize;
                this.pageSize = pageSize;
            }
        }
        gridOptions.onGridSizeChanged = ($event) => {
            this.gridWidth = $event.clientWidth;
            this.gridHeight = $event.clientHeight;
            this.setGridHeight(gridOptions);
        };
    }

    public refreshGrid(gridOptions, rows, columns): void {
        if (_.isNil(gridOptions.api)) {
            console.error('should not be in refreshGrid');
            return;
        }

        // TODO there is currently no support for dynamically changing this setting.
        // Leave here for when they support it.

        if (this.pageSize == 0 ||  _.isEmpty(rows) ||    rows.length < this.pageSize) {
            // turn off the pagination settings...
            gridOptions.pagination = false;
        }

        gridOptions.rowData = this.configureRows(rows);
        gridOptions.columnDefs = this.configureColumns(columns);
        gridOptions.api.setColumnDefs(gridOptions.columnDefs);
        gridOptions.api.setRowData(gridOptions.rowData);
        this.setGridHeight(gridOptions);
    }

    protected abstract configureColumns(columns);
    protected abstract configureRows(rows);

    private isScrollarShowingGrid(gridOptions) {
        let visibleColumns = gridOptions.columnApi.getAllDisplayedColumns();
        let width = 0;
        _.forEach(visibleColumns, function(column) {
            width += column.getActualWidth();
        });

        let gridWidth = this.getGridWidth();
        let retVal = width > gridWidth;

        return retVal;
    }

    private getGridWidth(): number {
        let width = 1200;
        let el = document.getElementById(this.elementId);
        if (el) {
            let positionInfo = el.getBoundingClientRect();
            width = positionInfo.width;
        }

        return width;
    }

    protected setGridHeight(gridOptions: any) {

        let eGridDiv = document.getElementById(this.elementId);
        if (eGridDiv) {

            if (_.get(gridOptions, 'rowData.length', 0) > 0) {
                // row data + 1 to account for the column headers...
                let height = (gridOptions.rowData.length + 1) * this.rowHeight + 5;
                if (this.isScrollarShowingGrid(gridOptions)) {
                    height += 20;
                }

                if (height > 300) {
                    height = 300;
                }

                if (this.paging) {
                    height += this.pagingPanelHeight;
                }

                eGridDiv.style.height = height + 'px';
                this.gridHeight = height;
                // console.log(`grid height ${height} for ${gridOptions.rowData.length} rows`);
            } else if (!_.isUndefined(gridOptions.emptyHeight)) {
                eGridDiv.style.height = gridOptions.emptyHeight;
                this.gridHeight = gridOptions.emptyHeight;
            }
        }
    }

    public selectFirstRow(gridOptions: GridOptions) : void {
    }

    static selectRow(gridOptions: GridOptions, index: number) {
        let node = gridOptions.api.getDisplayedRowAtIndex(index);

        if (!_.isEmpty(node)) {
            node.setSelected(true, true);
        }
    }

    static dueOverDueCellRenderer(params) {
        var value = params.value;

        var eDivPercentBar = document.createElement('div');
        eDivPercentBar.className = 'div-percent-bar';
        eDivPercentBar.style.width = '100%';
        if (value === 'OVERDUE') {
            eDivPercentBar.style.backgroundColor = '#FF3333';
        } else if (value === 'DUE') {
            eDivPercentBar.style.backgroundColor = '#66FF66';
        }

        var eValue = document.createElement('div');
        eValue.className = 'div-percent-value';
        eValue.innerHTML = value;

        var eOuterDiv = document.createElement('div');
        eOuterDiv.className = 'div-outer-div';
        eOuterDiv.appendChild(eValue);
        eOuterDiv.appendChild(eDivPercentBar);

        return eOuterDiv;
    }


    static getCellSettings(columnHeader: string, rowData: Array<any>) : Cell {
        let retVal = {} as Cell;
        let cells = rowData.filter(row => {
            return row.timestamp === columnHeader;
        });

        if (! _.isEmpty(cells)) {
            retVal.value = cells[0].value;
            retVal.timestamp = cells[0].timestamp;
            retVal.info = cells[0].info;
        }

        return retVal;
    }

    static flag(value: string, marker: string) {
        let flag = "<img border='0' width='15' height='10' style='margin-bottom: 2px' src='http://flags.fmcdn.net/data/flags/mini/us.png'>";

        let eDivPercentBar = document.createElement('div');
        eDivPercentBar.className = 'div-percent-bar';
        eDivPercentBar.style.width = '100%';
        eDivPercentBar.innerHTML = value + '  ' + flag;
        return eDivPercentBar;
    }

    static normal(params) {
        var eDivPercentBar = document.createElement('div');
        eDivPercentBar.className = 'div-percent-bar';
        eDivPercentBar.style.width = '100%';

        var eValue = document.createElement('div');
        eValue.className = 'div-percent-value';
        eValue.innerHTML = params.value;

        var eOuterDiv = document.createElement('div');
        eOuterDiv.className = 'div-outer-div';
        eOuterDiv.appendChild(eValue);
        eOuterDiv.appendChild(eDivPercentBar);

        return eOuterDiv;
    }
}
