import { GridPaginationService } from '../../services/grid.pagination.service';
export declare class PagingControlComponent {
    private gridPaginationService;
    private gridOptions;
    private serverSidePaging;
    private currentPage;
    private totalPages;
    private initialized;
    constructor(gridPaginationService: GridPaginationService);
    initialize(gridOptions: any): void;
    setServerSidePaging(serverSidePaging: boolean): void;
    reset(): void;
    changePage(option: string): void;
}
