"use strict";
/*
 *  Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var grid_pagination_service_1 = require("../../services/grid.pagination.service");
var PagingControlComponent = /** @class */ (function () {
    function PagingControlComponent(gridPaginationService) {
        this.gridPaginationService = gridPaginationService;
        this.serverSidePaging = false;
        this.currentPage = 0;
        this.totalPages = 0;
        this.initialized = false;
    }
    PagingControlComponent.prototype.initialize = function (gridOptions) {
        var _this = this;
        this.gridOptions = gridOptions;
        this.gridOptions.suppressPaginationPanel = true;
        this.gridOptions.onPaginationChanged = function () {
            if (!_this.serverSidePaging) {
                _this.currentPage = _this.gridOptions.api.paginationGetCurrentPage() + 1;
                _this.totalPages = _this.gridOptions.api.paginationGetTotalPages();
            }
            else {
                if (!_this.initialized) {
                    _this.currentPage = 1;
                    _this.totalPages = 1;
                    _this.initialized = true;
                }
            }
        };
        this.gridPaginationService.pageNumber$.subscribe(function (pageIndex) {
            // will only react to 0 which is to reset
            if (pageIndex === 0) {
                _this.reset();
            }
        });
        this.gridPaginationService.totalPages$.subscribe(function (count) {
            // will only react to 0 which is to reset
            _this.totalPages = count;
        });
    };
    PagingControlComponent.prototype.setServerSidePaging = function (serverSidePaging) {
        this.serverSidePaging = serverSidePaging;
    };
    PagingControlComponent.prototype.reset = function () {
        if (this.serverSidePaging) {
            this.currentPage = 1;
            this.totalPages = 1;
        }
    };
    PagingControlComponent.prototype.changePage = function (option) {
        // console.log(`coming changing page number to ${this.currentPage}`);
        switch (option) {
            case 'first':
                if (!this.serverSidePaging) {
                    this.gridOptions.api.paginationGoToFirstPage();
                }
                else {
                    this.currentPage = 1;
                    this.gridPaginationService.setPageNumber(this.currentPage);
                }
                break;
            case 'previous':
                if (!this.serverSidePaging) {
                    this.gridOptions.api.paginationGoToPreviousPage();
                }
                else if (this.currentPage > 1) {
                    this.currentPage--;
                    this.gridPaginationService.setPageNumber(this.currentPage);
                }
                break;
            case 'next':
                if (!this.serverSidePaging) {
                    this.gridOptions.api.paginationGoToNextPage();
                }
                else if (this.currentPage < this.totalPages) {
                    this.currentPage++;
                    this.gridPaginationService.setPageNumber(this.currentPage);
                }
                break;
            case 'last':
                if (!this.serverSidePaging) {
                    this.gridOptions.api.paginationGoToLastPage();
                }
                else {
                    this.currentPage = this.totalPages;
                    this.gridPaginationService.setPageNumber(this.currentPage);
                }
                break;
        }
        // console.log(`leaving changing page number to ${this.currentPage}`);
    };
    PagingControlComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'paging-control',
            styleUrls: ['./paging-control.component.css'],
            templateUrl: './paging-control.component.html'
        }),
        __metadata("design:paramtypes", [grid_pagination_service_1.GridPaginationService])
    ], PagingControlComponent);
    return PagingControlComponent;
}());
exports.PagingControlComponent = PagingControlComponent;
//# sourceMappingURL=paging-control.component.js.map