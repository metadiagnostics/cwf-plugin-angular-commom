/*
 *  Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import {Component, Input} from '@angular/core';
import {GridPaginationService} from '../../services/grid.pagination.service';
import {Observable} from "rxjs/Observable";

@Component({
    moduleId: module.id,
    selector: 'paging-control',
    styleUrls: ['./paging-control.component.css'],
    templateUrl: './paging-control.component.html'
})

export class PagingControlComponent {
    private gridOptions: any;
    private serverSidePaging: boolean = false;
    private currentPage: number = 0;
    private totalPages: number = 0;
    
    private initialized : boolean = false;
    constructor(private gridPaginationService: GridPaginationService) {
    }

    initialize(gridOptions: any) {
        this.gridOptions = gridOptions;
        this.gridOptions.suppressPaginationPanel = true;
        this.gridOptions.onPaginationChanged = () => {
            if (!this.serverSidePaging) {
                this.currentPage = this.gridOptions.api.paginationGetCurrentPage() + 1;
                this.totalPages = this.gridOptions.api.paginationGetTotalPages();
            } else {
                if (!this.initialized) {
                    this.currentPage = 1;
                    this.totalPages = 1;
                    this.initialized = true;
                }
            }
        }

        this.gridPaginationService.pageNumber$.subscribe((pageIndex) => {
            // will only react to 0 which is to reset
            if (pageIndex === 0) {
                this.reset();
            }
        });

        this.gridPaginationService.totalPages$.subscribe((count) => {
            // will only react to 0 which is to reset
            this.totalPages = count;
        });

    }

    public setServerSidePaging(serverSidePaging: boolean) {
        this.serverSidePaging = serverSidePaging;
    }

    public reset() {
        if (this.serverSidePaging) {
            this.currentPage = 1;
            this.totalPages = 1;
        }
    }

    changePage(option: string) {
        // console.log(`coming changing page number to ${this.currentPage}`);
        switch(option) {
            case 'first':
                if (! this.serverSidePaging) {
                    this.gridOptions.api.paginationGoToFirstPage();
                } else {
                    this.currentPage = 1;
                    this.gridPaginationService.setPageNumber(this.currentPage);
                }

                break;
            case 'previous':
                if (! this.serverSidePaging) {
                    this.gridOptions.api.paginationGoToPreviousPage();
                } else if (this.currentPage > 1) {
                    this.currentPage --;
                    this.gridPaginationService.setPageNumber(this.currentPage);
                }

                break;
            case 'next':
                if (! this.serverSidePaging) {
                    this.gridOptions.api.paginationGoToNextPage();
                } else if (this.currentPage < this.totalPages) {
                    this.currentPage ++;
                    this.gridPaginationService.setPageNumber(this.currentPage);
                }

                break;
            case 'last':
                if (! this.serverSidePaging) {
                    this.gridOptions.api.paginationGoToLastPage();
                } else {
                    this.currentPage = this.totalPages;
                    this.gridPaginationService.setPageNumber(this.currentPage);
                }

                break;
        }

        // console.log(`leaving changing page number to ${this.currentPage}`);
    }

}
