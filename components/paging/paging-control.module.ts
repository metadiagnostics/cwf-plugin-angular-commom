import {NgModule} from "@angular/core";

import { CommonModule} from "@angular/common";
import {PagingControlComponent} from "./paging-control.component";
import {GridPaginationService} from "../../services/grid.pagination.service";

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        PagingControlComponent
    ],
    exports: [
        PagingControlComponent
    ],
    providers: [
        GridPaginationService
    ]
})
export class PagingControlModule{}