import {InputBaseComponent} from '../input.base.component';
import {Injectable, EventEmitter} from '@angular/core';

@Injectable()
export class ComponentService {

    readonly onChange: EventEmitter<InputBaseComponent> = new EventEmitter();

    readonly afterInit: EventEmitter<any> = new EventEmitter();

}