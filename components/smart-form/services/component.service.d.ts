import { InputBaseComponent } from '../input.base.component';
import { EventEmitter } from '@angular/core';
export declare class ComponentService {
    readonly onChange: EventEmitter<InputBaseComponent>;
    readonly afterInit: EventEmitter<any>;
}
