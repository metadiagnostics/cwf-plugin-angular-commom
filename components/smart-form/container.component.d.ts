import { Responses } from './model/responses.model';
import { SmartFormModel } from './model/smart-form.model';
import { ComponentService } from './services/component.service';
import { IBackendService } from "../../services/ibackend.service";
import { CreateResourceService } from "../../services/create-resource.service";
export declare class ContainerComponent {
    private componentService;
    private backendService;
    private createResourceService;
    private _smartform;
    private _responses;
    private editMode;
    private final;
    private preEditedResponses;
    private _writebackMode;
    constructor(componentService: ComponentService, backendService: IBackendService, createResourceService: CreateResourceService);
    smartform: SmartFormModel;
    responses: Responses;
    writebackMode: string;
    editForm(): void;
    saveForm(): void;
    cancel(): void;
    closeForm(): void;
    private setEditMode(isEditMode);
}
