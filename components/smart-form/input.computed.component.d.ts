import { ComponentService } from './services/component.service';
import { ItemComponent } from './item.component';
import { InputBaseComponent } from './input.base.component';
export declare class InputComputedComponent extends InputBaseComponent {
    computedValue: any;
    private changeSubscription;
    private initSubscription;
    constructor(itemComponent: ItemComponent, componentService: ComponentService);
    recalculate(input: InputBaseComponent): void;
    calculateSum(): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
}
