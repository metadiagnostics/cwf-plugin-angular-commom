import {Responses} from './model/responses.model';
import {SmartFormModel} from './model/smart-form.model';
import {ComponentService} from './services/component.service';
import {ItemComponent} from './item.component';
import {Component, ViewEncapsulation, Input} from '@angular/core';

import * as _ from 'lodash';
import {IBackendService} from "../../services/ibackend.service";
import {CreateResourceService} from "../../services/create-resource.service";

// Smart Form container component
@Component({
    moduleId: module.id,
    selector: '[cms-smartform-container]',
    templateUrl: 'container.component.html',
    viewProviders: [ItemComponent],
    encapsulation: ViewEncapsulation.None
})
export class ContainerComponent {

    private _smartform: SmartFormModel = null;

    private _responses: Responses;

    private editMode: boolean = false;

    private final: boolean;

    private preEditedResponses: Responses;

    private _writebackMode: string;

    constructor(private componentService: ComponentService, private backendService: IBackendService, private createResourceService: CreateResourceService ) {

    }

    @Input()
    get smartform(): SmartFormModel {
        return this._smartform;
    }

    set smartform(smartform: SmartFormModel) {
        this._smartform = smartform;
    }

    @Input()
    get responses(): Responses {
        return this._responses;
    }

    set responses(responses: Responses) {
        this._responses = responses;

        setTimeout(() => {
            this.componentService.afterInit.emit(this);
        }, 100);
    }

    @Input()
    get writebackMode(): string {
        return this._writebackMode;
    }

    set writebackMode(writebackMode: string) {
        this._writebackMode = writebackMode;
        this.final = writebackMode !== 'create';
    }

    editForm() {
        this.preEditedResponses = _.cloneDeep(this._responses);
        this.setEditMode(true);
    }

    saveForm() {
        this.setEditMode(false);

        if (this._writebackMode === 'create') {
            let data = _.omit(this._responses, ['final', 'id']);

            // Hard coded properties
            _.assign(data, {
                _callback: 'liveUpdateMasterGrid',
                title: 'Asssessment',
                location: 'GutCheckNEC',
                documentType: 'Clinical Note',
                specialty: 'Neonatology',
                status: 'Signed',
                noteType: 'Nurse'
            });

            this.backendService.sendBackendEvent('saveNewForm', data);
            this.closeForm();
        } else if (this._writebackMode === 'edit') {
            let data = _.omit(this._responses, ['final']);
            _.assign(data, {
                _callback: 'liveUpdateMasterGrid'
            });

            this.backendService.sendBackendEvent('saveEditedForm', data);
        }
    }

    cancel() {
        this._responses = this.preEditedResponses;

        // Don't really want to use setTimeout, but necessary when two-way binding is not used
        setTimeout(() => {
            this.componentService.onChange.emit();
        }, 0);
        this.setEditMode(false);

        if (this._writebackMode === 'create') {
            this.closeForm();
        }
    }

    closeForm() {
        this.createResourceService.closeForm('note');
    }

    private setEditMode(isEditMode: boolean) {
        this.editMode = isEditMode;
        this.final = !isEditMode;
    }

}

