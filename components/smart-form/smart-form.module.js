"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var common_1 = require("@angular/common");
var button_1 = require("primeng/components/button/button");
var input_choice_component_1 = require("./input.choice.component");
var smart_form_component_1 = require("./smart-form.component");
var container_component_1 = require("./container.component");
var input_computed_component_1 = require("./input.computed.component");
var input_number_component_1 = require("./input.number.component");
var item_component_1 = require("./item.component");
var SmartFormModule = /** @class */ (function () {
    function SmartFormModule() {
    }
    SmartFormModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                button_1.ButtonModule,
                forms_1.FormsModule
            ],
            declarations: [
                smart_form_component_1.SmartFormComponent,
                container_component_1.ContainerComponent,
                item_component_1.ItemComponent,
                input_choice_component_1.InputChoiceComponent,
                input_number_component_1.InputNumberComponent,
                input_computed_component_1.InputComputedComponent
            ],
            exports: [
                smart_form_component_1.SmartFormComponent
            ]
        })
    ], SmartFormModule);
    return SmartFormModule;
}());
exports.SmartFormModule = SmartFormModule;
//# sourceMappingURL=smart-form.module.js.map