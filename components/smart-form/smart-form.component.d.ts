import { Responses } from './model/responses.model';
import { SmartFormModel } from './model/smart-form.model';
export declare class SmartFormComponent {
    protected _smartForm: SmartFormModel;
    protected _responses: Responses;
    protected _writebackMode: string;
    constructor();
    smartForm: SmartFormModel;
    responses: Responses;
    writebackMode: string;
    private initResponses(smartform);
}
