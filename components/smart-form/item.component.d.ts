import { Responses } from './model/responses.model';
import { SmartForm_Item } from './model/smart-form.model';
export declare class ItemComponent {
    item: SmartForm_Item;
    responses: Responses;
    final: boolean;
    readonly required: boolean;
}
