import {Responses_Response} from './model/responses.model';
import {SmartForm_Item} from './model/smart-form.model';
import {ComponentService} from './services/component.service';
import {ItemComponent} from './item.component';

import * as _ from 'lodash';

// Base class for input widgets
export abstract class InputBaseComponent {

    readonly item: SmartForm_Item;

    readonly required: boolean;

    private readonly hints: any;

    constructor(protected itemComponent: ItemComponent, protected componentService: ComponentService) {
        this.item = itemComponent.item;
        this.required = this.item.required;
        let hint = (this.item.hint || '').split(',');
        this.hints = {};

        for (let x of hint) {
            x = x.trim().toLowerCase();
            x ? this.hints[x] = true : null;
        }
    }

    get response(): Responses_Response {
        let responses = this.itemComponent.responses.responses;
        let response = _.find(responses, {question: this.item.question});

        if (!response) {
            response = <Responses_Response>{
                question: this.item.question
            };
            responses.push(response);
        }

        return response;
    }

    get final(): boolean {
        return this.itemComponent.final;
    }

    get value(): any {
        return this.response.value;
    }

    changed(event: Event) {
        this.componentService.onChange.emit(this);
    }

    hasHint(hint: string): boolean {
        return this.hints[hint.toLowerCase()];
    }

    protected setValue(value: any) {
        this.response.value = value;
    }
}

