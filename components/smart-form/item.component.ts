import {Responses} from './model/responses.model';
import {SmartForm_Item} from './model/smart-form.model';
import {Component, ViewEncapsulation, Input} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: '[cms-smartform-item]',
    templateUrl: 'item.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ItemComponent {

    @Input()
    item: SmartForm_Item;

    @Input()
    responses: Responses;

    @Input()
    final: boolean;

    get required(): boolean {
        return this.item.required;
    }

}

