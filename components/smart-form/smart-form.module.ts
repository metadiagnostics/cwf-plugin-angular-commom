import {NgModule} from "@angular/core";
import {FormsModule} from '@angular/forms'
import {CommonModule} from "@angular/common";
import {ButtonModule} from 'primeng/components/button/button';
import {InputChoiceComponent} from './input.choice.component';
import {SmartFormComponent} from './smart-form.component';
import {ContainerComponent} from './container.component';
import {InputComputedComponent} from './input.computed.component';
import {InputNumberComponent} from './input.number.component';
import {ItemComponent} from './item.component';

@NgModule({
    imports: [
        CommonModule,
        ButtonModule,
        FormsModule
    ],
    declarations: [
      SmartFormComponent,
      ContainerComponent, 
      ItemComponent,
      InputChoiceComponent, 
      InputNumberComponent, 
      InputComputedComponent
    ],
    exports: [
      SmartFormComponent
    ]
})
export class SmartFormModule{}