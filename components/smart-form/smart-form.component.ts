import {InputChoiceComponent} from './input.choice.component';
import {Responses} from './model/responses.model';
import {SmartFormModel} from './model/smart-form.model';
import {ComponentService} from './services/component.service';
import {ContainerComponent} from './container.component';
import {InputNumberComponent} from './input.number.component';
import {ItemComponent} from './item.component';
import {Component, Input, ViewEncapsulation} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {Guid} from "../../misc/utilities";

// Smart Form component
@Component({
    moduleId: module.id,
    selector: 'cms-smartform',
    templateUrl: 'smart-form.component.html',
    styleUrls: ['./smart-form.component.css'],
    providers: [ComponentService],
    viewProviders: [ContainerComponent],
    encapsulation: ViewEncapsulation.None
})
export class SmartFormComponent {


    protected _smartForm: SmartFormModel;

    protected _responses: Responses;

    protected _writebackMode: string;

    constructor() {
    }

    @Input()
    get smartForm(): SmartFormModel {
        return this._smartForm;
    }

    set smartForm(smartForm: SmartFormModel) {
        if (!smartForm) {
            return;
        }

        this.responses = this.initResponses(smartForm);
        this._smartForm = smartForm;
    }

    @Input()
    get responses(): Responses {
        return this._responses;
    }

    set responses(responses: Responses) {
        this._responses = responses;
    }

    @Input()
    get writebackMode(): string {
        return this._writebackMode;
    }

    set writebackMode(writebackMode: string) {
        this._writebackMode = writebackMode;
    }

    private initResponses(smartform: SmartFormModel): Responses {
        return {
            id: Guid.newGuid(),
            smartformType: smartform.type,
            smartformVersion: smartform.version,
            final: true,
            responses: []
        };
    }

}

