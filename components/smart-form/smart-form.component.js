"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var component_service_1 = require("./services/component.service");
var container_component_1 = require("./container.component");
var core_1 = require("@angular/core");
var utilities_1 = require("../../misc/utilities");
// Smart Form component
var SmartFormComponent = /** @class */ (function () {
    function SmartFormComponent() {
    }
    Object.defineProperty(SmartFormComponent.prototype, "smartForm", {
        get: function () {
            return this._smartForm;
        },
        set: function (smartForm) {
            if (!smartForm) {
                return;
            }
            this.responses = this.initResponses(smartForm);
            this._smartForm = smartForm;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SmartFormComponent.prototype, "responses", {
        get: function () {
            return this._responses;
        },
        set: function (responses) {
            this._responses = responses;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SmartFormComponent.prototype, "writebackMode", {
        get: function () {
            return this._writebackMode;
        },
        set: function (writebackMode) {
            this._writebackMode = writebackMode;
        },
        enumerable: true,
        configurable: true
    });
    SmartFormComponent.prototype.initResponses = function (smartform) {
        return {
            id: utilities_1.Guid.newGuid(),
            smartformType: smartform.type,
            smartformVersion: smartform.version,
            final: true,
            responses: []
        };
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], SmartFormComponent.prototype, "smartForm", null);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], SmartFormComponent.prototype, "responses", null);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], SmartFormComponent.prototype, "writebackMode", null);
    SmartFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'cms-smartform',
            templateUrl: 'smart-form.component.html',
            styleUrls: ['./smart-form.component.css'],
            providers: [component_service_1.ComponentService],
            viewProviders: [container_component_1.ContainerComponent],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [])
    ], SmartFormComponent);
    return SmartFormComponent;
}());
exports.SmartFormComponent = SmartFormComponent;
//# sourceMappingURL=smart-form.component.js.map