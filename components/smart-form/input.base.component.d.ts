import { Responses_Response } from './model/responses.model';
import { SmartForm_Item } from './model/smart-form.model';
import { ComponentService } from './services/component.service';
import { ItemComponent } from './item.component';
export declare abstract class InputBaseComponent {
    protected itemComponent: ItemComponent;
    protected componentService: ComponentService;
    readonly item: SmartForm_Item;
    readonly required: boolean;
    private readonly hints;
    constructor(itemComponent: ItemComponent, componentService: ComponentService);
    readonly response: Responses_Response;
    readonly final: boolean;
    readonly value: any;
    changed(event: Event): void;
    hasHint(hint: string): boolean;
    protected setValue(value: any): void;
}
