"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var component_service_1 = require("./services/component.service");
var item_component_1 = require("./item.component");
var input_base_component_1 = require("./input.base.component");
var core_1 = require("@angular/core");
var _ = require("lodash");
// Numeric entry widget
var InputComputedComponent = /** @class */ (function (_super) {
    __extends(InputComputedComponent, _super);
    function InputComputedComponent(itemComponent, componentService) {
        var _this = _super.call(this, itemComponent, componentService) || this;
        _this.computedValue = 0;
        return _this;
    }
    InputComputedComponent.prototype.recalculate = function (input) {
        if (this === input) {
            return;
        }
        if (this.item.calculation === 'sum') {
            this.calculateSum();
        }
    };
    InputComputedComponent.prototype.calculateSum = function () {
        this.computedValue = _.sumBy(this.itemComponent.responses.responses, function (question) {
            return Number(_.get(question, 'value', 0));
        });
    };
    InputComputedComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.changeSubscription = this.componentService.onChange.subscribe(function (input) {
            _this.recalculate(input);
        });
        this.initSubscription = this.componentService.afterInit.subscribe(function (input) {
            _this.recalculate(input);
        });
    };
    InputComputedComponent.prototype.ngOnDestroy = function () {
        this.changeSubscription.unsubscribe();
        this.initSubscription.unsubscribe();
    };
    InputComputedComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: '[cms-smartform-computed]',
            templateUrl: 'input.computed.component.html',
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [item_component_1.ItemComponent, component_service_1.ComponentService])
    ], InputComputedComponent);
    return InputComputedComponent;
}(input_base_component_1.InputBaseComponent));
exports.InputComputedComponent = InputComputedComponent;
//# sourceMappingURL=input.computed.component.js.map