import {ComponentService} from './services/component.service';
import {ItemComponent} from './item.component';
import {InputBaseComponent} from './input.base.component';
import {Component, ViewEncapsulation} from '@angular/core';

// Choice selection widget
@Component({
    moduleId: module.id,
    selector: '[cms-smartform-choice]',
    templateUrl: 'input.choice.component.html',
    encapsulation: ViewEncapsulation.None
})
export class InputChoiceComponent extends InputBaseComponent {

    constructor(itemComponent: ItemComponent, componentService: ComponentService) {
        super(itemComponent, componentService);
    }

    changed(event: any) {
        this.setValue(event.target.value);
        this.response.id = event.target.dataset.responseId;
        super.changed(event);
    }

    layout(): string {
        let horz = this.hasHint('horizontal') ? true :
            this.hasHint('vertical') ? false : this.item.options.length < 6;

        return horz ? 'cms-smartform-choice-horz' : 'cms-smartform-choice-vert';
    }

}

