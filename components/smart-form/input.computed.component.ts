import {ComponentService} from './services/component.service';
import {ItemComponent} from './item.component';
import {InputBaseComponent} from './input.base.component';
import {Component, ViewEncapsulation} from '@angular/core';

import * as _ from 'lodash';

// Numeric entry widget
@Component({
    moduleId: module.id,
    selector: '[cms-smartform-computed]',
    templateUrl: 'input.computed.component.html',
    encapsulation: ViewEncapsulation.None
})
export class InputComputedComponent extends InputBaseComponent {

    computedValue: any = 0;

    private changeSubscription: any;

    private initSubscription: any;

    constructor(itemComponent: ItemComponent, componentService: ComponentService) {
        super(itemComponent, componentService);
    }

    recalculate(input: InputBaseComponent) {
        if (this === input) {
            return;
        }

        if (this.item.calculation === 'sum') {
            this.calculateSum();
        }
    }

    calculateSum() {
        this.computedValue = _.sumBy(this.itemComponent.responses.responses, (question) => {
            return Number(_.get(question, 'value', 0));
        });
    }

    ngOnInit() {
        this.changeSubscription = this.componentService.onChange.subscribe(input => {
            this.recalculate(input);
        });
        this.initSubscription = this.componentService.afterInit.subscribe(input => {
            this.recalculate(input);
        });
    }

    ngOnDestroy() {
        this.changeSubscription.unsubscribe();
        this.initSubscription.unsubscribe();
    }
}

