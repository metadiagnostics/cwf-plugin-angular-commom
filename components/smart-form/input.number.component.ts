import {ComponentService} from './services/component.service';
import {ItemComponent} from './item.component';
import {InputBaseComponent} from './input.base.component';
import {Component, ViewEncapsulation} from '@angular/core';

// Numeric entry widget
@Component({
    moduleId: module.id,
    selector: '[cms-smartform-number]',
    templateUrl: 'input.number.component.html',
    encapsulation: ViewEncapsulation.None
})
export class InputNumberComponent extends InputBaseComponent {

    constructor(itemComponent: ItemComponent, componentService: ComponentService) {
        super(itemComponent, componentService);
    }

    changed(event: any) {
        this.setValue(event.target.value);
        super.changed(event);
    }

}

