"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
// Base class for input widgets
var InputBaseComponent = /** @class */ (function () {
    function InputBaseComponent(itemComponent, componentService) {
        this.itemComponent = itemComponent;
        this.componentService = componentService;
        this.item = itemComponent.item;
        this.required = this.item.required;
        var hint = (this.item.hint || '').split(',');
        this.hints = {};
        for (var _i = 0, hint_1 = hint; _i < hint_1.length; _i++) {
            var x = hint_1[_i];
            x = x.trim().toLowerCase();
            x ? this.hints[x] = true : null;
        }
    }
    Object.defineProperty(InputBaseComponent.prototype, "response", {
        get: function () {
            var responses = this.itemComponent.responses.responses;
            var response = _.find(responses, { question: this.item.question });
            if (!response) {
                response = {
                    question: this.item.question
                };
                responses.push(response);
            }
            return response;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputBaseComponent.prototype, "final", {
        get: function () {
            return this.itemComponent.final;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputBaseComponent.prototype, "value", {
        get: function () {
            return this.response.value;
        },
        enumerable: true,
        configurable: true
    });
    InputBaseComponent.prototype.changed = function (event) {
        this.componentService.onChange.emit(this);
    };
    InputBaseComponent.prototype.hasHint = function (hint) {
        return this.hints[hint.toLowerCase()];
    };
    InputBaseComponent.prototype.setValue = function (value) {
        this.response.value = value;
    };
    return InputBaseComponent;
}());
exports.InputBaseComponent = InputBaseComponent;
//# sourceMappingURL=input.base.component.js.map