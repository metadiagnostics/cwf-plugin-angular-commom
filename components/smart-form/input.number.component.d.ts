import { ComponentService } from './services/component.service';
import { ItemComponent } from './item.component';
import { InputBaseComponent } from './input.base.component';
export declare class InputNumberComponent extends InputBaseComponent {
    constructor(itemComponent: ItemComponent, componentService: ComponentService);
    changed(event: any): void;
}
