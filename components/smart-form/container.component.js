"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var component_service_1 = require("./services/component.service");
var item_component_1 = require("./item.component");
var core_1 = require("@angular/core");
var _ = require("lodash");
var ibackend_service_1 = require("../../services/ibackend.service");
var create_resource_service_1 = require("../../services/create-resource.service");
// Smart Form container component
var ContainerComponent = /** @class */ (function () {
    function ContainerComponent(componentService, backendService, createResourceService) {
        this.componentService = componentService;
        this.backendService = backendService;
        this.createResourceService = createResourceService;
        this._smartform = null;
        this.editMode = false;
    }
    Object.defineProperty(ContainerComponent.prototype, "smartform", {
        get: function () {
            return this._smartform;
        },
        set: function (smartform) {
            this._smartform = smartform;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContainerComponent.prototype, "responses", {
        get: function () {
            return this._responses;
        },
        set: function (responses) {
            var _this = this;
            this._responses = responses;
            setTimeout(function () {
                _this.componentService.afterInit.emit(_this);
            }, 100);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContainerComponent.prototype, "writebackMode", {
        get: function () {
            return this._writebackMode;
        },
        set: function (writebackMode) {
            this._writebackMode = writebackMode;
            this.final = writebackMode !== 'create';
        },
        enumerable: true,
        configurable: true
    });
    ContainerComponent.prototype.editForm = function () {
        this.preEditedResponses = _.cloneDeep(this._responses);
        this.setEditMode(true);
    };
    ContainerComponent.prototype.saveForm = function () {
        this.setEditMode(false);
        if (this._writebackMode === 'create') {
            var data = _.omit(this._responses, ['final', 'id']);
            // Hard coded properties
            _.assign(data, {
                _callback: 'liveUpdateMasterGrid',
                title: 'Asssessment',
                location: 'GutCheckNEC',
                documentType: 'Clinical Note',
                specialty: 'Neonatology',
                status: 'Signed',
                noteType: 'Nurse'
            });
            this.backendService.sendBackendEvent('saveNewForm', data);
            this.closeForm();
        }
        else if (this._writebackMode === 'edit') {
            var data = _.omit(this._responses, ['final']);
            _.assign(data, {
                _callback: 'liveUpdateMasterGrid'
            });
            this.backendService.sendBackendEvent('saveEditedForm', data);
        }
    };
    ContainerComponent.prototype.cancel = function () {
        var _this = this;
        this._responses = this.preEditedResponses;
        // Don't really want to use setTimeout, but necessary when two-way binding is not used
        setTimeout(function () {
            _this.componentService.onChange.emit();
        }, 0);
        this.setEditMode(false);
        if (this._writebackMode === 'create') {
            this.closeForm();
        }
    };
    ContainerComponent.prototype.closeForm = function () {
        this.createResourceService.closeForm('note');
    };
    ContainerComponent.prototype.setEditMode = function (isEditMode) {
        this.editMode = isEditMode;
        this.final = !isEditMode;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], ContainerComponent.prototype, "smartform", null);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], ContainerComponent.prototype, "responses", null);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], ContainerComponent.prototype, "writebackMode", null);
    ContainerComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: '[cms-smartform-container]',
            templateUrl: 'container.component.html',
            viewProviders: [item_component_1.ItemComponent],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [component_service_1.ComponentService, ibackend_service_1.IBackendService, create_resource_service_1.CreateResourceService])
    ], ContainerComponent);
    return ContainerComponent;
}());
exports.ContainerComponent = ContainerComponent;
//# sourceMappingURL=container.component.js.map