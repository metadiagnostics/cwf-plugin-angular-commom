export interface Responses_Response {
    id: string;
    value: any;
    question: string;
}
export interface Responses {
    id: string;
    smartformType: string;
    smartformVersion: string;
    final: boolean;
    patientId?: string;
    noteId?: string;
    [x: string]: any;
    responses: Responses_Response[];
}
