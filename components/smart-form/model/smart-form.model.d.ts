export interface SmartForm_Item_Choice {
    id: string;
    label: string;
    value: any;
    description: string;
}
export interface SmartForm_Item {
    question: string;
    label: string;
    units: string;
    description: string;
    hint: string;
    required: boolean;
    format: string;
    options?: SmartForm_Item_Choice[];
    calculation?: string;
}
export interface SmartFormModel {
    id: string;
    type: string;
    version: string;
    title?: string;
    subtitle?: string;
    copyright?: string;
    license?: string;
    distribution?: string;
    items: SmartForm_Item[];
    [x: string]: any;
}
