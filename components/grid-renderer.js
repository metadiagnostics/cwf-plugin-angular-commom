"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var Grid = /** @class */ (function () {
    function Grid() {
        this.gridWidth = 0;
        this.gridHeight = 0;
        this.rowHeight = 25;
        this.pagingPanelHeight = 32;
        this.paging = false;
        this.pageSize = 0;
    }
    Grid.prototype.initialize = function (gridOptions, elementId, paging, pageSize) {
        var _this = this;
        this.elementId = elementId;
        gridOptions.suppressLoadingOverlay = false;
        gridOptions.suppressNoRowsOverlay = false;
        gridOptions.columnDefs = [];
        gridOptions.rowData = [];
        if (!_.isUndefined(paging) && paging) {
            gridOptions.pagination = true;
            this.paging = true;
            if (!_.isUndefined(pageSize)) {
                gridOptions.paginationPageSize = pageSize;
                this.pageSize = pageSize;
            }
        }
        gridOptions.onGridSizeChanged = function ($event) {
            _this.gridWidth = $event.clientWidth;
            _this.gridHeight = $event.clientHeight;
            _this.setGridHeight(gridOptions);
        };
    };
    Grid.prototype.refreshGrid = function (gridOptions, rows, columns) {
        if (_.isNil(gridOptions.api)) {
            console.error('should not be in refreshGrid');
            return;
        }
        // TODO there is currently no support for dynamically changing this setting.
        // Leave here for when they support it.
        if (this.pageSize == 0 || _.isEmpty(rows) || rows.length < this.pageSize) {
            // turn off the pagination settings...
            gridOptions.pagination = false;
        }
        gridOptions.rowData = this.configureRows(rows);
        gridOptions.columnDefs = this.configureColumns(columns);
        gridOptions.api.setColumnDefs(gridOptions.columnDefs);
        gridOptions.api.setRowData(gridOptions.rowData);
        this.setGridHeight(gridOptions);
    };
    Grid.prototype.isScrollarShowingGrid = function (gridOptions) {
        var visibleColumns = gridOptions.columnApi.getAllDisplayedColumns();
        var width = 0;
        _.forEach(visibleColumns, function (column) {
            width += column.getActualWidth();
        });
        var gridWidth = this.getGridWidth();
        var retVal = width > gridWidth;
        return retVal;
    };
    Grid.prototype.getGridWidth = function () {
        var width = 1200;
        var el = document.getElementById(this.elementId);
        if (el) {
            var positionInfo = el.getBoundingClientRect();
            width = positionInfo.width;
        }
        return width;
    };
    Grid.prototype.setGridHeight = function (gridOptions) {
        var eGridDiv = document.getElementById(this.elementId);
        if (eGridDiv) {
            if (_.get(gridOptions, 'rowData.length', 0) > 0) {
                // row data + 1 to account for the column headers...
                var height = (gridOptions.rowData.length + 1) * this.rowHeight + 5;
                if (this.isScrollarShowingGrid(gridOptions)) {
                    height += 20;
                }
                if (height > 300) {
                    height = 300;
                }
                if (this.paging) {
                    height += this.pagingPanelHeight;
                }
                eGridDiv.style.height = height + 'px';
                this.gridHeight = height;
                // console.log(`grid height ${height} for ${gridOptions.rowData.length} rows`);
            }
            else if (!_.isUndefined(gridOptions.emptyHeight)) {
                eGridDiv.style.height = gridOptions.emptyHeight;
                this.gridHeight = gridOptions.emptyHeight;
            }
        }
    };
    Grid.prototype.selectFirstRow = function (gridOptions) {
    };
    Grid.selectRow = function (gridOptions, index) {
        var node = gridOptions.api.getDisplayedRowAtIndex(index);
        if (!_.isEmpty(node)) {
            node.setSelected(true, true);
        }
    };
    Grid.dueOverDueCellRenderer = function (params) {
        var value = params.value;
        var eDivPercentBar = document.createElement('div');
        eDivPercentBar.className = 'div-percent-bar';
        eDivPercentBar.style.width = '100%';
        if (value === 'OVERDUE') {
            eDivPercentBar.style.backgroundColor = '#FF3333';
        }
        else if (value === 'DUE') {
            eDivPercentBar.style.backgroundColor = '#66FF66';
        }
        var eValue = document.createElement('div');
        eValue.className = 'div-percent-value';
        eValue.innerHTML = value;
        var eOuterDiv = document.createElement('div');
        eOuterDiv.className = 'div-outer-div';
        eOuterDiv.appendChild(eValue);
        eOuterDiv.appendChild(eDivPercentBar);
        return eOuterDiv;
    };
    Grid.getCellSettings = function (columnHeader, rowData) {
        var retVal = {};
        var cells = rowData.filter(function (row) {
            return row.timestamp === columnHeader;
        });
        if (!_.isEmpty(cells)) {
            retVal.value = cells[0].value;
            retVal.timestamp = cells[0].timestamp;
            retVal.info = cells[0].info;
        }
        return retVal;
    };
    Grid.flag = function (value, marker) {
        var flag = "<img border='0' width='15' height='10' style='margin-bottom: 2px' src='http://flags.fmcdn.net/data/flags/mini/us.png'>";
        var eDivPercentBar = document.createElement('div');
        eDivPercentBar.className = 'div-percent-bar';
        eDivPercentBar.style.width = '100%';
        eDivPercentBar.innerHTML = value + '  ' + flag;
        return eDivPercentBar;
    };
    Grid.normal = function (params) {
        var eDivPercentBar = document.createElement('div');
        eDivPercentBar.className = 'div-percent-bar';
        eDivPercentBar.style.width = '100%';
        var eValue = document.createElement('div');
        eValue.className = 'div-percent-value';
        eValue.innerHTML = params.value;
        var eOuterDiv = document.createElement('div');
        eOuterDiv.className = 'div-outer-div';
        eOuterDiv.appendChild(eValue);
        eOuterDiv.appendChild(eDivPercentBar);
        return eOuterDiv;
    };
    return Grid;
}());
exports.Grid = Grid;
//# sourceMappingURL=grid-renderer.js.map