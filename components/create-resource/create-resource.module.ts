import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {CreateResourceComponent} from "./create-resource.component";
import {SmartFormModule} from "../smart-form/smart-form.module";
import {CreateResourceService} from "../../services/create-resource.service";

@NgModule({
    imports: [
        CommonModule,
        SmartFormModule
    ],
    declarations: [
        CreateResourceComponent
    ],
    exports: [
        CreateResourceComponent
    ],
    providers: [
        CreateResourceService
    ]
})
export class CreateResourceModule{}