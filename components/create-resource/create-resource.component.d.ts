import { IBackendService } from "../../services/ibackend.service";
export declare class CreateResourceComponent {
    private backendService;
    private display;
    private domain;
    private resourceId;
    private smartForm;
    constructor(backendService: IBackendService);
    initForm(domain: string): void;
    closeForm(): void;
    private requestNECFormDefinition();
    updateFormDefinition(response: any): void;
}
