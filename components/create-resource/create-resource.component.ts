import {Component} from '@angular/core';
import {IBackendService} from "../../services/ibackend.service";
import {Guid} from '../../misc/utilities';

@Component({
    moduleId   : module.id,
    selector   : 'create-resource',
    templateUrl: './create-resource.component.html'
})
export class CreateResourceComponent {
    private display: boolean = false;
    private domain: string;
    private resourceId: string;
    private smartForm;

    constructor(private backendService: IBackendService) {
        this.resourceId = Guid.newGuid();
    }

    public initForm(domain: string) {
        this.display = true;
        this.domain = domain;

        switch (this.domain) {
            case 'note':
                this.requestNECFormDefinition();
                break;
        }
    }

    public closeForm() {
        this.domain = null;
        this.display = false;
    }

    private requestNECFormDefinition(){
        let params: any = {
            formType: 'gutchecknec',
            formVersion: '1.0',
            _uuid: this.resourceId,
            _callback: "updateFormDefinition"
        };

        this.backendService.sendBackendEvent('requestFormDefinition', params);
    }

    // This needs to be called from somewhere else since fusion can only deal with top level components
    updateFormDefinition(response: any){
        this.smartForm = response.payload;
    }
}