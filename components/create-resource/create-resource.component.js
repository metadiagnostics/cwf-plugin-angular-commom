"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ibackend_service_1 = require("../../services/ibackend.service");
var utilities_1 = require("../../misc/utilities");
var CreateResourceComponent = /** @class */ (function () {
    function CreateResourceComponent(backendService) {
        this.backendService = backendService;
        this.display = false;
        this.resourceId = utilities_1.Guid.newGuid();
    }
    CreateResourceComponent.prototype.initForm = function (domain) {
        this.display = true;
        this.domain = domain;
        switch (this.domain) {
            case 'note':
                this.requestNECFormDefinition();
                break;
        }
    };
    CreateResourceComponent.prototype.closeForm = function () {
        this.domain = null;
        this.display = false;
    };
    CreateResourceComponent.prototype.requestNECFormDefinition = function () {
        var params = {
            formType: 'gutchecknec',
            formVersion: '1.0',
            _uuid: this.resourceId,
            _callback: "updateFormDefinition"
        };
        this.backendService.sendBackendEvent('requestFormDefinition', params);
    };
    // This needs to be called from somewhere else since fusion can only deal with top level components
    CreateResourceComponent.prototype.updateFormDefinition = function (response) {
        this.smartForm = response.payload;
    };
    CreateResourceComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'create-resource',
            templateUrl: './create-resource.component.html'
        }),
        __metadata("design:paramtypes", [ibackend_service_1.IBackendService])
    ], CreateResourceComponent);
    return CreateResourceComponent;
}());
exports.CreateResourceComponent = CreateResourceComponent;
//# sourceMappingURL=create-resource.component.js.map