"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var create_resource_component_1 = require("./create-resource.component");
var smart_form_module_1 = require("../smart-form/smart-form.module");
var create_resource_service_1 = require("../../services/create-resource.service");
var CreateResourceModule = /** @class */ (function () {
    function CreateResourceModule() {
    }
    CreateResourceModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                smart_form_module_1.SmartFormModule
            ],
            declarations: [
                create_resource_component_1.CreateResourceComponent
            ],
            exports: [
                create_resource_component_1.CreateResourceComponent
            ],
            providers: [
                create_resource_service_1.CreateResourceService
            ]
        })
    ], CreateResourceModule);
    return CreateResourceModule;
}());
exports.CreateResourceModule = CreateResourceModule;
//# sourceMappingURL=create-resource.module.js.map