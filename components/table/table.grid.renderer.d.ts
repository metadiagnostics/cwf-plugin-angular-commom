import { Grid } from '../grid-renderer';
export declare class TableGrid extends Grid {
    constructor(resizable?: boolean);
    initialize(gridOptions: any, elementId: string, paging?: boolean, pageSize?: number): void;
    protected configureColumns(columns: any): any;
    protected configureRows(rows: any): any;
}
