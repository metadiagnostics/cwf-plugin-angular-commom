/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import _ = require('lodash');
import * as moment from 'moment';
import {CellRenderers} from "./cell-renderers";

export class TableModelProcessor {
    protected rows: any = [];
    protected columns: any = [];
    private conditionalHeader: any;

    constructor(columns: any, conditionalHeader?: any) {
        this.columns = columns;
        this.conditionalHeader = conditionalHeader;
    }

    public process(model: any) {
        if (_.isNil(model)) {
            console.error('can not process an empty model');
            return;
        }

        this.rows = this.getRowsFromModel(model);
    }

    public getRows(): any {
        return this.rows;
    }

    public getColumns(rows?: any): any {
        _.each(this.columns, (column) => {
            if (column.renderer) {
                column.cellRenderer = CellRenderers.getRenderer(column.renderer);
            }
        });

        if (!_.isUndefined(this.conditionalHeader) && _.isArray(rows)) {
            let hasGroupProperty = _.find(rows, i => {
                return _.has(i, this.conditionalHeader.field);
            });

            if (hasGroupProperty) {
                this.columns.unshift(this.conditionalHeader);
            } else {
                _.remove(this.columns, {field: this.conditionalHeader.field});
            }
        }
        return this.columns;
    }

    protected getRowsFromModel(model) {
        let rows = [];
        // console.log(model);
        _.each(model.data, (item) => {
            let row = {};
            let key = _.get(_.keys(item), '[0]');

            if (!_.isUndefined(key)) {
                if (!_.isEmpty(key)) {
                    if (model.dataType === 'datetime') {
                        row[model.type] = this.formatDateTimeType(key);
                    }
                    else if (model.dataType === 'date') {
                        row[model.type] = this.formatDateType(key);
                    }
                    else {
                        row[model.type] = key;
                    }
                } else {
                    row[model.type] = '';
                }
                _.each(item[key], (cell) => {
                    if (cell.dataType === 'datetime') {
                        row[cell.type] = this.formatDateTimeType(cell.value);
                    }
                    else if (cell.dataType === 'date') {
                        row[cell.type] = this.formatDateType(cell.value);
                    }
                    else {
                        row[cell.type] = _.isEmpty(cell.value) ? '': cell.value;
                    }
                });
            }
            rows.push(row);
        });

        return rows;
    }

    protected formatDateTimeType(datetime) {
        if (_.isEmpty(datetime)) {
            return '';
        }
        return moment(datetime, 'YYYYMMDDHHmm').format("MM-DD-YYYY HH:mm");
    }

    protected formatDateType(datetime) {
        if (_.isEmpty(datetime)) {
            return '';
        }
        return moment(datetime, 'YYYY-MM-DD').format("MM-DD-YYYY");
    }
}

