import { RowNode } from 'ag-grid/main';
import { TableModelProcessor } from './table.model.processor';
import { IBackendService } from '../../services/ibackend.service';
import { Grid } from '../grid-renderer';
export declare class TableComponent {
    private service;
    private gridOptions;
    private modelProcessor;
    private grid;
    private uuid;
    private currentModel;
    private useCustomPaging;
    private useServeSidePaging;
    private pagingComponent;
    constructor(service: IBackendService);
    setServerSidePaging(useServeSidePaging: boolean): void;
    initialize(modelProcessor: TableModelProcessor, grid: Grid, gridOptions?: any, paging?: boolean, pageSize?: number, useCustomPaging?: boolean): void;
    getUUID(): string;
    selectRow(index: number): void;
    findRowById(value: any): RowNode;
    showLoadingIndicator(): void;
    setModel(model: any): void;
    updateModel(modelToAdd: any): void;
    private findModelId(modelItem);
    private process(tableModel);
}
