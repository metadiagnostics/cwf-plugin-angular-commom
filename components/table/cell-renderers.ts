import * as _ from 'lodash';

export class CellRenderers {

    private static renderers = {
        checkbox: function(params) {
            let checkbox = document.createElement('input');
            checkbox.setAttribute('type', 'checkbox');

            if (params.value === 1 || params.value === '1') {
                checkbox.setAttribute('checked', params.value);
                checkbox.disabled = true;
            }
            return checkbox;
        },
        flag: function(params) {
            let rootElement = document.createElement('div');

            let innerContent = params.value;
            let flag = _.get(params, 'data.flag', '');

            if (!_.isEmpty(flag)) {
                let color = 'green';
                if (flag === 'red') {
                    color = 'red';
                } else if (flag === 'yellow') {
                    color = 'yellow';
                }

                let flagContent = `<i class="fa fa-flag" style="color: ${color};"></i>&nbsp;`;
                innerContent = flagContent + innerContent;
            }

            rootElement.innerHTML = innerContent;

            return rootElement;
        },
        bar: function(params) {

            let eDivPercentBar = document.createElement('div');
            let className = 'div-percent-bar';
            eDivPercentBar.style.width = '100%';
            eDivPercentBar.style.height = '100%';
            let bar = _.get(params, 'data.bar', '');
            if (!_.isEmpty(bar)) {
                let color = 'green';
                if (bar === 'red') {
                    color = 'red';
                } else if (bar === 'yellow') {
                    color = 'yellow';
                }
                className += ' cds-' + color;
            }

            eDivPercentBar.className = className;
            return eDivPercentBar;
        }
    }

    public static getRenderer(type: string): any {
        return this.renderers[type];
    }
}