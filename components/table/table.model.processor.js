"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var moment = require("moment");
var cell_renderers_1 = require("./cell-renderers");
var TableModelProcessor = /** @class */ (function () {
    function TableModelProcessor(columns, conditionalHeader) {
        this.rows = [];
        this.columns = [];
        this.columns = columns;
        this.conditionalHeader = conditionalHeader;
    }
    TableModelProcessor.prototype.process = function (model) {
        if (_.isNil(model)) {
            console.error('can not process an empty model');
            return;
        }
        this.rows = this.getRowsFromModel(model);
    };
    TableModelProcessor.prototype.getRows = function () {
        return this.rows;
    };
    TableModelProcessor.prototype.getColumns = function (rows) {
        var _this = this;
        _.each(this.columns, function (column) {
            if (column.renderer) {
                column.cellRenderer = cell_renderers_1.CellRenderers.getRenderer(column.renderer);
            }
        });
        if (!_.isUndefined(this.conditionalHeader) && _.isArray(rows)) {
            var hasGroupProperty = _.find(rows, function (i) {
                return _.has(i, _this.conditionalHeader.field);
            });
            if (hasGroupProperty) {
                this.columns.unshift(this.conditionalHeader);
            }
            else {
                _.remove(this.columns, { field: this.conditionalHeader.field });
            }
        }
        return this.columns;
    };
    TableModelProcessor.prototype.getRowsFromModel = function (model) {
        var _this = this;
        var rows = [];
        // console.log(model);
        _.each(model.data, function (item) {
            var row = {};
            var key = _.get(_.keys(item), '[0]');
            if (!_.isUndefined(key)) {
                if (!_.isEmpty(key)) {
                    if (model.dataType === 'datetime') {
                        row[model.type] = _this.formatDateTimeType(key);
                    }
                    else if (model.dataType === 'date') {
                        row[model.type] = _this.formatDateType(key);
                    }
                    else {
                        row[model.type] = key;
                    }
                }
                else {
                    row[model.type] = '';
                }
                _.each(item[key], function (cell) {
                    if (cell.dataType === 'datetime') {
                        row[cell.type] = _this.formatDateTimeType(cell.value);
                    }
                    else if (cell.dataType === 'date') {
                        row[cell.type] = _this.formatDateType(cell.value);
                    }
                    else {
                        row[cell.type] = _.isEmpty(cell.value) ? '' : cell.value;
                    }
                });
            }
            rows.push(row);
        });
        return rows;
    };
    TableModelProcessor.prototype.formatDateTimeType = function (datetime) {
        if (_.isEmpty(datetime)) {
            return '';
        }
        return moment(datetime, 'YYYYMMDDHHmm').format("MM-DD-YYYY HH:mm");
    };
    TableModelProcessor.prototype.formatDateType = function (datetime) {
        if (_.isEmpty(datetime)) {
            return '';
        }
        return moment(datetime, 'YYYY-MM-DD').format("MM-DD-YYYY");
    };
    return TableModelProcessor;
}());
exports.TableModelProcessor = TableModelProcessor;
//# sourceMappingURL=table.model.processor.js.map