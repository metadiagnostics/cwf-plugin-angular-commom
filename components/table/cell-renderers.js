"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var CellRenderers = /** @class */ (function () {
    function CellRenderers() {
    }
    CellRenderers.getRenderer = function (type) {
        return this.renderers[type];
    };
    CellRenderers.renderers = {
        checkbox: function (params) {
            var checkbox = document.createElement('input');
            checkbox.setAttribute('type', 'checkbox');
            if (params.value === 1 || params.value === '1') {
                checkbox.setAttribute('checked', params.value);
                checkbox.disabled = true;
            }
            return checkbox;
        },
        flag: function (params) {
            var rootElement = document.createElement('div');
            var innerContent = params.value;
            var flag = _.get(params, 'data.flag', '');
            if (!_.isEmpty(flag)) {
                var color = 'green';
                if (flag === 'red') {
                    color = 'red';
                }
                else if (flag === 'yellow') {
                    color = 'yellow';
                }
                var flagContent = "<i class=\"fa fa-flag\" style=\"color: " + color + ";\"></i>&nbsp;";
                innerContent = flagContent + innerContent;
            }
            rootElement.innerHTML = innerContent;
            return rootElement;
        },
        bar: function (params) {
            var eDivPercentBar = document.createElement('div');
            var className = 'div-percent-bar';
            eDivPercentBar.style.width = '100%';
            eDivPercentBar.style.height = '100%';
            var bar = _.get(params, 'data.bar', '');
            if (!_.isEmpty(bar)) {
                var color = 'green';
                if (bar === 'red') {
                    color = 'red';
                }
                else if (bar === 'yellow') {
                    color = 'yellow';
                }
                className += ' cds-' + color;
            }
            eDivPercentBar.className = className;
            return eDivPercentBar;
        }
    };
    return CellRenderers;
}());
exports.CellRenderers = CellRenderers;
//# sourceMappingURL=cell-renderers.js.map