/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import {GridOptions, RowNode} from 'ag-grid/main';

import * as _ from 'lodash';

import { TableModelProcessor } from './table.model.processor';
import { IBackendService } from '../../services/ibackend.service';
import { Guid } from '../../misc/utilities';
import { Grid } from '../grid-renderer';
import { PagingControlComponent } from "../paging/paging-control.component";

@Component({
    moduleId: module.id,
    selector: 'grid',
    styleUrls: ['./table.component.css'],
    templateUrl: './table.component.html',
    encapsulation: ViewEncapsulation.None
})

export class TableComponent {
    // There is a bug in ag-grid that requires us to set this property here - should be fixed in a recent
    // version of ag-grid
    private gridOptions: GridOptions = {
        overlayNoRowsTemplate: '<span class="ag-overlay-no-rows-center">No Records Found</span>'
    };
    private modelProcessor: TableModelProcessor = null;
    private grid : Grid = null;
    private uuid : any = null;
    private currentModel: any = null;
    private useCustomPaging: boolean = false;
    private useServeSidePaging: boolean = false;

    @ViewChild("paging")
    private pagingComponent: PagingControlComponent;

    constructor(private service : IBackendService) {
        this.uuid = Guid.newGuid();
    }

    public setServerSidePaging(useServeSidePaging: boolean) : void {
        this.useServeSidePaging = useServeSidePaging;
    }

    public initialize(modelProcessor: TableModelProcessor, grid : Grid, gridOptions?: any, paging?: boolean, pageSize?: number, useCustomPaging?: boolean) {
        this.modelProcessor = modelProcessor;
        this.grid = grid;

        _.assign(this.gridOptions, gridOptions);
        this.useCustomPaging = useCustomPaging;

        if (this.useServeSidePaging) {
            this.pagingComponent.setServerSidePaging(true);
            this.pagingComponent.initialize(this.gridOptions);
        } else if (this.useCustomPaging) {
            this.pagingComponent.initialize(this.gridOptions);
        }
        
        this.grid.initialize(this.gridOptions, this.uuid, paging, pageSize);
    }

    public getUUID(): string {
        return this.uuid;
    }

    public selectRow(index: number) {
        let node = this.gridOptions.api.getDisplayedRowAtIndex(index);

        if (!_.isEmpty(node)) {
            node.setSelected(true, true);
        }
    }

    public findRowById(value): RowNode {
        return this.gridOptions.api.getRowNode(value);
    }

    public showLoadingIndicator() {
        this.gridOptions.api.showLoadingOverlay();
    }

    public setModel(model) {
        this.currentModel = model;
        this.process(model);
    }

    public updateModel(modelToAdd: any) {
        let newModelId = this.findModelId(_.get(modelToAdd, 'data[0]'));
        let index = -1;

        if (!_.isNull(newModelId)) {
            index = _.findIndex(this.currentModel.data, (item) => {
                return this.findModelId(item) === newModelId;
            });
        }

        if (index === -1) {
            this.currentModel.data = this.currentModel.data.concat(modelToAdd.data);
        } else {
            this.currentModel.data[index] = modelToAdd.data[0];
        }

        this.process(this.currentModel);
    }

    private findModelId(modelItem: any) {
        if (_.isUndefined(modelItem)) {
            return null;
        }

        let key = _.get(_.keys(modelItem), '[0]');
        let match = _.find(modelItem[key], {type: 'id'});

        return _.get(match, 'value', null);
    }

    // from the model, figure out the rows and columns
    private process(tableModel: any) {
        this.modelProcessor.process(tableModel);

        let rows = this.modelProcessor.getRows();
        if (!Array.isArray(rows)) {
            return;
        }

        let columns = this.modelProcessor.getColumns(rows);
        if (!Array.isArray(columns) || columns.length === 0) {
            return;
        }

        this.grid.refreshGrid(this.gridOptions, rows, columns);
        this.gridOptions.api.sizeColumnsToFit();
    }
}
