export declare class TableModelProcessor {
    protected rows: any;
    protected columns: any;
    private conditionalHeader;
    constructor(columns: any, conditionalHeader?: any);
    process(model: any): void;
    getRows(): any;
    getColumns(rows?: any): any;
    protected getRowsFromModel(model: any): any[];
    protected formatDateTimeType(datetime: any): string;
    protected formatDateType(datetime: any): string;
}
