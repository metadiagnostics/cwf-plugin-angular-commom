"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var _ = require("lodash");
var ibackend_service_1 = require("../../services/ibackend.service");
var utilities_1 = require("../../misc/utilities");
var paging_control_component_1 = require("../paging/paging-control.component");
var TableComponent = /** @class */ (function () {
    function TableComponent(service) {
        this.service = service;
        // There is a bug in ag-grid that requires us to set this property here - should be fixed in a recent
        // version of ag-grid
        this.gridOptions = {
            overlayNoRowsTemplate: '<span class="ag-overlay-no-rows-center">No Records Found</span>'
        };
        this.modelProcessor = null;
        this.grid = null;
        this.uuid = null;
        this.currentModel = null;
        this.useCustomPaging = false;
        this.useServeSidePaging = false;
        this.uuid = utilities_1.Guid.newGuid();
    }
    TableComponent.prototype.setServerSidePaging = function (useServeSidePaging) {
        this.useServeSidePaging = useServeSidePaging;
    };
    TableComponent.prototype.initialize = function (modelProcessor, grid, gridOptions, paging, pageSize, useCustomPaging) {
        this.modelProcessor = modelProcessor;
        this.grid = grid;
        _.assign(this.gridOptions, gridOptions);
        this.useCustomPaging = useCustomPaging;
        if (this.useServeSidePaging) {
            this.pagingComponent.setServerSidePaging(true);
            this.pagingComponent.initialize(this.gridOptions);
        }
        else if (this.useCustomPaging) {
            this.pagingComponent.initialize(this.gridOptions);
        }
        this.grid.initialize(this.gridOptions, this.uuid, paging, pageSize);
    };
    TableComponent.prototype.getUUID = function () {
        return this.uuid;
    };
    TableComponent.prototype.selectRow = function (index) {
        var node = this.gridOptions.api.getDisplayedRowAtIndex(index);
        if (!_.isEmpty(node)) {
            node.setSelected(true, true);
        }
    };
    TableComponent.prototype.findRowById = function (value) {
        return this.gridOptions.api.getRowNode(value);
    };
    TableComponent.prototype.showLoadingIndicator = function () {
        this.gridOptions.api.showLoadingOverlay();
    };
    TableComponent.prototype.setModel = function (model) {
        this.currentModel = model;
        this.process(model);
    };
    TableComponent.prototype.updateModel = function (modelToAdd) {
        var _this = this;
        var newModelId = this.findModelId(_.get(modelToAdd, 'data[0]'));
        var index = -1;
        if (!_.isNull(newModelId)) {
            index = _.findIndex(this.currentModel.data, function (item) {
                return _this.findModelId(item) === newModelId;
            });
        }
        if (index === -1) {
            this.currentModel.data = this.currentModel.data.concat(modelToAdd.data);
        }
        else {
            this.currentModel.data[index] = modelToAdd.data[0];
        }
        this.process(this.currentModel);
    };
    TableComponent.prototype.findModelId = function (modelItem) {
        if (_.isUndefined(modelItem)) {
            return null;
        }
        var key = _.get(_.keys(modelItem), '[0]');
        var match = _.find(modelItem[key], { type: 'id' });
        return _.get(match, 'value', null);
    };
    // from the model, figure out the rows and columns
    TableComponent.prototype.process = function (tableModel) {
        this.modelProcessor.process(tableModel);
        var rows = this.modelProcessor.getRows();
        if (!Array.isArray(rows)) {
            return;
        }
        var columns = this.modelProcessor.getColumns(rows);
        if (!Array.isArray(columns) || columns.length === 0) {
            return;
        }
        this.grid.refreshGrid(this.gridOptions, rows, columns);
        this.gridOptions.api.sizeColumnsToFit();
    };
    __decorate([
        core_1.ViewChild("paging"),
        __metadata("design:type", paging_control_component_1.PagingControlComponent)
    ], TableComponent.prototype, "pagingComponent", void 0);
    TableComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'grid',
            styleUrls: ['./table.component.css'],
            templateUrl: './table.component.html',
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [ibackend_service_1.IBackendService])
    ], TableComponent);
    return TableComponent;
}());
exports.TableComponent = TableComponent;
//# sourceMappingURL=table.component.js.map