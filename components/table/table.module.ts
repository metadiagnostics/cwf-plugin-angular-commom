import {NgModule} from "@angular/core";

import { CommonModule} from "@angular/common";
import { FormsModule} from "@angular/forms";
import { AgGridModule } from "ag-grid-angular/main";
import {TableComponent} from "./table.component";
import {PagingControlModule} from "../paging/paging-control.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        PagingControlModule,
        AgGridModule.withComponents([])
    ],
    declarations: [
        TableComponent
    ],
    exports: [
        TableComponent
    ]
})
export class TableModule{}