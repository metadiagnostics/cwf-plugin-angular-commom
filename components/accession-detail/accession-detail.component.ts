import {Component, Input, OnInit} from '@angular/core';

@Component({
    moduleId   : module.id,
    selector   : 'accession-detail',
    templateUrl: './accession-detail.component.html'
})
export class AccessionDetailComponent implements OnInit{
  
  @Input()
  private accessionId: string;
  
  @Input()
  private accessionDate: string;
  
  ngOnInit() {
  }
}