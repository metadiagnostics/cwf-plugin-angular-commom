import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {AccessionDetailComponent} from "./accession-detail.component";

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        AccessionDetailComponent
    ],
    exports: [
        AccessionDetailComponent
    ]
})
export class AccessionDetailModule{}