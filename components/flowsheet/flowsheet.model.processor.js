"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var moment = require("moment");
var FlowsheetModelProcessor = /** @class */ (function () {
    function FlowsheetModelProcessor(type) {
        this.type = '';
        this.rows = [];
        this.columns = [];
        this.headerTitle = '';
        this.type = type;
    }
    FlowsheetModelProcessor.prototype.setHeaderTitle = function (title) {
        this.headerTitle = title;
    };
    FlowsheetModelProcessor.prototype.process = function (model) {
        if (_.isNil(model)) {
            console.error('can not process an empty model');
            return;
        }
        // NOTE: The order does matter here...
        this.columns = this.getColumnsFromModel(model);
        this.rows = this.getRowsFromModel(model);
        this.columns = this.formatHeaders(this.columns);
    };
    FlowsheetModelProcessor.prototype.getRows = function () {
        return this.rows;
    };
    FlowsheetModelProcessor.prototype.getColumns = function () {
        return this.columns;
    };
    FlowsheetModelProcessor.prototype.getColumnsFromModel = function (model) {
        var modelDatum = _.get(model, 'data', []);
        if (_.isEmpty(modelDatum)) {
            return [];
        }
        var tempColumns = [];
        _.forEach(modelDatum, function (data) {
            var types = Object.keys(data);
            _.forEach(types, function (type) {
                var details = _.get(data, type);
                _.forEach(details, function (detail) {
                    var timestamp = _.get(detail, 'timestamp');
                    if (!_.isUndefined(timestamp)) {
                        tempColumns.push(timestamp);
                    }
                });
            });
        });
        // dedupe the columns, and sort them
        // remove any duplicates
        tempColumns = tempColumns.filter(function (elem, index) {
            return index === tempColumns.indexOf(elem);
        });
        tempColumns.sort();
        var columns = [];
        _.forEach(tempColumns, function (column) {
            var columnValue = {};
            if (column !== undefined) {
                columnValue.timestamp = column;
                columnValue.time = column;
                columns.push(columnValue);
            }
        });
        return columns;
    };
    FlowsheetModelProcessor.prototype.getRowsFromModel = function (model) {
        var dataModel = _.get(model, 'data', []);
        if (_.isEmpty(dataModel)) {
            return [];
        }
        var rows = [];
        var numberOfRows = dataModel.length;
        // we have to use old fashion loop since the order is very important
        for (var rowIndex = 0; rowIndex < numberOfRows; rowIndex++) {
            if (rowIndex === 0) {
                // console.log(dataModel[rowIndex]);
            }
            var row = {};
            row.columnName = row.originalName = dataModel[rowIndex].name;
            row.type = '' + dataModel[rowIndex].type;
            row.resourceID = dataModel[rowIndex].resourceID;
            row.governance = dataModel[rowIndex].governance;
            row.values = dataModel[rowIndex].values;
            for (var i = 0; i < this.columns.length; i++) {
                var cellValue = this.columns[i].timestamp;
                if (cellValue !== undefined) {
                    row[cellValue] = '';
                }
            }
            var keys = _.keys(row.values);
            var length_1 = keys.length;
            for (var j = 0; j < length_1; j++) {
                var currentKey = 'data[' + rowIndex + '].values[' + j + ']';
                var newField = _.get(model, currentKey);
                if (!_.isUndefined(newField)) {
                    row[newField.timestamp] = newField.value;
                }
            }
            rows.push(row);
        }
        return rows;
    };
    FlowsheetModelProcessor.prototype.formatHeaders = function (columnsDetails) {
        if (_.isEmpty(columnsDetails)) {
            return [];
        }
        var retVal = [];
        var value = {};
        value.headerName = this.headerTitle; //'I & O';
        value.field = 'columnName';
        // console.log('headerName: ' + value.headerName + ' field is: ' + value.field);
        retVal.push(value);
        for (var key in columnsDetails) {
            value = {};
            if (columnsDetails[key].time !== undefined) {
                value.headerName = this.format(columnsDetails[key].timestamp);
                value.field = columnsDetails[key].timestamp;
                // console.log('headerName: ' + value.headerName + ' field is: ' + value.field);
                retVal.push(value);
            }
        }
        return retVal;
    };
    FlowsheetModelProcessor.prototype.format = function (timestamp) {
        if (!moment(timestamp, 'YYYYMMDDHHmmss').isValid()) {
            return '';
        }
        return moment(timestamp, "YYYYMMDDHHmmss").format('DD-MMM HH:mm');
    };
    FlowsheetModelProcessor.prototype.setType = function (type) {
        this.type = type;
    };
    return FlowsheetModelProcessor;
}());
exports.FlowsheetModelProcessor = FlowsheetModelProcessor;
//# sourceMappingURL=flowsheet.model.processor.js.map