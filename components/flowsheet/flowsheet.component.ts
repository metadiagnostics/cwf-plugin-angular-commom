/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import {Component, ViewEncapsulation, OnInit, ViewChild} from '@angular/core';
import { GridOptions } from 'ag-grid/main';
import * as _ from 'lodash';

import { FlowsheetModelProcessor } from './flowsheet.model.processor';
import { IBackendService } from '../../services/ibackend.service';

import { Grid } from '../grid-renderer';
import { Guid } from '../../misc/utilities';
import {PagingControlComponent} from "../paging/paging-control.component";

@Component({
               moduleId: module.id,
               selector: 'flowsheet',
               templateUrl: './flowsheet.component.html',
               styleUrls: ['./I-O/renderer.css'],
               encapsulation: ViewEncapsulation.None
           })

export class FlowsheetComponent implements OnInit{
    private gridOptions: GridOptions = {
        overlayNoRowsTemplate: '<span class="ag-overlay-no-rows-center">No Records Found</span>'
    };
    private modelProcessor: FlowsheetModelProcessor = null;
    private grid : Grid = null;
    private uuid : any = null;
    private type: string = '';
    private lastModel: any = null;
    private useCustomPaging: boolean = false;

    @ViewChild("paging")
    private pagingComponent: PagingControlComponent;


    constructor(private service : IBackendService) {
    }

    public getGrid() : Grid {
        return this.grid;
    }

    public initialize(modelProcessor: FlowsheetModelProcessor, grid : Grid, gridOptions: any, pageSize : number = 30, useCustomPaging?: boolean) {
        this.uuid = Guid.newGuid();
        this.modelProcessor = modelProcessor;
        this.grid = grid;
        _.assign(this.gridOptions, gridOptions);

        this.useCustomPaging = useCustomPaging;

        if (this.useCustomPaging) {
            this.pagingComponent.initialize(this.gridOptions);
        }
        this.grid.initialize(this.gridOptions, this.uuid, true, pageSize);
    }

    public showLoadingIndicator() {
        this.gridOptions.api.showLoadingOverlay();
    }

    public setType(type: string) : void {
        this.type = type;
    }

    public getUUID(): string {
        return this.uuid;
    }

    public getType() : string {
        return this.type;
    }

    protected isModelMine(model : any ) : boolean {
        let retVal = !_.isNil(model)
            && (_.get(model, 'metadata.uuid') === this.uuid || _.get(model, 'metadata.uuid')  === 'noop');
        return retVal;
    }

    ngOnInit() {
        this.service.getModelChangeSource$()
            .subscribe(
                model => {
                    if (this.isModelMine(model)) {

                        delete model.metadata.uuid;
                        this.process(model);
                    }
                },
                err => console.error(err)
            );
    }

    private getResourceIndex(items: Array<any>, newResourceID: string) : number {
        let resourceIndex = 0;

        while (resourceIndex < items.length) {
            if (items[resourceIndex].resourceID === newResourceID ) {
                return resourceIndex;
            }
             resourceIndex ++;
        }

        return -1;
    }

    // from the model, figure out the rows and columns
    private process(flowsheetModel: any) : void {
        this.modelProcessor.process(flowsheetModel);

        let rows = this.modelProcessor.getRows();
        let columns = this.modelProcessor.getColumns();

        // TODO only for hard coded testing...
        // this.gridOptions.columnDefs = columnDefs;
        // this.gridOptions.rowData = rowData;
        // end of TODO

        this.lastModel = flowsheetModel;

        this.grid.refreshGrid(this.gridOptions, rows, columns);
        this.grid.selectFirstRow(this.gridOptions);
    }

    public setModel(model: any) : void {
        this.process(model);
    }

    public updatedModel(modelToAdd: any) {
        if (_.isEmpty(modelToAdd.data)) {
            console.error('update with empty model');
            return;
        }

        let updatedResourceID = modelToAdd.data[0].resourceID;
        if (_.isEmpty(updatedResourceID)) {
            console.error('update is missing an resourceID setting');
            return;
        }

        let newValues = modelToAdd.data[0].values;
        if (_.isEmpty(newValues)) {
            console.error('update is missing an cell values setting');
            return;
        }


        let resourceIndex = this.getResourceIndex(this.lastModel.data, updatedResourceID);
        if (resourceIndex === -1) {
            this.lastModel.data = this.lastModel.data.concat(modelToAdd.data);
            this.process(this.lastModel);
            return;
        }

        let newCellValue  = modelToAdd.data[0].values[0];
        this.morphModels(resourceIndex, newCellValue);
    }

    private morphModels(updatedResourceindex: number, newValue: any){
        let cellValues = this.lastModel.data[updatedResourceindex];
        let index = 0;
        while(index < 0) {
            let cell = cellValues[index];
            if (cell.timestamp === newValue.timestamp) {
                cell.value = newValue.value;
                return;
            }
            index ++;
        }
    }

    public selectRow(index: number) {
        let node = this.gridOptions.api.getDisplayedRowAtIndex(index);

        if (!_.isEmpty(node)) {
            node.setSelected(true, true);
        }
    }
}
