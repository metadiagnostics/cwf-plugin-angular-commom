"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var moment = require("moment");
var linechart_utils_1 = require("../charts/linechart.utils");
var utilities_1 = require("../../misc/utilities");
var FlowsheetPluginHelper = /** @class */ (function () {
    function FlowsheetPluginHelper() {
    }
    FlowsheetPluginHelper.getFormattedDateParam = function (date, daysToAdd) {
        return moment(date).add(daysToAdd, 'day').format('YYYYMMDDHHmmss');
    };
    FlowsheetPluginHelper.getDataToPlot = function (row, columns, includeAll) {
        // lets gather the column date that are visible...
        if (includeAll === void 0) { includeAll = true; }
        var columnFields = [];
        _.forEach(columns, function (column) {
            columnFields.push(column.colId);
        });
        var data = [];
        var fields = Object.keys(row);
        _.forEach(fields, function (field) {
            if (includeAll || _.includes(columnFields, field)) {
                var timeStamp = utilities_1.Utilities.formatTimestamp(field, utilities_1.Utilities.TIMESTAMP_OUTPUT_FORMAT2);
                if (!_.isEmpty(timeStamp)) {
                    // it is a valid timestamp
                    // we need it as an X-axis entry
                    var value = _.get(row, field);
                    // console.log(`adding value ${value}`);
                    if (!_.isEmpty(value)) {
                        data.push(new linechart_utils_1.LineChartElement(timeStamp, value));
                    }
                }
            }
        });
        return data;
    };
    return FlowsheetPluginHelper;
}());
exports.FlowsheetPluginHelper = FlowsheetPluginHelper;
//# sourceMappingURL=flowsheet.plugin.helper.js.map