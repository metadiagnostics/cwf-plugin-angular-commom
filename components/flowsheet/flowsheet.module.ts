import {NgModule} from "@angular/core";

import { CommonModule} from "@angular/common";
import { AgGridModule } from "ag-grid-angular/main";
import {FlowsheetComponent} from "./flowsheet.component";
import {FullWidthCellRenderer} from "./full-width-cell-renderer";
import {PagingControlModule} from "../paging/paging-control.module";

@NgModule({
    imports: [
        CommonModule,
        PagingControlModule,
        AgGridModule.withComponents(
        [
            FullWidthCellRenderer
        ])
    ],
    declarations: [
        FlowsheetComponent,
        FullWidthCellRenderer
    ],
    exports: [
        FlowsheetComponent
    ]
})
export class FlowsheetModule{}