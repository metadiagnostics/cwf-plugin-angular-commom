import { Observable } from 'rxjs/Observable';
import { Grid } from '../grid-renderer';
export declare class FlowsheetGrid extends Grid {
    private _selectedRow;
    selectedRowChanged$: Observable<any>;
    private _visibleColumns;
    visibleColumnsChanged$: Observable<any>;
    constructor();
    initialize(gridOptions: any, uuid: any, paging?: boolean, pageSize?: number): void;
    protected configureColumns(columns: any): Array<any>;
    protected configureRows(rows: any): Array<any>;
    selectFirstRow(gridOptions: any): void;
    static cellRender(params: any): HTMLDivElement;
}
