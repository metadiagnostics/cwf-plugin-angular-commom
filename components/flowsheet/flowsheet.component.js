"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var _ = require("lodash");
var ibackend_service_1 = require("../../services/ibackend.service");
var utilities_1 = require("../../misc/utilities");
var paging_control_component_1 = require("../paging/paging-control.component");
var FlowsheetComponent = /** @class */ (function () {
    function FlowsheetComponent(service) {
        this.service = service;
        this.gridOptions = {
            overlayNoRowsTemplate: '<span class="ag-overlay-no-rows-center">No Records Found</span>'
        };
        this.modelProcessor = null;
        this.grid = null;
        this.uuid = null;
        this.type = '';
        this.lastModel = null;
        this.useCustomPaging = false;
    }
    FlowsheetComponent.prototype.getGrid = function () {
        return this.grid;
    };
    FlowsheetComponent.prototype.initialize = function (modelProcessor, grid, gridOptions, pageSize, useCustomPaging) {
        if (pageSize === void 0) { pageSize = 30; }
        this.uuid = utilities_1.Guid.newGuid();
        this.modelProcessor = modelProcessor;
        this.grid = grid;
        _.assign(this.gridOptions, gridOptions);
        this.useCustomPaging = useCustomPaging;
        if (this.useCustomPaging) {
            this.pagingComponent.initialize(this.gridOptions);
        }
        this.grid.initialize(this.gridOptions, this.uuid, true, pageSize);
    };
    FlowsheetComponent.prototype.showLoadingIndicator = function () {
        this.gridOptions.api.showLoadingOverlay();
    };
    FlowsheetComponent.prototype.setType = function (type) {
        this.type = type;
    };
    FlowsheetComponent.prototype.getUUID = function () {
        return this.uuid;
    };
    FlowsheetComponent.prototype.getType = function () {
        return this.type;
    };
    FlowsheetComponent.prototype.isModelMine = function (model) {
        var retVal = !_.isNil(model)
            && (_.get(model, 'metadata.uuid') === this.uuid || _.get(model, 'metadata.uuid') === 'noop');
        return retVal;
    };
    FlowsheetComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.service.getModelChangeSource$()
            .subscribe(function (model) {
            if (_this.isModelMine(model)) {
                delete model.metadata.uuid;
                _this.process(model);
            }
        }, function (err) { return console.error(err); });
    };
    FlowsheetComponent.prototype.getResourceIndex = function (items, newResourceID) {
        var resourceIndex = 0;
        while (resourceIndex < items.length) {
            if (items[resourceIndex].resourceID === newResourceID) {
                return resourceIndex;
            }
            resourceIndex++;
        }
        return -1;
    };
    // from the model, figure out the rows and columns
    FlowsheetComponent.prototype.process = function (flowsheetModel) {
        this.modelProcessor.process(flowsheetModel);
        var rows = this.modelProcessor.getRows();
        var columns = this.modelProcessor.getColumns();
        // TODO only for hard coded testing...
        // this.gridOptions.columnDefs = columnDefs;
        // this.gridOptions.rowData = rowData;
        // end of TODO
        this.lastModel = flowsheetModel;
        this.grid.refreshGrid(this.gridOptions, rows, columns);
        this.grid.selectFirstRow(this.gridOptions);
    };
    FlowsheetComponent.prototype.setModel = function (model) {
        this.process(model);
    };
    FlowsheetComponent.prototype.updatedModel = function (modelToAdd) {
        if (_.isEmpty(modelToAdd.data)) {
            console.error('update with empty model');
            return;
        }
        var updatedResourceID = modelToAdd.data[0].resourceID;
        if (_.isEmpty(updatedResourceID)) {
            console.error('update is missing an resourceID setting');
            return;
        }
        var newValues = modelToAdd.data[0].values;
        if (_.isEmpty(newValues)) {
            console.error('update is missing an cell values setting');
            return;
        }
        var resourceIndex = this.getResourceIndex(this.lastModel.data, updatedResourceID);
        if (resourceIndex === -1) {
            this.lastModel.data = this.lastModel.data.concat(modelToAdd.data);
            this.process(this.lastModel);
            return;
        }
        var newCellValue = modelToAdd.data[0].values[0];
        this.morphModels(resourceIndex, newCellValue);
    };
    FlowsheetComponent.prototype.morphModels = function (updatedResourceindex, newValue) {
        var cellValues = this.lastModel.data[updatedResourceindex];
        var index = 0;
        while (index < 0) {
            var cell = cellValues[index];
            if (cell.timestamp === newValue.timestamp) {
                cell.value = newValue.value;
                return;
            }
            index++;
        }
    };
    FlowsheetComponent.prototype.selectRow = function (index) {
        var node = this.gridOptions.api.getDisplayedRowAtIndex(index);
        if (!_.isEmpty(node)) {
            node.setSelected(true, true);
        }
    };
    __decorate([
        core_1.ViewChild("paging"),
        __metadata("design:type", paging_control_component_1.PagingControlComponent)
    ], FlowsheetComponent.prototype, "pagingComponent", void 0);
    FlowsheetComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'flowsheet',
            templateUrl: './flowsheet.component.html',
            styleUrls: ['./I-O/renderer.css'],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [ibackend_service_1.IBackendService])
    ], FlowsheetComponent);
    return FlowsheetComponent;
}());
exports.FlowsheetComponent = FlowsheetComponent;
//# sourceMappingURL=flowsheet.component.js.map