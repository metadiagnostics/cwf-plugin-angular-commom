/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import * as _ from 'lodash';
import * as moment from 'moment';


export interface RowSetting {
    originalName: string;
    name: string;
    type: string;
    governance: string;
    values : Array<any>;
    headerName: string;
    columnName : any;
    resourceID: string;
}

export interface ColumnHeader {
    time: string;
    timestamp : string;
}

export class FlowsheetModelProcessor {
    protected type: string = '';
    protected rows: Array<RowSetting> = [];
    protected columns: Array<ColumnHeader>  = [];
    private headerTitle : string = '';

    constructor(type?: string) {
        this.type = type;
    }

    protected setHeaderTitle(title: string) {
        this.headerTitle = title;
    }


    public process(model: any) : void {
        if (_.isNil(model)) {
            console.error('can not process an empty model');
            return;
        }

        // NOTE: The order does matter here...

        this.columns = this.getColumnsFromModel(model);
        this.rows = this.getRowsFromModel(model);
        this.columns = this.formatHeaders(this.columns);
    }

    public getRows(): any {
        return this.rows;
    }

    public getColumns(): any {
        return this.columns;
    }

    private getColumnsFromModel(model) : Array<ColumnHeader> {
        let modelDatum = _.get(model, 'data', []);
        if (_.isEmpty(modelDatum)) {
            return [];
        }

        let tempColumns = [];

        _.forEach(modelDatum, function (data) {
            let types = Object.keys(data);
            _.forEach(types, function (type) {
                let details = _.get(data, type);
                _.forEach(details, function (detail) {
                    let timestamp = _.get(detail, 'timestamp');
                    if (!_.isUndefined(timestamp)) {
                        tempColumns.push(timestamp);
                    }
                });
            });
        });


        // dedupe the columns, and sort them
        // remove any duplicates
        tempColumns = tempColumns.filter(function (elem, index) {
            return index === tempColumns.indexOf(elem);
        });

        tempColumns.sort();

        let columns : Array<ColumnHeader> = [];
        _.forEach(tempColumns, function (column) {
            let columnValue = {} as ColumnHeader;
            if (column !== undefined) {
                columnValue.timestamp = column;
                columnValue.time = column
                columns.push(columnValue);
            }
        });

        return columns;
    }

    private getRowsFromModel(model) : Array<RowSetting> {

        let dataModel = _.get(model, 'data', []);
        if (_.isEmpty(dataModel)) {
            return [];
        }

        let rows: Array<RowSetting> = [];
        let numberOfRows = dataModel.length;

        // we have to use old fashion loop since the order is very important
        for (let rowIndex = 0; rowIndex < numberOfRows; rowIndex++) {
            if (rowIndex === 0) {
                // console.log(dataModel[rowIndex]);
            }

            let row : RowSetting = {} as RowSetting;
            row.columnName = row.originalName = dataModel[rowIndex].name;
            row.type = ''+ dataModel[rowIndex].type;
            row.resourceID = dataModel[rowIndex].resourceID;
            row.governance = dataModel[rowIndex].governance;
            row.values = dataModel[rowIndex].values;

            for (let i = 0; i < this.columns.length; i ++) {
                let cellValue = this.columns[i].timestamp;
                if (cellValue !== undefined) {
                    row[cellValue] = '';
                }
            }

            let keys = _.keys(row.values);
            let length = keys.length;

            for (let j = 0; j < length; j++) {
                let currentKey = 'data[' + rowIndex + '].values[' + j + ']';
                let newField = _.get(model, currentKey);
                if (! _.isUndefined(newField)) {
                    row[newField.timestamp] = newField.value;
                }
            }

            rows.push(row);
        }

        return rows;
    }

    private formatHeaders(columnsDetails): Array<any> {
        if (_.isEmpty(columnsDetails)) {
            return [];
        }

        var retVal = [];
        var value: any = {};

        value.headerName = this.headerTitle; //'I & O';
        value.field = 'columnName';
        // console.log('headerName: ' + value.headerName + ' field is: ' + value.field);
        retVal.push(value);

        for (var key in columnsDetails) {
            value = {};
            if (columnsDetails[key].time !== undefined) {
                value.headerName = this.format(columnsDetails[key].timestamp);

                value.field = columnsDetails[key].timestamp;
                // console.log('headerName: ' + value.headerName + ' field is: ' + value.field);
                retVal.push(value);
            }
        }

        return retVal;
    }

    private format(timestamp): string {
        if (!moment(timestamp, 'YYYYMMDDHHmmss').isValid()) {
            return '';
        }

        return moment(timestamp, "YYYYMMDDHHmmss").format('DD-MMM HH:mm');
    }

    public setType(type: string) : void {
        this.type = type;
    }

    public

}

