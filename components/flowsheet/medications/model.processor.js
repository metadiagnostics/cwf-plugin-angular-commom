"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var flowsheet_model_processor_1 = require("../flowsheet.model.processor");
var constants_1 = require("./constants");
var MedicationModelProcessor = /** @class */ (function (_super) {
    __extends(MedicationModelProcessor, _super);
    function MedicationModelProcessor() {
        var _this = _super.call(this) || this;
        _this.setHeaderTitle(constants_1.MedicationConstants.HEADER_TITLE);
        return _this;
    }
    MedicationModelProcessor.prototype.getResourceID = function (row) {
        if (_.isEmpty(this.rows)) {
            return '';
        }
        if (_.isEmpty(row)) {
            return this.rows[0].resourceID;
        }
        return row.resourceID;
    };
    return MedicationModelProcessor;
}(flowsheet_model_processor_1.FlowsheetModelProcessor));
exports.MedicationModelProcessor = MedicationModelProcessor;
//# sourceMappingURL=model.processor.js.map