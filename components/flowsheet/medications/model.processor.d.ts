import { FlowsheetModelProcessor } from '../flowsheet.model.processor';
export declare class MedicationModelProcessor extends FlowsheetModelProcessor {
    constructor();
    getResourceID(row?: any): string;
}
