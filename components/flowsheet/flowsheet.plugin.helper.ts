/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import * as _ from 'lodash';
import * as moment from 'moment';
import {LineChartElement} from "../charts/linechart.utils";
import {Utilities} from "../../misc/utilities";

export abstract class FlowsheetPluginHelper {
    protected static getFormattedDateParam(date: Date, daysToAdd: number) : string {
        return moment(date).add(daysToAdd, 'day').format('YYYYMMDDHHmmss');
    }

    protected static getDataToPlot(row: any, columns: Array<any>, includeAll : boolean = true) : Array<LineChartElement> {
        // lets gather the column date that are visible...

        let columnFields: Array<any> = [];
        _.forEach(columns, function (column) {
            columnFields.push(column.colId);
        });

        let data : Array<LineChartElement> = [];
        let fields = Object.keys(row);

        _.forEach(fields, function (field) {
            if ( includeAll || _.includes(columnFields, field)) {
                let timeStamp = Utilities.formatTimestamp(field, Utilities.TIMESTAMP_OUTPUT_FORMAT2);
                if (! _.isEmpty(timeStamp)) {
                    // it is a valid timestamp
                    // we need it as an X-axis entry
                    let value = _.get(row,field);
                    // console.log(`adding value ${value}`);
                    if (! _.isEmpty(value)) {
                        data.push(new LineChartElement(timeStamp, value));
                    }
                }
            }});

        return data;
    }

    protected abstract initializeFlowsheet() : void;
    protected abstract getModelQueryParams() : any;
    protected abstract activatePlugin(response) : void;
}
