/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import * as moment from 'moment';
import * as _ from 'lodash';

import { FlowsheetModelProcessor, RowSetting} from '../flowsheet.model.processor';
import { TotalColumnProvider } from './total.columns';
import { IOConstants } from './constants';

export class IOModelProcessor extends FlowsheetModelProcessor {
    private hasInputRows : boolean = false;
    private hasOutputRows : boolean = false;

    constructor(private totalType : string) {
        super();
        this.setHeaderTitle(IOConstants.HEADER_TITLE);
    }

    public process(model: any) {
        super.process(model);
        this.postProcess();
    }

    protected postProcess() {
        if (_.isEmpty(this.getRows())) {
            return;
        }

        let currentDate = moment().format('YYYYMMDDHH');
        let i_o_total = new TotalColumnProvider(this.getRows(), this.getColumns(), currentDate, this.totalType);

        this.columns = i_o_total.getColumns();
        this.rows = i_o_total.getRows();

        this.addFullRows();
    }

    private addFullRows() {

        let index = 0;
        let firstInputIndex = -1;
        let firstOutputIndex = -1;

        // assumption here is that the input rows and output rows are returned together and not
        // interspersed...
        // lets look for input and output indeces...

        while (index < this.rows.length) {
            let type = this.rows[index].type + '';

            if (type === IOConstants.INPUT_TYPE) {
                if (firstInputIndex === -1) {
                    firstInputIndex = index;
                }
            } else if (type === IOConstants.OUTPUT_TYPE){
                if (firstOutputIndex === -1) {
                    firstOutputIndex = index;
                }
            }

            index++;
        }

        if (firstInputIndex !== -1) {
            this.hasInputRows = true;
            let intake = {} as RowSetting;
            intake.headerName = IOConstants.INTAKE;
            this.rows.splice(firstInputIndex, 0, intake);
        }

        if (firstOutputIndex !== -1) {
            let output = {} as RowSetting;
            output.headerName = IOConstants.OUTPUT;
            this.hasOutputRows = true;

            // have we added an Intake row already?

            let location = (this.hasInputRows) ? firstOutputIndex + 1 : firstOutputIndex
            this.rows.splice(location, 0, output);
        }
    }

    public hasInput() : boolean {
        return this.hasInputRows;
    }

    public hasOutput() : boolean {
        return this.hasOutputRows;
    }
}
