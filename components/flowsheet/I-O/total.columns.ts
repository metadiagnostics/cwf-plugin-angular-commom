/*
 *  Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import _ = require('lodash');
import * as moment from 'moment';

import { IOConstants } from './constants';


export class TotalColumnProvider {
    constructor(private rows: any[], private columns: any[], private todayDate, private totalType) {
        // console.log('before rows: ' + rows);
        this.adjustTotals();
    }

    private isRelevantDay(time) : boolean {
        if (this.totalType === IOConstants.DAILY_TOTAL) {
            return this.isSameDay(time, this.todayDate);
        }

        if (this.totalType === IOConstants._24_HOUR_TOTAL) {
            return this.isLast24Hours(time, this.todayDate);
        }

        return false;
    }

    private isSameDay(time, todayDate) : boolean {
        if (!moment(time, 'YYYYMMDDHHmmss').isValid()) {
            return false;
        }

        var year = time.substring(0, 4);
        var month = time.substring(4, 6);
        var day = time.substring(6, 8);

        var testDay = new Date(year, month, day);

        year = todayDate.substring(0, 4);
        month = todayDate.substring(4, 6);
        day = todayDate.substring(6, 8);

        var today =  new Date(year, month, day);

        let retVal =
            testDay.getFullYear() === today.getFullYear() &&
            testDay.getMonth() === today.getMonth() &&
            testDay.getDate() === today.getDate();

        return retVal;
    }

    private isLast24Hours(time, todayDate) : boolean {
        if (!moment(time, 'YYYYMMDDHHmmss').isValid()) {
            return false;
        }

        var year = time.substring(0, 4);
        var month = time.substring(4, 6);
        var day = time.substring(6, 8);
        var hour = time.substring(8,10);
        // console.log('test day  --- year: '+ year + ' month: ' + month + 'day: ' + day + ' hour: ' + hour);

        var testDay = new Date(year, month, day, hour);

        year = todayDate.substring(0, 4);
        month = todayDate.substring(4, 6);
        day = todayDate.substring(6, 8);
        hour = todayDate.substring(8,10);

        // console.log('today --- year: '+ year + ' month: ' + month + 'day: ' + day + ' hour: ' + hour);

        var today =  new Date(year, month, day, hour);

        let retVal =
            testDay.getFullYear() === today.getFullYear() &&
            testDay.getMonth() === today.getMonth() &&
            (
                (testDay.getDate() === today.getDate() || // same day
                    (today.getDate() - testDay.getDate() === 1 && testDay.getHours() >= today.getHours()) // yesterday and hours higher than now
                )
            );

        return retVal;
    }

    private adjustTotals() : void {
        let that = this;


        _.forEach(this.rows, function (row) {
            let rowType = row.type;
            let rowSum = 0;

            // let's all of the keys for this row and pick out the ones that
            // are integers and check whether the dates are relevant or not...

            Object.keys(row).map(key => {
                let rowValue = parseInt(row[key]);

                if (!isNaN(rowValue)) {
                    let dateToCompare = key;
                    let relevantDate = that.isRelevantDay(dateToCompare);

                    if (relevantDate) {
                        if (rowType === IOConstants.OUTPUT_TYPE) {
                            rowSum = rowSum - rowValue;
                        } else {
                            rowSum = rowSum + rowValue;
                        }
                    }
                }
            });

            row.total = rowSum;
        });

        this.columns.push({headerName: this.GetHeaderName(), field: 'total'});

        // _.forEach(this.columns, column => {
        //     console.log(column);
        // });

    }

    private GetHeaderName() : string {
        let headerName = IOConstants.TOTAL + ' (';
        if (this.totalType === IOConstants._24_HOUR_TOTAL) {
            headerName += 'last 24 hours)';
        } else if (this.totalType === IOConstants.DAILY_TOTAL) {
            headerName += 'today)';
        } else {
            console.error('undefined total type');
        }

        return headerName;
    }

    public getRows() : Array<any> {
        return this.rows;
    }

    public getColumns() : Array<any> {
        return this.columns;
    }
}
