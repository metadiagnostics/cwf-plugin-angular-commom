export declare class TotalColumnProvider {
    private rows;
    private columns;
    private todayDate;
    private totalType;
    constructor(rows: any[], columns: any[], todayDate: any, totalType: any);
    private isRelevantDay(time);
    private isSameDay(time, todayDate);
    private isLast24Hours(time, todayDate);
    private adjustTotals();
    private GetHeaderName();
    getRows(): Array<any>;
    getColumns(): Array<any>;
}
