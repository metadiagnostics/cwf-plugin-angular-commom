"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
var _ = require("lodash");
var grid_renderer_1 = require("../../grid-renderer");
var full_width_cell_renderer_1 = require("../full-width-cell-renderer");
var constants_1 = require("./constants");
var I_O_Grid = /** @class */ (function (_super) {
    __extends(I_O_Grid, _super);
    function I_O_Grid() {
        var _this = _super.call(this) || this;
        _this._selectedRow = new BehaviorSubject_1.BehaviorSubject(null);
        _this.selectedRowChanged$ = _this._selectedRow.asObservable();
        _this._visibleColumns = new BehaviorSubject_1.BehaviorSubject(null);
        _this.visibleColumnsChanged$ = _this._visibleColumns.asObservable();
        return _this;
    }
    I_O_Grid.prototype.isInputOrOutputRow = function (node) {
        return this.isInputRow(node) || this.isOutputRow(node);
    };
    I_O_Grid.prototype.initialize = function (gridOptions, uuid, paging, pageSize) {
        var _this = this;
        if (!_.isUndefined(paging) && paging) {
            gridOptions.pagination = true;
            gridOptions.paginationPageSize = _.isUndefined(pageSize) ? 50 : pageSize;
            _super.prototype.initialize.call(this, gridOptions, uuid, gridOptions.pagination, gridOptions.paginationPageSize);
        }
        else {
            _super.prototype.initialize.call(this, gridOptions, uuid);
        }
        gridOptions.enableSorting = false;
        gridOptions.suppressColumnVirtualisation = false;
        gridOptions.rowSelection = 'single';
        gridOptions.enableColResize = false;
        gridOptions.suppressMovableColumns = true;
        gridOptions.suppressCellSelection = true;
        gridOptions.suppressLoadingOverlay = false;
        gridOptions.emptyHeight = '200px';
        gridOptions.getBusinessKeyForNode = function (node) {
            return node.lastChild ? 'NetIntake' : 'nosideeffect';
        };
        gridOptions.isFullWidthCell = function (node) {
            return _this.isInputOrOutputRow(node);
        };
        gridOptions.onSelectionChanged = function ($event) {
            var selectedRows = $event.api.getSelectedRows();
            if (_.isArray(selectedRows) && !_.isEmpty(selectedRows)) {
                _this._selectedRow.next(selectedRows[0]);
            }
        };
        gridOptions.onVirtualColumnsChanged = function ($event) {
            _this._visibleColumns.next($event.columnApi.
                columnController.
                allDisplayedVirtualColumns);
        };
        gridOptions.fullWidthCellRendererFramework = full_width_cell_renderer_1.FullWidthCellRenderer;
    };
    I_O_Grid.prototype.configureColumns = function (columns) {
        _.forEach(columns, function (column) {
            column.cellRenderer = I_O_Grid.cellRender;
            // column.cellStyle = {'text-align': 'center'};
            column.editable = false;
            column.width = 110;
        });
        if (columns !== undefined && columns.length > 0) {
            columns[0].pinned = 'left';
            columns[0].width = 330;
            columns[columns.length - 1].width = 180;
        }
        return columns;
    };
    I_O_Grid.prototype.configureRows = function (rows) {
        return rows;
    };
    I_O_Grid.prototype.isInputRow = function (node) {
        return node.data.headerName === constants_1.IOConstants.INTAKE;
    };
    I_O_Grid.prototype.isOutputRow = function (node) {
        return node.data.headerName === constants_1.IOConstants.OUTPUT;
    };
    I_O_Grid.prototype.selectFirstRow = function (gridOptions) {
        grid_renderer_1.Grid.selectRow(gridOptions, 1);
    };
    I_O_Grid.cellRender = function (params) {
        var currentCell = grid_renderer_1.Grid.getCellSettings(params.colDef.field, params.data.values);
        // TODO uncomment this when it is time...
        // lets not draw the flag since no valida data is coming back...
        // if (!_.isEmpty(currentCell) && !_.isEmpty(currentCell.info)) {
        //     console.error(currentCell.info);
        //     return Grid.flag(currentCell.value, currentCell.info);
        // }
        if (params.value === 'OVERDUE' || params.value === 'DUE') {
            return grid_renderer_1.Grid.dueOverDueCellRenderer(params);
        }
        return grid_renderer_1.Grid.normal(params);
    };
    return I_O_Grid;
}(grid_renderer_1.Grid));
exports.I_O_Grid = I_O_Grid;
//# sourceMappingURL=renderer.js.map