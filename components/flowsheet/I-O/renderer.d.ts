import { Grid } from '../../grid-renderer';
import { Observable } from 'rxjs/Observable';
export declare class I_O_Grid extends Grid {
    private _selectedRow;
    selectedRowChanged$: Observable<any>;
    private _visibleColumns;
    visibleColumnsChanged$: Observable<any>;
    constructor();
    private isInputOrOutputRow(node);
    initialize(gridOptions: any, uuid: any, paging?: boolean, pageSize?: number): void;
    protected configureColumns(columns: any): Array<any>;
    protected configureRows(rows: any): Array<any>;
    isInputRow(node: any): boolean;
    isOutputRow(node: any): boolean;
    selectFirstRow(gridOptions: any): void;
    static cellRender(params: any): HTMLDivElement;
}
