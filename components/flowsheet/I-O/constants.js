"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var IOConstants = /** @class */ (function () {
    function IOConstants() {
    }
    IOConstants.INTAKE = 'Intake';
    IOConstants.OUTPUT = 'Output';
    IOConstants.OUTPUT_TYPE = '-1';
    IOConstants.INPUT_TYPE = '1';
    IOConstants.I_O = 'I_O';
    IOConstants.TOTAL = 'Total';
    IOConstants.DAILY_TOTAL = 'sameDayTotal';
    IOConstants._24_HOUR_TOTAL = '24HourTotal';
    IOConstants.HEADER_TITLE = 'I & O';
    IOConstants.HEADER_TITLE_EX = 'I & O';
    return IOConstants;
}());
exports.IOConstants = IOConstants;
//# sourceMappingURL=constants.js.map