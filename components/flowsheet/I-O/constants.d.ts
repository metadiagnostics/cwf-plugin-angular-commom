export declare class IOConstants {
    static readonly INTAKE: string;
    static readonly OUTPUT: string;
    static readonly OUTPUT_TYPE: string;
    static readonly INPUT_TYPE: string;
    static readonly I_O: string;
    static readonly TOTAL: string;
    static readonly DAILY_TOTAL: string;
    static readonly _24_HOUR_TOTAL: string;
    static readonly HEADER_TITLE: string;
    static readonly HEADER_TITLE_EX: string;
}
