"use strict";
/*
 *  Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var moment = require("moment");
var constants_1 = require("./constants");
var TotalColumnProvider = /** @class */ (function () {
    function TotalColumnProvider(rows, columns, todayDate, totalType) {
        this.rows = rows;
        this.columns = columns;
        this.todayDate = todayDate;
        this.totalType = totalType;
        // console.log('before rows: ' + rows);
        this.adjustTotals();
    }
    TotalColumnProvider.prototype.isRelevantDay = function (time) {
        if (this.totalType === constants_1.IOConstants.DAILY_TOTAL) {
            return this.isSameDay(time, this.todayDate);
        }
        if (this.totalType === constants_1.IOConstants._24_HOUR_TOTAL) {
            return this.isLast24Hours(time, this.todayDate);
        }
        return false;
    };
    TotalColumnProvider.prototype.isSameDay = function (time, todayDate) {
        if (!moment(time, 'YYYYMMDDHHmmss').isValid()) {
            return false;
        }
        var year = time.substring(0, 4);
        var month = time.substring(4, 6);
        var day = time.substring(6, 8);
        var testDay = new Date(year, month, day);
        year = todayDate.substring(0, 4);
        month = todayDate.substring(4, 6);
        day = todayDate.substring(6, 8);
        var today = new Date(year, month, day);
        var retVal = testDay.getFullYear() === today.getFullYear() &&
            testDay.getMonth() === today.getMonth() &&
            testDay.getDate() === today.getDate();
        return retVal;
    };
    TotalColumnProvider.prototype.isLast24Hours = function (time, todayDate) {
        if (!moment(time, 'YYYYMMDDHHmmss').isValid()) {
            return false;
        }
        var year = time.substring(0, 4);
        var month = time.substring(4, 6);
        var day = time.substring(6, 8);
        var hour = time.substring(8, 10);
        // console.log('test day  --- year: '+ year + ' month: ' + month + 'day: ' + day + ' hour: ' + hour);
        var testDay = new Date(year, month, day, hour);
        year = todayDate.substring(0, 4);
        month = todayDate.substring(4, 6);
        day = todayDate.substring(6, 8);
        hour = todayDate.substring(8, 10);
        // console.log('today --- year: '+ year + ' month: ' + month + 'day: ' + day + ' hour: ' + hour);
        var today = new Date(year, month, day, hour);
        var retVal = testDay.getFullYear() === today.getFullYear() &&
            testDay.getMonth() === today.getMonth() &&
            ((testDay.getDate() === today.getDate() || // same day
                (today.getDate() - testDay.getDate() === 1 && testDay.getHours() >= today.getHours()) // yesterday and hours higher than now
            ));
        return retVal;
    };
    TotalColumnProvider.prototype.adjustTotals = function () {
        var that = this;
        _.forEach(this.rows, function (row) {
            var rowType = row.type;
            var rowSum = 0;
            // let's all of the keys for this row and pick out the ones that
            // are integers and check whether the dates are relevant or not...
            Object.keys(row).map(function (key) {
                var rowValue = parseInt(row[key]);
                if (!isNaN(rowValue)) {
                    var dateToCompare = key;
                    var relevantDate = that.isRelevantDay(dateToCompare);
                    if (relevantDate) {
                        if (rowType === constants_1.IOConstants.OUTPUT_TYPE) {
                            rowSum = rowSum - rowValue;
                        }
                        else {
                            rowSum = rowSum + rowValue;
                        }
                    }
                }
            });
            row.total = rowSum;
        });
        this.columns.push({ headerName: this.GetHeaderName(), field: 'total' });
        // _.forEach(this.columns, column => {
        //     console.log(column);
        // });
    };
    TotalColumnProvider.prototype.GetHeaderName = function () {
        var headerName = constants_1.IOConstants.TOTAL + ' (';
        if (this.totalType === constants_1.IOConstants._24_HOUR_TOTAL) {
            headerName += 'last 24 hours)';
        }
        else if (this.totalType === constants_1.IOConstants.DAILY_TOTAL) {
            headerName += 'today)';
        }
        else {
            console.error('undefined total type');
        }
        return headerName;
    };
    TotalColumnProvider.prototype.getRows = function () {
        return this.rows;
    };
    TotalColumnProvider.prototype.getColumns = function () {
        return this.columns;
    };
    return TotalColumnProvider;
}());
exports.TotalColumnProvider = TotalColumnProvider;
//# sourceMappingURL=total.columns.js.map