"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var moment = require("moment");
var _ = require("lodash");
var flowsheet_model_processor_1 = require("../flowsheet.model.processor");
var total_columns_1 = require("./total.columns");
var constants_1 = require("./constants");
var IOModelProcessor = /** @class */ (function (_super) {
    __extends(IOModelProcessor, _super);
    function IOModelProcessor(totalType) {
        var _this = _super.call(this) || this;
        _this.totalType = totalType;
        _this.hasInputRows = false;
        _this.hasOutputRows = false;
        _this.setHeaderTitle(constants_1.IOConstants.HEADER_TITLE);
        return _this;
    }
    IOModelProcessor.prototype.process = function (model) {
        _super.prototype.process.call(this, model);
        this.postProcess();
    };
    IOModelProcessor.prototype.postProcess = function () {
        if (_.isEmpty(this.getRows())) {
            return;
        }
        var currentDate = moment().format('YYYYMMDDHH');
        var i_o_total = new total_columns_1.TotalColumnProvider(this.getRows(), this.getColumns(), currentDate, this.totalType);
        this.columns = i_o_total.getColumns();
        this.rows = i_o_total.getRows();
        this.addFullRows();
    };
    IOModelProcessor.prototype.addFullRows = function () {
        var index = 0;
        var firstInputIndex = -1;
        var firstOutputIndex = -1;
        // assumption here is that the input rows and output rows are returned together and not
        // interspersed...
        // lets look for input and output indeces...
        while (index < this.rows.length) {
            var type = this.rows[index].type + '';
            if (type === constants_1.IOConstants.INPUT_TYPE) {
                if (firstInputIndex === -1) {
                    firstInputIndex = index;
                }
            }
            else if (type === constants_1.IOConstants.OUTPUT_TYPE) {
                if (firstOutputIndex === -1) {
                    firstOutputIndex = index;
                }
            }
            index++;
        }
        if (firstInputIndex !== -1) {
            this.hasInputRows = true;
            var intake = {};
            intake.headerName = constants_1.IOConstants.INTAKE;
            this.rows.splice(firstInputIndex, 0, intake);
        }
        if (firstOutputIndex !== -1) {
            var output = {};
            output.headerName = constants_1.IOConstants.OUTPUT;
            this.hasOutputRows = true;
            // have we added an Intake row already?
            var location_1 = (this.hasInputRows) ? firstOutputIndex + 1 : firstOutputIndex;
            this.rows.splice(location_1, 0, output);
        }
    };
    IOModelProcessor.prototype.hasInput = function () {
        return this.hasInputRows;
    };
    IOModelProcessor.prototype.hasOutput = function () {
        return this.hasOutputRows;
    };
    return IOModelProcessor;
}(flowsheet_model_processor_1.FlowsheetModelProcessor));
exports.IOModelProcessor = IOModelProcessor;
//# sourceMappingURL=model.processor.js.map