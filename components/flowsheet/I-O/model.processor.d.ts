import { FlowsheetModelProcessor } from '../flowsheet.model.processor';
export declare class IOModelProcessor extends FlowsheetModelProcessor {
    private totalType;
    private hasInputRows;
    private hasOutputRows;
    constructor(totalType: string);
    process(model: any): void;
    protected postProcess(): void;
    private addFullRows();
    hasInput(): boolean;
    hasOutput(): boolean;
}
