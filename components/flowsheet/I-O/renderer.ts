/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


import {BehaviorSubject} from "rxjs/BehaviorSubject";
import * as _ from 'lodash';
import { Grid, Cell } from '../../grid-renderer';
import { FullWidthCellRenderer } from '../full-width-cell-renderer';
import { IOConstants } from './constants';
import {Observable} from 'rxjs/Observable';

export class I_O_Grid extends Grid {
    private _selectedRow = new BehaviorSubject<any>(null);
    public selectedRowChanged$: Observable<any> = this._selectedRow.asObservable();

    private _visibleColumns = new BehaviorSubject<any>(null);
    public visibleColumnsChanged$ = this._visibleColumns.asObservable();


    constructor() {
        super();
    }

    private isInputOrOutputRow(node) : boolean{
        return this.isInputRow(node) || this.isOutputRow(node);
    }

    initialize(gridOptions: any, uuid: any, paging?: boolean, pageSize?: number) : void {
        if (!_.isUndefined(paging) && paging) {
            gridOptions.pagination = true;
            gridOptions.paginationPageSize = _.isUndefined(pageSize) ? 50 : pageSize;

            super.initialize(gridOptions, uuid, gridOptions.pagination, gridOptions.paginationPageSize);
        } else {

            super.initialize(gridOptions, uuid);
        }

        gridOptions.enableSorting = false;
        gridOptions.suppressColumnVirtualisation = false;
        gridOptions.rowSelection = 'single';
        gridOptions.enableColResize = false;
        gridOptions.suppressMovableColumns = true;
        gridOptions.suppressCellSelection = true;
        gridOptions.suppressLoadingOverlay = false;
        gridOptions.emptyHeight = '200px';

        gridOptions.getBusinessKeyForNode = (node) => {
            return node.lastChild ? 'NetIntake' : 'nosideeffect';
        };

        gridOptions.isFullWidthCell =  (node) => {
            return this.isInputOrOutputRow(node) ;
        };

        gridOptions.onSelectionChanged =  ($event) => {
            let selectedRows = $event.api.getSelectedRows();
            if (_.isArray(selectedRows) && ! _.isEmpty(selectedRows)) {
                this._selectedRow.next(selectedRows[0]);
            }
        }

        gridOptions.onVirtualColumnsChanged =  ($event) => {
            this._visibleColumns.next($event.columnApi.
                                          columnController.
                                          allDisplayedVirtualColumns);
        }

        gridOptions.fullWidthCellRendererFramework = FullWidthCellRenderer;
    }

    protected configureColumns(columns) : Array<any> {
        _.forEach(columns, function(column) {
            column.cellRenderer = I_O_Grid.cellRender;
            // column.cellStyle = {'text-align': 'center'};
            column.editable = false;
            column.width = 110;
        });

        if (columns !== undefined && columns.length > 0) {
            columns[0].pinned = 'left';
            columns[0].width = 330;
            columns[columns.length - 1].width = 180;
        }

        return columns;
    }

    protected configureRows(rows) : Array<any>{
        return rows;
    }

    public isInputRow(node) : boolean {
        return node.data.headerName === IOConstants.INTAKE;

    }

    public isOutputRow(node) : boolean {
        return node.data.headerName === IOConstants.OUTPUT;
    }

    public selectFirstRow(gridOptions) : void {
        Grid.selectRow(gridOptions, 1);
    }

    static cellRender(params) {
        let currentCell : Cell = Grid.getCellSettings(params.colDef.field, params.data.values);
        // TODO uncomment this when it is time...
        // lets not draw the flag since no valida data is coming back...
        // if (!_.isEmpty(currentCell) && !_.isEmpty(currentCell.info)) {
        //     console.error(currentCell.info);
        //     return Grid.flag(currentCell.value, currentCell.info);
        // }

        if (params.value === 'OVERDUE' || params.value === 'DUE') {
            return Grid.dueOverDueCellRenderer(params);
        }

        return Grid.normal(params)
    }

}








