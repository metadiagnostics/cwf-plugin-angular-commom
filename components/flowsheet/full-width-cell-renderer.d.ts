import { ICellRendererAngularComp } from "ag-grid-angular";
export declare class FullWidthCellRenderer implements ICellRendererAngularComp {
    private params;
    values: string;
    agInit(params: any): void;
    refresh(): boolean;
}
