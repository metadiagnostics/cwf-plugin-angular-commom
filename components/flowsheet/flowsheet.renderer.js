"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
var _ = require("lodash");
var grid_renderer_1 = require("../grid-renderer");
var FlowsheetGrid = /** @class */ (function (_super) {
    __extends(FlowsheetGrid, _super);
    function FlowsheetGrid() {
        var _this = _super.call(this) || this;
        _this._selectedRow = new BehaviorSubject_1.BehaviorSubject(null);
        _this.selectedRowChanged$ = _this._selectedRow.asObservable();
        _this._visibleColumns = new BehaviorSubject_1.BehaviorSubject(null);
        _this.visibleColumnsChanged$ = _this._visibleColumns.asObservable();
        return _this;
    }
    FlowsheetGrid.prototype.initialize = function (gridOptions, uuid, paging, pageSize) {
        var _this = this;
        if (!_.isUndefined(paging) && paging) {
            gridOptions.pagination = true;
            gridOptions.paginationPageSize = _.isUndefined(pageSize) ? 50 : pageSize;
            _super.prototype.initialize.call(this, gridOptions, uuid, gridOptions.pagination, gridOptions.paginationPageSize);
        }
        else {
            _super.prototype.initialize.call(this, gridOptions, uuid);
        }
        gridOptions.enableSorting = false;
        gridOptions.suppressColumnVirtualisation = false;
        gridOptions.rowSelection = 'single';
        gridOptions.enableColResize = false;
        gridOptions.suppressMovableColumns = true;
        gridOptions.suppressCellSelection = true;
        gridOptions.suppressLoadingOverlay = false;
        gridOptions.emptyHeight = '200px';
        gridOptions.onSelectionChanged = function ($event) {
            var selectedRows = $event.api.getSelectedRows();
            if (_.isArray(selectedRows) && !_.isEmpty(selectedRows)) {
                _this._selectedRow.next(selectedRows[0]);
            }
        };
        gridOptions.onVirtualColumnsChanged = function ($event) {
            _this._visibleColumns.next($event.columnApi.
                columnController.
                allDisplayedVirtualColumns);
        };
    };
    FlowsheetGrid.prototype.configureColumns = function (columns) {
        _.forEach(columns, function (column) {
            column.cellRenderer = FlowsheetGrid.cellRender;
            column.editable = false;
            column.width = 110;
        });
        if (columns !== undefined && columns.length > 0) {
            columns[0].pinned = 'left';
            columns[0].width = 330;
            columns[columns.length - 1].width = 180;
        }
        return columns;
    };
    FlowsheetGrid.prototype.configureRows = function (rows) {
        return rows;
    };
    FlowsheetGrid.prototype.selectFirstRow = function (gridOptions) {
        grid_renderer_1.Grid.selectRow(gridOptions, 0);
    };
    FlowsheetGrid.cellRender = function (params) {
        var currentCell = grid_renderer_1.Grid.getCellSettings(params.colDef.field, params.data.values);
        // TODO uncomment this when it is time...
        // lets not draw the flag since no valida data is coming back...
        // if (!_.isEmpty(currentCell) && !_.isEmpty(currentCell.info)) {
        //     console.error(currentCell.info);
        //     return Grid.flag(currentCell.value, currentCell.info);
        // }
        if (params.value === 'OVERDUE' || params.value === 'DUE') {
            return grid_renderer_1.Grid.dueOverDueCellRenderer(params);
        }
        return grid_renderer_1.Grid.normal(params);
    };
    return FlowsheetGrid;
}(grid_renderer_1.Grid));
exports.FlowsheetGrid = FlowsheetGrid;
//# sourceMappingURL=flowsheet.renderer.js.map