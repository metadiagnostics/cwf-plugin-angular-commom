import { LineChartElement } from "../charts/linechart.utils";
export declare abstract class FlowsheetPluginHelper {
    protected static getFormattedDateParam(date: Date, daysToAdd: number): string;
    protected static getDataToPlot(row: any, columns: Array<any>, includeAll?: boolean): Array<LineChartElement>;
    protected abstract initializeFlowsheet(): void;
    protected abstract getModelQueryParams(): any;
    protected abstract activatePlugin(response: any): void;
}
