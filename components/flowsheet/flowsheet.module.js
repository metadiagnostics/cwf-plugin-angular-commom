"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var main_1 = require("ag-grid-angular/main");
var flowsheet_component_1 = require("./flowsheet.component");
var full_width_cell_renderer_1 = require("./full-width-cell-renderer");
var paging_control_module_1 = require("../paging/paging-control.module");
var FlowsheetModule = /** @class */ (function () {
    function FlowsheetModule() {
    }
    FlowsheetModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                paging_control_module_1.PagingControlModule,
                main_1.AgGridModule.withComponents([
                    full_width_cell_renderer_1.FullWidthCellRenderer
                ])
            ],
            declarations: [
                flowsheet_component_1.FlowsheetComponent,
                full_width_cell_renderer_1.FullWidthCellRenderer
            ],
            exports: [
                flowsheet_component_1.FlowsheetComponent
            ]
        })
    ], FlowsheetModule);
    return FlowsheetModule;
}());
exports.FlowsheetModule = FlowsheetModule;
//# sourceMappingURL=flowsheet.module.js.map