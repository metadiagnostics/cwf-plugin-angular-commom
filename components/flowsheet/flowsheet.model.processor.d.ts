export interface RowSetting {
    originalName: string;
    name: string;
    type: string;
    governance: string;
    values: Array<any>;
    headerName: string;
    columnName: any;
    resourceID: string;
}
export interface ColumnHeader {
    time: string;
    timestamp: string;
}
export declare class FlowsheetModelProcessor {
    protected type: string;
    protected rows: Array<RowSetting>;
    protected columns: Array<ColumnHeader>;
    private headerTitle;
    constructor(type?: string);
    protected setHeaderTitle(title: string): void;
    process(model: any): void;
    getRows(): any;
    getColumns(): any;
    private getColumnsFromModel(model);
    private getRowsFromModel(model);
    private formatHeaders(columnsDetails);
    private format(timestamp);
    setType(type: string): void;
    public: any;
}
