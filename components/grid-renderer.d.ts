import { GridOptions } from 'ag-grid/main';
export interface Cell {
    timestamp: string;
    value: string;
    info: string;
}
export declare abstract class Grid {
    protected gridWidth: number;
    protected gridHeight: number;
    private rowHeight;
    private pagingPanelHeight;
    private elementId;
    private paging;
    private pageSize;
    constructor();
    initialize(gridOptions: GridOptions, elementId: any, paging?: boolean, pageSize?: number): void;
    refreshGrid(gridOptions: any, rows: any, columns: any): void;
    protected abstract configureColumns(columns: any): any;
    protected abstract configureRows(rows: any): any;
    private isScrollarShowingGrid(gridOptions);
    private getGridWidth();
    protected setGridHeight(gridOptions: any): void;
    selectFirstRow(gridOptions: GridOptions): void;
    static selectRow(gridOptions: GridOptions, index: number): void;
    static dueOverDueCellRenderer(params: any): HTMLDivElement;
    static getCellSettings(columnHeader: string, rowData: Array<any>): Cell;
    static flag(value: string, marker: string): HTMLDivElement;
    static normal(params: any): HTMLDivElement;
}
