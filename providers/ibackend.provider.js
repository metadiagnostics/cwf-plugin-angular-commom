"use strict";
/*
 *  Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var ibackend_service_1 = require("../services/ibackend.service");
var fusion_service_1 = require("../services/fusion.service");
var rest_service_1 = require("../services/rest.service");
var utilities_1 = require("../misc/utilities");
var ibackendServiceFactory = function (appRef, zone, http) {
    if (utilities_1.Utilities.isRunningStandalone()) {
        return new rest_service_1.RESTService(appRef, zone, http);
    }
    else {
        return new fusion_service_1.FusionService(http);
    }
};
exports.ibackendProvider = {
    provide: ibackend_service_1.IBackendService,
    useFactory: ibackendServiceFactory,
    deps: [core_1.ApplicationRef, core_1.NgZone, http_1.Http]
};
//# sourceMappingURL=ibackend.provider.js.map