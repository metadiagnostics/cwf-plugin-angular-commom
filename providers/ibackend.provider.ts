/*
 *  Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


import { ApplicationRef, NgZone } from '@angular/core';
import { Http } from '@angular/http';

import { IBackendService } from '../services/ibackend.service';
import { FusionService } from '../services/fusion.service';
import { RESTService } from '../services/rest.service';
import {Utilities} from '../misc/utilities';


let ibackendServiceFactory = (appRef: ApplicationRef, zone: NgZone, http: Http) => {
  if (Utilities.isRunningStandalone()){
    return new RESTService(appRef, zone, http);
  } else {
    return new FusionService(http);
  }
};

export let ibackendProvider = { 
  provide: IBackendService,
  useFactory: ibackendServiceFactory,
  deps: [ApplicationRef, NgZone, Http]
};