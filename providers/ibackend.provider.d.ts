import { ApplicationRef, NgZone } from '@angular/core';
import { Http } from '@angular/http';
import { IBackendService } from '../services/ibackend.service';
import { FusionService } from '../services/fusion.service';
import { RESTService } from '../services/rest.service';
export declare let ibackendProvider: {
    provide: typeof IBackendService;
    useFactory: (appRef: ApplicationRef, zone: NgZone, http: Http) => FusionService | RESTService;
    deps: (typeof NgZone | typeof ApplicationRef | typeof Http)[];
};
