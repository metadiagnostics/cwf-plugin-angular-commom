export declare class Utilities {
    static readonly TIMESTAMP_INPUT_FORMAT: string;
    static readonly TIMESTAMP_OUTPUT_FORMAT1: string;
    static readonly TIMESTAMP_OUTPUT_FORMAT2: string;
    static formatTimestamp(timestamp: string, format?: string): string;
    static toEnum(value: any, target: any, dflt?: number): any;
    static base64clean(str: String): String;
    static isRunningStandalone(): any;
}
export declare class Guid {
    static newGuid(): string;
}
