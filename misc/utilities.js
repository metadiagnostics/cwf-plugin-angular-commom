"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var moment = require("moment");
var _ = require("lodash");
var Utilities = /** @class */ (function () {
    function Utilities() {
    }
    Utilities.formatTimestamp = function (timestamp, format) {
        if (format === void 0) { format = Utilities.TIMESTAMP_OUTPUT_FORMAT1; }
        var ts = moment(timestamp, Utilities.TIMESTAMP_INPUT_FORMAT);
        return !ts.isValid() ? '' : ts.format(format);
    };
    Utilities.toEnum = function (value, target, dflt) {
        if (_.isFinite(value)) {
            return value;
        }
        value = _.isNil(value) ? value : target[value];
        return _.isNil(value) ? dflt : value;
    };
    Utilities.base64clean = function (str) {
        var INVALID_BASE64_RE = /[^+/0-9A-Za-z-_]/g;
        // Node strips out invalid characters like \n and \t from the string, base64-js does not
        str = str.trim().replace(INVALID_BASE64_RE, '');
        // Node converts strings with length < 2 to ''
        if (str.length < 2)
            return '';
        // Node allows for non-padded base64 strings (missing trailing ===), base64-js does not
        while (str.length % 4 !== 0) {
            str = str + '=';
        }
        return str;
    };
    Utilities.isRunningStandalone = function () {
        return window['standalone'];
    };
    Utilities.TIMESTAMP_INPUT_FORMAT = 'YYYYMMDDHHmmss';
    Utilities.TIMESTAMP_OUTPUT_FORMAT1 = 'DD-MMM HH:mm';
    Utilities.TIMESTAMP_OUTPUT_FORMAT2 = 'MM/DD HH:mm';
    return Utilities;
}());
exports.Utilities = Utilities;
var Guid = /** @class */ (function () {
    function Guid() {
    }
    Guid.newGuid = function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };
    return Guid;
}());
exports.Guid = Guid;
//# sourceMappingURL=utilities.js.map