/**
 * This interface must be implemented by all the CWF-Angular Components
 */
export interface CWFPlugin {
    getPluginId(): string;
}
