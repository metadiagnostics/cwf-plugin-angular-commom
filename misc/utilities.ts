import * as moment from 'moment';
import _ = require('lodash');

export class Utilities {

    public static readonly TIMESTAMP_INPUT_FORMAT = 'YYYYMMDDHHmmss';

    public static readonly TIMESTAMP_OUTPUT_FORMAT1 = 'DD-MMM HH:mm';

    public static readonly TIMESTAMP_OUTPUT_FORMAT2 = 'MM/DD HH:mm';

    public static formatTimestamp(timestamp: string, format: string = Utilities.TIMESTAMP_OUTPUT_FORMAT1): string {
        let ts: moment.Moment = moment(timestamp, Utilities.TIMESTAMP_INPUT_FORMAT);
        return !ts.isValid() ? '' : ts.format(format);
    }

    public static toEnum(value: any, target: any, dflt?: number): any {
        if (_.isFinite(value)) {
            return value;
        }
        value = _.isNil(value) ? value : target[value];
        return _.isNil(value) ? dflt : value;
    }

    public static base64clean (str: String) {
        var INVALID_BASE64_RE = /[^+/0-9A-Za-z-_]/g

        // Node strips out invalid characters like \n and \t from the string, base64-js does not
        str = str.trim().replace(INVALID_BASE64_RE, '')
        // Node converts strings with length < 2 to ''
        if (str.length < 2) return ''
        // Node allows for non-padded base64 strings (missing trailing ===), base64-js does not
        while (str.length % 4 !== 0) {
            str = str + '='
        }
        return str
    }

    public static isRunningStandalone() {
        return window['standalone'];
    }
}

export class Guid {
    static newGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }
}


