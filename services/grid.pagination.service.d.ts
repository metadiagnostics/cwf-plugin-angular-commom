import { Observable } from "rxjs/Observable";
export declare class GridPaginationService {
    private _pageNumber;
    pageNumber$: Observable<any>;
    private _totalPages;
    totalPages$: Observable<any>;
    setPageNumber(page: number): void;
    setTotalPages(count: number): void;
}
