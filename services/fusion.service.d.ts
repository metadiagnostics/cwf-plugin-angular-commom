import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { IBackendService } from './ibackend.service';
export declare class FusionService extends IBackendService {
    private http;
    private _plugin;
    constructor(http: Http);
    sendBackendEvent(eventName: String, data: any): void;
    setPlugin(plugin: any): void;
    static isEmptyModel(model: any): boolean;
    static isNoopModel(model: any): boolean;
}
