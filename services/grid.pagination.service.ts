/*
 *  Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import {Injectable} from '@angular/core';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Observable } from "rxjs/Observable";

@Injectable()
export class GridPaginationService {
    private _pageNumber = new BehaviorSubject<any>(null);
    public pageNumber$: Observable<any> = this._pageNumber.asObservable();

    private _totalPages = new BehaviorSubject<any>(null);
    public totalPages$: Observable<any> = this._totalPages.asObservable();

    public setPageNumber(page: number) {
        this._pageNumber.next(page);
    }

    public setTotalPages(count: number) {
        this._totalPages.next(Math.ceil(count));
    }

}






