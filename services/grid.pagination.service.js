"use strict";
/*
 *  Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
var GridPaginationService = /** @class */ (function () {
    function GridPaginationService() {
        this._pageNumber = new BehaviorSubject_1.BehaviorSubject(null);
        this.pageNumber$ = this._pageNumber.asObservable();
        this._totalPages = new BehaviorSubject_1.BehaviorSubject(null);
        this.totalPages$ = this._totalPages.asObservable();
    }
    GridPaginationService.prototype.setPageNumber = function (page) {
        this._pageNumber.next(page);
    };
    GridPaginationService.prototype.setTotalPages = function (count) {
        this._totalPages.next(Math.ceil(count));
    };
    GridPaginationService = __decorate([
        core_1.Injectable()
    ], GridPaginationService);
    return GridPaginationService;
}());
exports.GridPaginationService = GridPaginationService;
//# sourceMappingURL=grid.pagination.service.js.map