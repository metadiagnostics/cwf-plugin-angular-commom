/*
 *  Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Rx';

/**
 * Abstract class that abtracts the concrete way to send data to the backend.
 * 
 */
export abstract class IBackendService {
  
    protected _rootElement : any = null;
    // Observable stream
    protected _modelChangeSubject = new BehaviorSubject<any>(null);
    

    setRootNativeElement(rootElement) {
        this._rootElement = rootElement;
    }

    getRootNativeElement() {
        return this._rootElement;
    }
    
    getModelChangeSource$():Observable<any>{
      return this._modelChangeSubject.asObservable();
    }

    setModel(model: any) {
        console.log('model changed, lets broadcast this', model);
        this._modelChangeSubject.next(model);
    }
    
    abstract sendBackendEvent(eventName: String, data: any);


}