"use strict";
/*
 *  Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
/**
 * Abstract class that abtracts the concrete way to send data to the backend.
 *
 */
var IBackendService = /** @class */ (function () {
    function IBackendService() {
        this._rootElement = null;
        // Observable stream
        this._modelChangeSubject = new BehaviorSubject_1.BehaviorSubject(null);
    }
    IBackendService.prototype.setRootNativeElement = function (rootElement) {
        this._rootElement = rootElement;
    };
    IBackendService.prototype.getRootNativeElement = function () {
        return this._rootElement;
    };
    IBackendService.prototype.getModelChangeSource$ = function () {
        return this._modelChangeSubject.asObservable();
    };
    IBackendService.prototype.setModel = function (model) {
        console.log('model changed, lets broadcast this', model);
        this._modelChangeSubject.next(model);
    };
    return IBackendService;
}());
exports.IBackendService = IBackendService;
//# sourceMappingURL=ibackend.service.js.map