import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from "rxjs/Observable";
export declare class CreateResourceService {
    private _closeResourceFormEvent;
    closeResourceFormEvent: Observable<any>;
    closeForm(domain: string): void;
}
