"use strict";
/*
 *  Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var $ = require("jquery");
var fujion = require('fujion-core');
// Import RxJs required methods
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
var ibackend_service_1 = require("./ibackend.service");
var FusionService = /** @class */ (function (_super) {
    __extends(FusionService, _super);
    function FusionService(http) {
        var _this = _super.call(this) || this;
        _this.http = http;
        _this._plugin = false;
        return _this;
    }
    FusionService.prototype.sendBackendEvent = function (eventName, data) {
        var wgt = fujion.wgt(this._rootElement);
        var event = $.Event(eventName, { target: wgt.widget$, data: data });
        wgt.trigger(eventName, { data: data });
    };
    FusionService.prototype.setPlugin = function (plugin) {
        this._plugin = plugin;
        console.log('this control plugin setting: ', this._plugin);
    };
    FusionService.isEmptyModel = function (model) {
        var retVal = model !== null
            && model.data !== null
            && Array.isArray(model.data)
            && model.data.length === 0;
        // console.log('model is: ' + (retVal === false ? 'not ' : ' ') + 'empty');
        return retVal;
    };
    FusionService.isNoopModel = function (model) {
        var retVal = model !== null
            && model.dataUnavailable !== undefined
            && model.dataUnavailable === true;
        // console.log('model is: ' + (retVal === false ? 'not ' : ' ') + 'noop');
        return retVal;
    };
    FusionService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], FusionService);
    return FusionService;
}(ibackend_service_1.IBackendService));
exports.FusionService = FusionService;
//# sourceMappingURL=fusion.service.js.map