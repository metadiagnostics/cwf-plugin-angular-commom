"use strict";
/*
 *  Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
var CreateResourceService = /** @class */ (function () {
    function CreateResourceService() {
        this._closeResourceFormEvent = new BehaviorSubject_1.BehaviorSubject(null);
        this.closeResourceFormEvent = this._closeResourceFormEvent.asObservable();
    }
    CreateResourceService.prototype.closeForm = function (domain) {
        this._closeResourceFormEvent.next(domain);
    };
    CreateResourceService = __decorate([
        core_1.Injectable()
    ], CreateResourceService);
    return CreateResourceService;
}());
exports.CreateResourceService = CreateResourceService;
//# sourceMappingURL=create-resource.service.js.map