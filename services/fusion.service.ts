/*
 *  Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import * as $ from "jquery";
let fujion = require('fujion-core');

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { IBackendService } from './ibackend.service';

@Injectable()
export class FusionService extends IBackendService {
    private _plugin: boolean = false;

    constructor(private http: Http) {
      super();
    }

    sendBackendEvent(eventName: String, data: any) {
        let wgt = fujion.wgt(this._rootElement);
        let event = $.Event(eventName, {target: wgt.widget$, data: data});
        wgt.trigger(eventName, {data: data});
    }

    setPlugin(plugin) {
        this._plugin = plugin;
        console.log('this control plugin setting: ' , this._plugin);
    }

    static isEmptyModel(model) {
        var retVal = model !== null
            && model.data !== null
            && Array.isArray(model.data)
            && model.data.length === 0;

        // console.log('model is: ' + (retVal === false ? 'not ' : ' ') + 'empty');
        return retVal;
    }

    static isNoopModel(model): boolean {
        let retVal = model !== null
            && model.dataUnavailable !== undefined
            && model.dataUnavailable === true;

        // console.log('model is: ' + (retVal === false ? 'not ' : ' ') + 'noop');
        return retVal;
    }
}