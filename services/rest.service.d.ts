import { ApplicationRef, NgZone } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { IBackendService } from './ibackend.service';
export declare class RESTService extends IBackendService {
    private appRef;
    private zone;
    private http;
    modelChangeSource$: Observable<any>;
    constructor(appRef: ApplicationRef, zone: NgZone, http: Http);
    sendBackendEvent(eventName: String, data: any): void;
}
