/*
 *  Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { ApplicationRef, ComponentRef, Injectable, NgZone } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import * as _ from 'lodash';


// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';

import { IBackendService } from './ibackend.service';

const serverURL : string = 'http://localhost:4000/';

@Injectable()
export class RESTService extends IBackendService {

    public modelChangeSource$: Observable<any> = this._modelChangeSubject.asObservable();

    constructor(private appRef: ApplicationRef, private zone: NgZone, private http: Http) {
        super();
    }

    sendBackendEvent(eventName: String, data: any) {
        let callback: string = data._callback;
        if (_.isEmpty(callback)) {
            // nothing to do...
            return;
        }

        let endpoint = serverURL;

        if (!_.isEmpty(data.endpoint)) {
            endpoint += data.endpoint;
        } else {
            endpoint += _.get(data, 'type', '') + '/' + eventName;
        }

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        this.http.post(endpoint, data, options).subscribe(response => {

                try{
                    let model = _.get(response, '_body');
                    // console.log(JSON.stringify(model));

                    let result : any = JSON.parse(model);

                    this.zone.run(() => {
                        let instance = this.appRef.components[0].instance;
                        if (!instance[callback]){
                            throw new Error("Instance "+instance+" doesn't have a method called "+callback);
                        }
                        instance[callback].apply(instance, [result]);
                    });

                } catch(e) {
                    console.log('bad model received: ', e);
                }

            // we also have the option of publishing the new model also

        });
    }
}






