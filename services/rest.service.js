"use strict";
/*
 *  Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var _ = require("lodash");
// Import RxJs required methods
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
var ibackend_service_1 = require("./ibackend.service");
var serverURL = 'http://localhost:4000/';
var RESTService = /** @class */ (function (_super) {
    __extends(RESTService, _super);
    function RESTService(appRef, zone, http) {
        var _this = _super.call(this) || this;
        _this.appRef = appRef;
        _this.zone = zone;
        _this.http = http;
        _this.modelChangeSource$ = _this._modelChangeSubject.asObservable();
        return _this;
    }
    RESTService.prototype.sendBackendEvent = function (eventName, data) {
        var _this = this;
        var callback = data._callback;
        if (_.isEmpty(callback)) {
            // nothing to do...
            return;
        }
        var endpoint = serverURL;
        if (!_.isEmpty(data.endpoint)) {
            endpoint += data.endpoint;
        }
        else {
            endpoint += _.get(data, 'type', '') + '/' + eventName;
        }
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        this.http.post(endpoint, data, options).subscribe(function (response) {
            try {
                var model = _.get(response, '_body');
                // console.log(JSON.stringify(model));
                var result_1 = JSON.parse(model);
                _this.zone.run(function () {
                    var instance = _this.appRef.components[0].instance;
                    if (!instance[callback]) {
                        throw new Error("Instance " + instance + " doesn't have a method called " + callback);
                    }
                    instance[callback].apply(instance, [result_1]);
                });
            }
            catch (e) {
                console.log('bad model received: ', e);
            }
            // we also have the option of publishing the new model also
        });
    };
    RESTService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [core_1.ApplicationRef, core_1.NgZone, http_1.Http])
    ], RESTService);
    return RESTService;
}(ibackend_service_1.IBackendService));
exports.RESTService = RESTService;
//# sourceMappingURL=rest.service.js.map