import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Rx';
/**
 * Abstract class that abtracts the concrete way to send data to the backend.
 *
 */
export declare abstract class IBackendService {
    protected _rootElement: any;
    protected _modelChangeSubject: BehaviorSubject<any>;
    setRootNativeElement(rootElement: any): void;
    getRootNativeElement(): any;
    getModelChangeSource$(): Observable<any>;
    setModel(model: any): void;
    abstract sendBackendEvent(eventName: String, data: any): any;
}
